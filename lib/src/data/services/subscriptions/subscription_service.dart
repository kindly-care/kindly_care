import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/analytics/analytics.dart';
import '../../../common/constants/constants.dart';
import '../../references.dart';
import 'subscription_repository.dart';

class SubscriptionService implements SubscriptionRepository {
  @override
  Future<void> updateSubscription(
      {required DateTime subEnds, required String patientId}) async {
    try {
      final DocumentReference<Map<String, dynamic>> patientDoc =
          userCollection.doc(patientId);

      await patientDoc.update(<String, dynamic>{
        kSubscriptionEnds: subEnds.toIso8601String(),
      }).timeout(kTimeOut);

      analytics.logEvent(name: kSubscriptionUpdatedEvt);
    } on Exception {
      throw UpdateDataException(kOperationFailed);
    }
  }
}

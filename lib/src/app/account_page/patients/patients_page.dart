import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../blocs/patient/patient_bloc.dart';
import '../../../common/constants/constants.dart';
import '../../../cubits/account_cubit/account_cubit.dart';
import '../../../data/services/services.dart';

class PatientsPage extends StatelessWidget {
  const PatientsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PatientBloc>(
      create: (BuildContext context) => PatientBloc(
        patientRepository: context.read<PatientService>(),
      ),
      child: const _PatientsPageView(),
    );
  }
}

class _PatientsPageView extends StatefulWidget {
  const _PatientsPageView({Key? key}) : super(key: key);

  @override
  State<_PatientsPageView> createState() => _CareRecipientsPageViewState();
}

class _CareRecipientsPageViewState extends State<_PatientsPageView> {
  void _onAddPatientTap({Patient? patient}) {
    Navigator.pushNamed(
      context,
      kAddPatientPageRoute,
      arguments: patient,
    );
  }

  void _onAddCircleMemberTap(String patientId) {
    Navigator.pushNamed(
      context,
      kAddCircleMemberPageRoute,
      arguments: patientId,
    );
  }

  Future<void> _onRemovePatientTap(String patientId) async {
    context.read<PatientBloc>().add(RemovePatient(patientId: patientId));
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final List<Patient> patients = context
        .select((AccountCubit cubit) => cubit.state.patients)
        .where((Patient patient) => patient.nextOfKin.contains(user.uid))
        .toList();

    final bool isLoading = context.select((PatientBloc bloc) => bloc.state)
        is RemovePatientInProgress;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Care Recipients'),
        actions: <Widget>[
          IconButton(
            onPressed: _onAddPatientTap,
            icon: Icon(Icons.add, size: 3.8.h),
          ),
        ],
      ),
      body: patients.isEmpty
          ? const NoDataWidget(
              'You currently do not have any Care Recipients. Press the (+) button to add a Care Recipient.')
          : LoadingOverlay(
              isLoading: isLoading,
              child: BlocListener<PatientBloc, PatientState>(
                listener: (BuildContext context, PatientState state) {
                  if (state is RemovePatientError) {
                    errorSnackbar(context, state.error);
                  } else if (state is RemovePatientSuccess) {
                    successSnackbar(context, state.message);
                  }
                },
                child: ListView.separated(
                  itemCount: patients.length,
                  padding: EdgeInsets.all(3.0.w),
                  physics: const BouncingScrollPhysics(),
                  separatorBuilder: (_, __) =>
                      Divider(indent: 2.0.w, endIndent: 2.0.w),
                  itemBuilder: (BuildContext context, int index) {
                    final Patient patient = patients[index];
                    return Padding(
                      padding: EdgeInsets.symmetric(vertical: 0.8.h),
                      child: PatientCard(
                        patient: patient,
                        onTap: () {
                          Navigator.pushNamed(context, kPatientProfilePageRoute,
                              arguments: patient.uid);
                        },
                        onLongPress: () {
                          if (patient.nextOfKin.contains(user.uid)) {
                            _onPatientLongPress(patient);
                          }
                        },
                      ),
                    );
                  },
                ),
              ),
            ),
    );
  }

  void _showDeleteConfirmationDialog(Patient patient) {
    showTwoButtonDialog(
      context,
      isDestructiveAction: true,
      title: 'Confirm Action',
      content:
          'Are you sure you want to remove ${patient.name}? The action cannot be undone.',
      buttonText1: 'Cancel',
      buttonText2: 'Remove',
      onPressed1: () => Navigator.pop(context),
      onPressed2: () {
        Navigator.pop(context);
        _onRemovePatientTap(patient.uid);
      },
    );
  }

  void _onPatientLongPress(Patient patient) {
    HapticFeedback.lightImpact();
    optionsBottomSheet(
      height: 46.0.h,
      context: context,
      title: patient.name,
      options: <Widget>[
        IconListTile(
          title: 'Edit Profile',
          icon: FeatherIcons.edit,
          showTrailing: false,
          onTap: () {
            Navigator.pop(context);
            _onAddPatientTap(patient: patient);
          },
        ),
        Divider(indent: 18.w),
        IconListTile(
          title: 'Add Circle Member',
          icon: FeatherIcons.users,
          showTrailing: false,
          onTap: () async {
            Navigator.pop(context);
            _onAddCircleMemberTap(patient.uid);
          },
        ),
        Divider(indent: 18.w),
        IconListTile(
          title: 'Remove Care Recipient',
          icon: FeatherIcons.userMinus,
          showTrailing: false,
          onTap: () {
            Navigator.pop(context);
            _showDeleteConfirmationDialog(patient);
          },
        ),
      ],
    );
  }
}

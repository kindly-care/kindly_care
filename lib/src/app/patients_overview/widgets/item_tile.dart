import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class ItemTile extends StatelessWidget {
  final String item;
  final String? time;
  final String itemType;
  const ItemTile({
    super.key,
    this.time,
    required this.item,
    required this.itemType,
  });

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return ListTile(
      visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
      title: Padding(
        padding: EdgeInsets.only(bottom: 0.6.h),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              itemType,
              style: theme.titleMedium?.copyWith(fontSize: 17.5.sp),
            ),
            time == null
                ? const SizedBox.shrink()
                : Text(
                    time!,
                    style: theme.titleMedium?.copyWith(fontSize: 17.0.sp),
                  ),
          ],
        ),
      ),
      subtitle: Text(item,
          style: theme.bodyMedium?.copyWith(
            color: Colors.black.withOpacity(0.55),
          )),
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../blocs/patient/patient_bloc.dart';
import '../../../common/constants/constants.dart';
import '../../../data/services/services.dart';
import 'care_tasks_tab/care_task_tab.dart';
import 'patient_circle_tab/patient_circle_tab.dart';
import 'patient_info_tab/patient_info_tab.dart';

enum _PopupAction { vitals, reports, careTasks, circleMember }

class PatientProfilePage extends StatelessWidget {
  final String patientId;
  const PatientProfilePage({super.key, required this.patientId});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PatientBloc>(
      create: (BuildContext context) =>
          PatientBloc(patientRepository: context.read<PatientService>())
            ..add(FetchPatient(patientId: patientId)),
      child: _CareRecipientProfilePageView(patientId),
    );
  }
}

class _CareRecipientProfilePageView extends StatelessWidget {
  final String patientId;
  const _CareRecipientProfilePageView(this.patientId, {Key? key})
      : super(key: key);

  void _onFetchPatient(BuildContext context) {
    context.read<PatientBloc>().add(FetchPatient(patientId: patientId));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<PatientBloc, PatientState>(
        builder: (BuildContext context, PatientState state) {
          if (state is PatientLoadError) {
            return LoadErrorWidget(
              message: state.error,
              onRetry: () => _onFetchPatient(context),
            );
          } else if (state is PatientLoadSuccess) {
            final Patient? patient = state.patient;
            if (patient == null) {
              return const NoDataWidget('Care Recipient not found.');
            } else {
              return _PageContent(patient);
            }
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _PageContent extends StatefulWidget {
  final Patient patient;

  const _PageContent(this.patient, {Key? key}) : super(key: key);

  @override
  _PageContentState createState() => _PageContentState();
}

class _PageContentState extends State<_PageContent>
    with SingleTickerProviderStateMixin {
  late TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  List<String> _getAilments() {
    return widget.patient.conditions.length < 3
        ? widget.patient.conditions
        : widget.patient.conditions.sublist(0, 3);
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark,
        ),
        actions: <Widget>[
          _PopupButton(widget.patient),
        ],
      ),
      body: Column(
        children: <Widget>[
          SizedBox(height: 9.5.h),
          CircleAvatar(
            radius: 12.5.w,
            backgroundImage: CachedNetworkImageProvider(
              widget.patient.avatar,
            ),
          ),
          SizedBox(height: 2.0.h),
          Text(widget.patient.name, style: theme.titleLarge),
          SizedBox(height: 0.5.h),
          Text(
            widget.patient.address?.shortAddress() ?? kUnknownAddress,
            style: theme.bodyLarge?.copyWith(fontWeight: FontWeight.w600),
          ),
          SizedBox(height: 1.0.h),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: _getAilments()
                .map((String condition) => ColoredPill(
                      title: condition,
                      margin: EdgeInsets.symmetric(horizontal: 1.0.w),
                    ))
                .toList(),
          ),
          SizedBox(height: 1.5.h),
          TabBar(
            controller: _controller,
            physics: const NeverScrollableScrollPhysics(),
            labelStyle: TextStyle(
              fontSize: 18.88.sp,
              fontWeight: FontWeight.w500,
            ),
            labelPadding: EdgeInsets.only(left: 8.5.w, right: 8.5.w),
            indicatorColor: Theme.of(context).primaryColor,
            labelColor: Colors.black,
            indicatorSize: TabBarIndicatorSize.tab,
            unselectedLabelColor: Colors.grey,
            isScrollable: true,
            tabs: const <Widget>[
              Tab(text: 'About'),
              Tab(text: 'Care Tasks'),
              Tab(text: 'Circle'),
            ],
          ),
          SizedBox(
            height: 52.5.h,
            child: TabBarView(
              physics: const BouncingScrollPhysics(),
              controller: _controller,
              children: <Widget>[
                PatientInfoTab(patient: widget.patient),
                CareTaskTab(patient: widget.patient),
                PatientCircleTab(
                  patient: widget.patient,
                  members: widget.patient.circle.keys.toList(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _PopupButton extends StatefulWidget {
  final Patient patient;
  const _PopupButton(this.patient, {Key? key}) : super(key: key);

  @override
  State<_PopupButton> createState() => _PopupButtonState();
}

class _PopupButtonState extends State<_PopupButton> {
  Future<void> _handlePopupAction(_PopupAction action, AppUser user) async {
    switch (action) {
      case _PopupAction.vitals:
        Navigator.of(context)
            .pushNamed(kPatientRecordsPageRoute, arguments: widget.patient);
        break;
      case _PopupAction.reports:
        Navigator.of(context)
            .pushNamed(kPatientReportsPageRoute, arguments: widget.patient);
        break;
      case _PopupAction.careTasks:
        Navigator.of(context)
            .pushNamed(kCareTasksPageRoute, arguments: widget.patient);
        break;
      case _PopupAction.circleMember:
        Navigator.pushNamed<void>(
          context,
          kAddCircleMemberPageRoute,
          arguments: widget.patient.uid,
        );

        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);

    return PopupMenuButton<_PopupAction>(
      icon: Icon(Icons.more_vert_outlined, size: 3.8.h),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0)),
      onSelected: (_PopupAction action) => _handlePopupAction(action, user),
      itemBuilder: (BuildContext context) {
        return <PopupMenuEntry<_PopupAction>>[
          const PopupMenuItem<_PopupAction>(
            value: _PopupAction.vitals,
            child: IconMenuItem(
                icon: FeatherIcons.activity, title: 'Health Vitals'),
          ),
          const PopupMenuDivider(),
          const PopupMenuItem<_PopupAction>(
            value: _PopupAction.careTasks,
            child: IconMenuItem(icon: Boxicons.bx_task, title: 'Care Tasks'),
          ),
          const PopupMenuDivider(),
          const PopupMenuItem<_PopupAction>(
            value: _PopupAction.reports,
            child:
                IconMenuItem(icon: Boxicons.bx_notepad, title: 'Care Reports'),
          ),
          const PopupMenuDivider(),
          PopupMenuItem<_PopupAction>(
            enabled: widget.patient.nextOfKin.contains(user.uid),
            value: _PopupAction.circleMember,
            child: IconMenuItem(
              title: 'Add Circle Member',
              icon: Icons.group_add_outlined,
              enabled: widget.patient.nextOfKin.contains(user.uid),
            ),
          ),
        ];
      },
    );
  }
}

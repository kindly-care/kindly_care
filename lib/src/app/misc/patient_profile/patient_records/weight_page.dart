import 'package:collection/collection.dart';
import 'package:dart_date/dart_date.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../data/services/services.dart';
import '../../../widgets/subscription_expired.dart';
import 'bloc/patient_records_bloc.dart';

class WeightPage extends StatelessWidget {
  final Patient patient;
  const WeightPage({super.key, required this.patient});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PatientRecordsBloc>(
      create: (BuildContext context) {
        return PatientRecordsBloc(
          vitalsRepository: context.read<VitalsService>(),
        )..add(FetchWeightRecords(
            month: DateTime.now().month,
            patientId: patient.uid,
          ));
      },
      child: _WeightPageView(patient: patient),
    );
  }
}

class _WeightPageView extends StatefulWidget {
  final Patient patient;
  const _WeightPageView({Key? key, required this.patient}) : super(key: key);

  @override
  State<_WeightPageView> createState() => _WeightPageViewState();
}

class _WeightPageViewState extends State<_WeightPageView> {
  int _month = DateTime.now().month;

  void _onFetchWeightRecords() {
    context
        .read<PatientRecordsBloc>()
        .add(FetchWeightRecords(month: _month, patientId: widget.patient.uid));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        title: const Text('Body Weight'),
        actions: <Widget>[
          MonthPopupButton(
            icon: Icons.today_outlined,
            onChanged: (int month) {
              setState(() => _month = month);
              _onFetchWeightRecords();
            },
          ),
        ],
      ),
      body: BlocBuilder<PatientRecordsBloc, PatientRecordsState>(
        builder: (BuildContext context, PatientRecordsState state) {
          if (state is PatientRecordsLoadError) {
            return LoadErrorWidget(
                message: state.error, onRetry: _onFetchWeightRecords);
          } else if (state is PatientWeightRecordsLoadSuccess) {
            if (widget.patient.subscriptionEnds.isPast) {
              return SubscriptionExpired(
                patient: widget.patient,
                content: 'Weight records',
                detail: 'Weight records currently not available.',
              );
            } else {
              return _WeightTableView(month: _month, records: state.records);
            }
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _WeightTableView extends StatelessWidget {
  final int month;
  final List<WeightRecord> records;
  const _WeightTableView({Key? key, required this.month, required this.records})
      : super(key: key);

  static List<String> columns = <String>[
    'Care Pro',
    'Date',
    'Weight (kg)',
  ];

  @override
  Widget build(BuildContext context) {
    final DateTime date = DateTime.now().setMonth(month);
    final TextTheme theme = Theme.of(context).textTheme;
    final String monthYear = DateFormat.yMMMM('en_GB').format(date);
    if (records.isEmpty) {
      return NoDataWidget('No Weight records found for $monthYear.');
    } else {
      return InteractiveViewer(
        alignPanAxis: true,
        constrained: false,
        scaleEnabled: false,
        child: DataTable(
          columnSpacing: 19.5.w,
          horizontalMargin: 2.5.w,
          columns: columns
              .mapIndexed((int index, String item) => DataColumn(
                    label: Text(
                      item,
                      style: theme.bodyMedium?.copyWith(
                        fontWeight: FontWeight.w600,
                        color: Colors.black.withOpacity(0.7),
                      ),
                    ),
                    numeric: index == 2,
                  ))
              .toList(),
          rows: records.map((WeightRecord record) {
            final String weight = '${record.weight}';

            final String date =
                DateFormat('dd/MM', 'en_GB').add_Hm().format(record.takenAt);

            final String name = record.careProviderName.firstNameLastInitial();

            final List<String> cells = <String>[
              name,
              date,
              weight,
            ];
            return DataRow(
              cells: cells
                  .map((String item) =>
                      DataCell(Text(item, style: theme.bodyMedium)))
                  .toList(),
            );
          }).toList(),
        ),
      );
    }
  }
}

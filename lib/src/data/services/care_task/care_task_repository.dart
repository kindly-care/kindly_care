import 'package:kindly_components/kindly_components.dart';

abstract class CareTaskRepository {
  /// fetches [Patients]'s care tasks. Throws [FetchDataException].
  Stream<List<CareTask>> fetchCareTasks(String patientId);

  /// adds/updates [CareTask]. Throws [UpdateDataException].
  Future<void> createCareTask(CareTask task);

  /// removes [CareTask]. Throws [UpdateDataException].
  Future<void> removeCareTask(CareTask task);
}

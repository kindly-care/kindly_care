import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:uuid/uuid.dart';

import '../../data/services/services.dart';

part 'care_provider_event.dart';
part 'care_provider_state.dart';

class CareProviderBloc extends Bloc<CareProviderEvent, CareProviderState> {
  final PatientRepository _patientRepository;
  final NotificationRepository _notificationRepository;
  final CareProviderRepository _careProviderRepository;

  CareProviderBloc({
    required PatientRepository patientRepository,
    required NotificationRepository notificationRepository,
    required CareProviderRepository careProviderRepository,
  })  : _patientRepository = patientRepository,
        _notificationRepository = notificationRepository,
        _careProviderRepository = careProviderRepository,
        super(CareProviderInitial()) {
    on<AssignCareProvider>(_onAssignCareProvider);
    on<DismissCareProvider>(_onDismissCareProvider);
    on<FetchAllCareProviders>(_onFetchAllCareProviders);
    on<FetchAssignedCareProviders>(_onFetchAssignedCareProviders);
  }

  Future<void> _onAssignCareProvider(
      AssignCareProvider event, Emitter<CareProviderState> emit) async {
    emit(AssignCareProviderInProgress());
    final Patient patient = event.patient;

    try {
      final CareProvider? careProvider =
          await _careProviderRepository.fetchCareProviderByEmail(event.email);

      if (careProvider == null) {
        emit(const AssignCareProviderError(
            error:
                'The provided email address is not associated to any Care Provider. Please check the email address and try again.'));
      } else {
        final DateTime now = DateTime.now();
        final AssignedCareProvider assignedCareProvider = AssignedCareProvider(
          assignedDate: now,
          shift: event.shift,
          patientId: patient.uid,
          careProviderId: careProvider.uid,
          careProviderName: careProvider.name,
          careProviderPhone: careProvider.phone,
        );

        final AssignedPatient assignedPatient = AssignedPatient(
          assignedDate: now,
          shift: event.shift,
          patientId: patient.uid,
          patientName: patient.name,
          careProviderId: careProvider.uid,
        );

        await _careProviderRepository.assignCareProvider(
          client: event.client,
          assignedPatient: assignedPatient,
          assignedCareProvider: assignedCareProvider,
        );

        await _dispatchPushNotification(
            event.client, patient, careProvider.uid);

        emit(const AssignCareProviderSuccess(message: 'Care Provider added'));
      }
    } catch (e) {
      emit(AssignCareProviderError(error: e.toString()));
    }
  }

  Future<void> _onDismissCareProvider(
      DismissCareProvider event, Emitter<CareProviderState> emit) async {
    emit(DismissCareProviderInProgress());
    try {
      await _patientRepository.removeCareProvider(
          patient: event.patient, careProvider: event.careProvider);
      emit(const DismissCareProviderSuccess(
          message: 'Care Provider dismissed.'));
    } on UpdateDataException catch (e) {
      emit(DismissCareProviderError(error: e.toString()));
    }
  }

  Future<void> _onFetchAllCareProviders(
      FetchAllCareProviders event, Emitter<CareProviderState> emit) async {
    emit(CareProvidersLoading());

    try {
      final List<CareProvider> careProviders =
          await _careProviderRepository.fetchAllCareProviders();
      emit(CareProvidersLoadSuccess(careProviders: careProviders));
    } on FetchDataException catch (e) {
      emit(CareProvidersLoadError(error: e.toString()));
    }
  }

  Future<void> _onFetchAssignedCareProviders(
      FetchAssignedCareProviders event, Emitter<CareProviderState> emit) async {
    emit(CareProvidersLoading());

    try {
      final List<CareProvider> careProviders = await _patientRepository
          .fetchAssignedCareProviders(event.providerIds);
      emit(CareProvidersLoadSuccess(careProviders: careProviders));
    } on FetchDataException catch (e) {
      emit(CareProvidersLoadError(error: e.toString()));
    }
  }

  Future<void> _dispatchPushNotification(
      AppUser client, Patient patient, String agentId) async {
    final DateTime now = DateTime.now();

    try {
      final PushNotification notification = PushNotification(
        text:
            'You have been assigned a new patient, ${patient.name} by ${client.name}',
        title: 'New Patient',
        createdAt: now,
        authorId: client.uid,
        id: const Uuid().v4(),
        authorName: client.name,
        authorPhone: client.phone,
        recipients: <String>[agentId],
      );

      await _notificationRepository.sendPushNotification(notification);
    } on Exception catch (_) {
      rethrow;
    }
  }
}

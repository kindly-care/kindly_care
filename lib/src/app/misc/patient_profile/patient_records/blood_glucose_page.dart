import 'package:collection/collection.dart';
import 'package:dart_date/dart_date.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../data/services/services.dart';

import '../../../widgets/subscription_expired.dart';
import 'bloc/patient_records_bloc.dart';

class BloodGlucosePage extends StatelessWidget {
  final Patient patient;

  const BloodGlucosePage({super.key, required this.patient});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PatientRecordsBloc>(
      create: (BuildContext context) {
        return PatientRecordsBloc(
          vitalsRepository: context.read<VitalsService>(),
        )..add(FetchBloodGlucoseRecords(
            patientId: patient.uid, month: DateTime.now().month));
      },
      child: _BloodGlucosePageView(patient: patient),
    );
  }
}

class _BloodGlucosePageView extends StatefulWidget {
  final Patient patient;
  const _BloodGlucosePageView({Key? key, required this.patient})
      : super(key: key);

  @override
  State<_BloodGlucosePageView> createState() => _BloodGlucosePageViewState();
}

class _BloodGlucosePageViewState extends State<_BloodGlucosePageView> {
  int _month = DateTime.now().month;
  late TextEditingController _bloodSugarController;

  @override
  void initState() {
    super.initState();
    _bloodSugarController = TextEditingController();
  }

  @override
  void dispose() {
    _bloodSugarController.dispose();
    super.dispose();
  }

  void _onFetchBloodGlucose() {
    context.read<PatientRecordsBloc>().add(
        FetchBloodGlucoseRecords(patientId: widget.patient.uid, month: _month));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        title: const Text('Blood Glucose'),
        actions: <Widget>[
          MonthPopupButton(
            icon: Icons.today_outlined,
            onChanged: (int month) {
              setState(() => _month = month);
              _onFetchBloodGlucose();
            },
          ),
        ],
      ),
      body: BlocBuilder<PatientRecordsBloc, PatientRecordsState>(
        builder: (BuildContext context, PatientRecordsState state) {
          if (state is PatientRecordsLoadError) {
            return LoadErrorWidget(
                message: state.error, onRetry: _onFetchBloodGlucose);
          } else if (state is PatientBloodGlucoseRecordsLoadSuccess) {
            if (widget.patient.subscriptionEnds.isPast) {
              return SubscriptionExpired(
                patient: widget.patient,
                content: 'Blood Glucose records',
                detail: 'Blood Glucose records currently not available.',
              );
            } else {
              return _BloodSugarTableView(
                  records: state.records, month: _month);
            }
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _BloodSugarTableView extends StatelessWidget {
  final int month;
  final List<BloodGlucoseRecord> records;
  const _BloodSugarTableView(
      {Key? key, required this.month, required this.records})
      : super(key: key);

  static List<String> columns = <String>[
    'Care Pro',
    'Date',
    'Blood Glucose',
  ];

  @override
  Widget build(BuildContext context) {
    final DateTime date = DateTime.now().setMonth(month);
    final TextTheme theme = Theme.of(context).textTheme;
    final String monthYear = DateFormat.yMMMM('en_GB').format(date);
    if (records.isEmpty) {
      return NoDataWidget('No Blood Glucose records found for $monthYear.');
    } else {
      return InteractiveViewer(
        alignPanAxis: true,
        constrained: false,
        scaleEnabled: false,
        child: DataTable(
          columnSpacing: 17.5.w,
          horizontalMargin: 2.5.w,
          columns: columns
              .mapIndexed((int index, String item) => DataColumn(
                    label: Text(
                      item,
                      style: theme.bodyMedium?.copyWith(
                        fontWeight: FontWeight.w600,
                        color: Colors.black.withOpacity(0.7),
                      ),
                    ),
                    numeric: index == 2,
                  ))
              .toList(),
          rows: records.map((BloodGlucoseRecord record) {
            final String bloodSugar = '${record.bloodGlucose} mmol/L';

            final String date =
                DateFormat('dd/MM', 'en_GB').add_jm().format(record.takenAt);

            final String name = record.careProviderName.firstNameLastInitial();

            final List<String> cells = <String>[
              name,
              date,
              bloodSugar,
            ];
            return DataRow(
              cells: cells
                  .map((String item) =>
                      DataCell(Text(item, style: theme.bodyMedium)))
                  .toList(),
            );
          }).toList(),
        ),
      );
    }
  }
}

import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class NoTaskTile extends StatelessWidget {
  final String content;
  const NoTaskTile(this.content, {super.key});

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return ListTile(
      contentPadding: EdgeInsets.symmetric(horizontal: 2.0.w),
      title: Text(
        content,
        style: theme.titleMedium?.copyWith(fontSize: 17.68.sp),
      ),
    );
  }
}

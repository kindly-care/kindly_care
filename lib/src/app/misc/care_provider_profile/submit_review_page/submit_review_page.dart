import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../data/services/care_provider/care_provider_service.dart';
import '../care_provider_reviews_tab/bloc/review_bloc.dart';

class SubmitReviewPage extends StatelessWidget {
  final CareProvider careProvider;
  const SubmitReviewPage({super.key, required this.careProvider});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ReviewBloc>(
      create: (BuildContext context) => ReviewBloc(
        careProviderRepository: context.read<CareProviderService>(),
      ),
      child: _SubmitReviewPageView(careProvider),
    );
  }
}

class _SubmitReviewPageView extends StatefulWidget {
  final CareProvider careProvider;

  const _SubmitReviewPageView(this.careProvider, {Key? key}) : super(key: key);

  @override
  State<_SubmitReviewPageView> createState() => _SubmitReviewPageViewState();
}

class _SubmitReviewPageViewState extends State<_SubmitReviewPageView> {
  double _rating = 1;
  late TextEditingController _reviewController;

  @override
  void initState() {
    super.initState();
    _reviewController = TextEditingController();
  }

  Future<void> _onSubmitReview(AppUser user) async {
    final Review review = Review(
      name: user.name,
      rating: _rating,
      phone: user.phone,
      email: user.email,
      authorId: user.uid,
      createdAt: DateTime.now(),
      text: _reviewController.text,
      careProviderId: widget.careProvider.uid,
    );

    context.read<ReviewBloc>().add(SubmitReview(review: review));
  }

  @override
  Widget build(BuildContext context) {
    final String name = widget.careProvider.name.firstName();
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final bool isLoading = context.select((ReviewBloc bloc) => bloc.state)
        is SubmitReviewInProgress;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.careProvider.name),
      ),
      body: LoadingOverlay(
        isLoading: isLoading,
        child: BlocListener<ReviewBloc, ReviewState>(
          listenWhen: (ReviewState previous, ReviewState current) {
            return current is SubmitReviewSuccess ||
                current is SubmitReviewError;
          },
          listener: (BuildContext context, ReviewState state) {
            if (state is SubmitReviewError) {
              _showReviewErrorDialog(state.error, user);
            } else if (state is SubmitReviewSuccess) {
              Navigator.pop(context);
              successSnackbar(context, state.message);
            }
          },
          child: ListView(
            padding: EdgeInsets.all(3.0.w),
            physics: const BouncingScrollPhysics(),
            children: <Widget>[
              SizedBox(height: 2.0.h),
              Center(
                child: RatingBar.builder(
                  initialRating: _rating,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemSize: 5.5.h,
                  minRating: 1.0,
                  itemPadding: EdgeInsets.symmetric(horizontal: 1.5.w),
                  unratedColor: Colors.grey.shade400,
                  itemBuilder: (BuildContext context, _) =>
                      const Icon(Icons.star, color: Color(0xFFFE7404)),
                  onRatingUpdate: (double rating) => _rating = rating,
                ),
              ),
              SizedBox(height: 4.0.h),
              MaterialTextField(
                minLines: 3,
                maxLines: null,
                labelText: '',
                hintText: 'Describe your experience with $name..',
                onChanged: (_) => setState(() {}),
                controller: _reviewController,
                textInputAction: TextInputAction.newline,
              ),
              SizedBox(height: 18.0.h),
              ActionButton(
                title: 'Post Review',
                onPressed: () => _onSubmitReview(user),
                enabled: _reviewController.text.isNotEmpty,
              ),
              SizedBox(height: 2.0.h),
            ],
          ),
        ),
      ),
    );
  }

  void _showReviewErrorDialog(String message, AppUser user) {
    showTwoButtonDialog(
      context,
      title: 'Error',
      content: message,
      buttonText1: 'Exit',
      buttonText2: 'Retry',
      onPressed1: () => Navigator.pop(context),
      onPressed2: () {
        Navigator.pop(context);
        _onSubmitReview(user);
      },
    );
  }

  @override
  void dispose() {
    _reviewController.dispose();
    super.dispose();
  }
}

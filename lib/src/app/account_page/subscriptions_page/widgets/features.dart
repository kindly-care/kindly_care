import 'package:flutter/material.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class Features extends StatelessWidget {
  const Features({super.key});

  static final Map<String, IconData> _features = <String, IconData>{
    'Time Logs': Icons.pending_actions_outlined,
    'Care Reports': Icons.assignment_turned_in_outlined,
    'Health Records': Boxicons.bx_clipboard,
    'Unlimited Family Accounts': Icons.groups_outlined,
    'Help Support Further App Development': Boxicons.bx_donate_heart,
  };

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Column(
      children: _features.entries
          .map((MapEntry<String, IconData> entry) => Container(
                margin: EdgeInsets.symmetric(horizontal: 4.8.w),
                child: Row(
                  children: <Widget>[
                    Icon(
                      entry.value,
                      size: 3.8.h,
                      color: Colors.black.withOpacity(0.7),
                    ),
                    Padding(
                      padding: EdgeInsets.all(2.5.h),
                      child: Text(
                        entry.key,
                        maxLines: 2,
                        style: theme.bodyMedium,
                      ),
                    ),
                  ],
                ),
              ))
          .toList(),
    );
  }
}

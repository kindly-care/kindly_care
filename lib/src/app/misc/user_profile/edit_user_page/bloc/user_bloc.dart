import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../../../data/services/user/user_repository.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  final UserRepository _userRepository;

  UserBloc({required UserRepository userRepository})
      : _userRepository = userRepository,
        super(UserInitial()) {
    on<UpdateUser>(_onUpdateUser);
  }

  Future<void> _onUpdateUser(UpdateUser event, Emitter<UserState> emit) async {
    emit(UserLoadingState());

    try {
      await _userRepository.updateUser(event.user);
      emit(const UserUpdateSuccess(message: 'Changes successfully saved.'));
    } on UpdateDataException catch (e) {
      emit(UserUpdateError(error: e.toString()));
    }
  }
}

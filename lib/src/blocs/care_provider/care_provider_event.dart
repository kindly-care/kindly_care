part of 'care_provider_bloc.dart';

abstract class CareProviderEvent extends Equatable {
  const CareProviderEvent();

  @override
  List<Object> get props => <Object>[];
}

class FetchAllCareProviders extends CareProviderEvent {
  const FetchAllCareProviders();

  @override
  List<Object> get props => <Object>[];
}

class AssignCareProvider extends CareProviderEvent {
  final String shift;
  final String email;
  final AppUser client;
  final Patient patient;

  const AssignCareProvider({
    required this.shift,
    required this.email,
    required this.client,
    required this.patient,
  });

  @override
  List<Object> get props => <Object>[shift, email, client, patient];
}

class DismissCareProvider extends CareProviderEvent {
  final Patient patient;
  final CareProvider careProvider;

  const DismissCareProvider(
      {required this.careProvider, required this.patient});

  @override
  List<Object> get props => <Object>[careProvider, patient];
}

class FetchAssignedCareProviders extends CareProviderEvent {
  final List<String> providerIds;
  const FetchAssignedCareProviders({required this.providerIds});

  @override
  List<Object> get props => <Object>[providerIds];
}

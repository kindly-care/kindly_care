import 'package:dart_date/dart_date.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../common/constants/constants.dart';
import 'item_tile.dart';

class Meals extends StatelessWidget {
  final Patient patient;
  const Meals({super.key, required this.patient});

  String _getBreakfast() {
    final String? breakfast = patient.meals[kBreakfast]?.meal;
    if (breakfast == null || breakfast.isEmpty) {
      return 'No meal entry';
    } else if (patient.subscriptionEnds.isPast) {
      return 'Data currently not available.';
    } else {
      return breakfast;
    }
  }

  String? _getBreakfastTime() {
    final DateTime? time = patient.meals[kBreakfast]?.time;
    if (time == null) {
      return null;
    } else {
      return DateFormat('EEE, d MMM H:m', 'en_GB').format(time);
    }
  }

  String _getLunch() {
    final String? lunch = patient.meals[kLunch]?.meal;
    if (lunch == null || lunch.isEmpty) {
      return 'No meal entry';
    } else if (patient.subscriptionEnds.isPast) {
      return 'Data currently not available.';
    } else {
      return lunch;
    }
  }

  String? _getLunchTime() {
    final DateTime? time = patient.meals[kLunch]?.time;
    if (time == null) {
      return null;
    } else {
      return DateFormat('EEE, d MMM H:m', 'en_GB').format(time);
    }
  }

  String _getSupper() {
    final String? supper = patient.meals[kSupper]?.meal;
    if (supper == null || supper.isEmpty) {
      return 'No meal entry';
    } else if (patient.subscriptionEnds.isPast) {
      return 'Data currently not available.';
    } else {
      return supper;
    }
  }

  String? _getSupperTime() {
    final DateTime? time = patient.meals[kSupper]?.time;
    if (time == null) {
      return null;
    } else {
      return DateFormat('EEE, d MMM H:m', 'en_GB').format(time);
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final Color color = Colors.black.withOpacity(0.65);

    return Theme(
      data: Theme.of(context).copyWith(
        dividerColor: Colors.transparent,
      ),
      child: ExpansionTile(
        initiallyExpanded: true,
        title: Text('Meals', style: theme.titleMedium),
        iconColor: color,
        textColor: color,
        collapsedIconColor: color,
        collapsedTextColor: color,
        children: <Widget>[
          ItemTile(
            itemType: 'Breakfast',
            item: _getBreakfast(),
            time: _getBreakfastTime(),
          ),
          SizedBox(height: 1.0.h),
          ItemTile(
            itemType: 'Lunch',
            item: _getLunch(),
            time: _getLunchTime(),
          ),
          SizedBox(height: 1.0.h),
          ItemTile(
            itemType: 'Supper',
            item: _getSupper(),
            time: _getSupperTime(),
          ),
        ],
      ),
    );
  }
}

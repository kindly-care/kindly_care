import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:collection/collection.dart';
import 'package:dart_date/dart_date.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:uuid/uuid.dart';

import '../../../../blocs/location/location_bloc.dart';
import '../../../../blocs/patient/patient_bloc.dart';
import '../../../../common/constants/constants.dart';
import '../../../../data/local/local.dart';
import '../../../../data/services/services.dart';
import '../widgets/map_widget.dart';

class AddPatientPage extends StatelessWidget {
  final Patient? patient;
  const AddPatientPage({super.key, this.patient});

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    return MultiBlocProvider(
      providers: <BlocProvider<dynamic>>[
        BlocProvider<PatientBloc>(
          create: (BuildContext context) => PatientBloc(
            patientRepository: context.read<PatientService>(),
          ),
        ),
        BlocProvider<LocationBloc>(
          create: (BuildContext context) => LocationBloc(
            locationRepository: context.read<LocationService>(),
          ),
        ),
      ],
      child: _AddPatientPageView(user: user, patient: patient),
    );
  }
}

class _AddPatientPageView extends StatefulWidget {
  final AppUser user;
  final Patient? patient;
  const _AddPatientPageView({Key? key, this.patient, required this.user})
      : super(key: key);

  @override
  State<_AddPatientPageView> createState() => _AddPatientPageViewState();
}

class _AddPatientPageViewState extends State<_AddPatientPageView> {
  File? _avatar;
  DateTime? _dob;
  LatLng? _location;
  late TextEditingController _nameController;
  late TextEditingController _genderController;
  late TextEditingController _dobController;
  final GlobalKey<FormState> _formKey = GlobalKey();
  late TextEditingController _maritalStatusController;
  late TextEditingController _phoneController;
  late TextEditingController _emailController;
  late TextEditingController _addressNumberController;
  late TextEditingController _streetNameController;
  late TextEditingController _suburbController;
  late TextEditingController _cityController;
  late TextEditingController _conditionsController;
  late TextEditingController _mobilityController;
  late TextEditingController _allergiesController;
  late TextEditingController _bioController;
  late TextEditingController _relationController;

  @override
  void initState() {
    super.initState();
    _dob = widget.patient?.dob;
    final double? latitude = widget.patient?.address?.latitude;
    final double? longitude = widget.patient?.address?.longitude;
    if (latitude != null && longitude != null) {
      _location = LatLng(latitude, longitude);
    }

    _nameController = TextEditingController(text: widget.patient?.name);
    _genderController = TextEditingController(text: widget.patient?.gender);
    _dobController = TextEditingController(
      text: widget.patient?.dob == null
          ? ''
          : DateFormat.yMMMMd('en_GB').format(widget.patient!.dob!),
    );
    _maritalStatusController =
        TextEditingController(text: widget.patient?.maritalStatus);
    _phoneController = TextEditingController(text: widget.patient?.phone);
    _emailController = TextEditingController(text: widget.patient?.email);
    _addressNumberController =
        TextEditingController(text: widget.patient?.address?.number.toString());
    _streetNameController =
        TextEditingController(text: widget.patient?.address?.streetName);
    _suburbController =
        TextEditingController(text: widget.patient?.address?.suburb);
    _cityController =
        TextEditingController(text: widget.patient?.address?.city);
    if (widget.patient != null) {
      final String conditions = widget.patient!.conditions.join(', ');
      _conditionsController = TextEditingController(text: conditions);
    } else {
      _conditionsController = TextEditingController();
    }
    _mobilityController = TextEditingController(text: widget.patient?.mobility);
    _allergiesController =
        TextEditingController(text: widget.patient?.allergies.join(', '));

    _bioController = TextEditingController(text: widget.patient?.bio);

    final String? relation = widget.patient?.circle.entries
        .firstWhereOrNull(
            (MapEntry<String, String> entry) => entry.key == widget.user.uid)
        ?.value;
    _relationController = TextEditingController(text: relation);
  }

  @override
  void dispose() {
    _nameController.dispose();
    _genderController.dispose();
    _dobController.dispose();
    _maritalStatusController.dispose();
    _phoneController.dispose();
    _emailController.dispose();
    _addressNumberController.dispose();
    _streetNameController.dispose();
    _suburbController.dispose();
    _cityController.dispose();
    _conditionsController.dispose();
    _mobilityController.dispose();
    _allergiesController.dispose();
    _bioController.dispose();
    _relationController.dispose();
    super.dispose();
  }

  Future<void> _onSaveButtonTap() async {
    final FormState form = _formKey.currentState!;
    final bool isValid = form.validate();
    final Patient? patient = _buildPatient();

    if (isValid && patient != null) {
      context.read<PatientBloc>().add(
            AddPatient(
              patient: patient,
              isNewPatient: widget.patient == null,
            ),
          );
    } else {
      showToast(message: 'Please fill all fields');
    }
  }

  Patient? _buildPatient() {
    final String uid = widget.patient?.uid ?? const Uuid().v4();
    final Address address = _buildAddress();
    if (address.latitude == null || address.longitude == null) {
      showToast(message: 'Please pick address on the map');
      return null;
    } else {
      return Patient(
        uid: uid,
        localAvatar: _avatar,
        name: _nameController.text,
        dob: widget.patient?.dob ?? _dob!,
        gender: _genderController.text,
        maritalStatus: _maritalStatusController.text,
        phone: _phoneController.text,
        email: _emailController.text,
        avatar: widget.patient?.avatar ?? kDefaultAvatar,
        address: _buildAddress(),
        conditions: _conditionsController.text.split(', '),
        mobility: _mobilityController.text,
        allergies: _allergiesController.text.isNotEmpty
            ? _allergiesController.text.split(', ')
            : <String>[],
        circle: widget.patient?.circle ??
            <String, String>{widget.user.uid: _relationController.text},
        nextOfKin: widget.patient?.nextOfKin ?? <String>[widget.user.uid],
        circleMembers:
            widget.patient?.circleMembers ?? <String>[widget.user.uid],
        joinedDate: widget.patient?.joinedDate ?? DateTime.now(),
        subscriptionEnds:
            widget.patient?.subscriptionEnds ?? DateTime.now().addDays(15),
        assignedCareProviders: widget.patient?.assignedCareProviders ??
            <String, AssignedCareProvider>{},
        contacts: widget.patient?.contacts ?? <String>[widget.user.uid],
        bio: _bioController.text,
        streamToken: 'streamToken',
        messageToken: 'messageToken',
      );
    }
  }

  Address _buildAddress() {
    return Address(
      number: int.tryParse(_addressNumberController.text) ?? 0,
      streetName: _streetNameController.text,
      suburb: _suburbController.text,
      city: _cityController.text,
      latitude: _location?.latitude,
      longitude: _location?.longitude,
    );
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final bool isLoading = context.select((PatientBloc bloc) => bloc.state)
        is AddPatientInProgress;

    return Scaffold(
      body: BlocListener<PatientBloc, PatientState>(
        listener: (BuildContext cxt, PatientState state) {
          if (state is AddPatientError) {
            retrySnackbar(
              cxt,
              state.error,
              onRetry: _onSaveButtonTap,
            );
          } else if (state is AddPatientSuccess) {
            Navigator.pop(context);
            successSnackbar(context, state.message);
          }
        },
        child: LoadingOverlay(
          isLoading: isLoading,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 3.0.w),
            child: CustomScrollView(
              physics: const BouncingScrollPhysics(),
              slivers: <Widget>[
                const SliverAppBar(
                  pinned: false,
                  floating: true,
                  elevation: 0.0,
                  title: Text('Care Recipient'),
                ),
                Form(
                  key: _formKey,
                  child: SliverList(
                    delegate: SliverChildListDelegate(
                      <Widget>[
                        Center(
                          child: SizedBox(
                            height: 16.5.h,
                            width: 27.5.w,
                            child: Stack(
                              children: <Widget>[
                                _avatar == null
                                    ? CircleAvatar(
                                        radius: 12.5.w,
                                        backgroundImage:
                                            CachedNetworkImageProvider(
                                          widget.patient?.avatar ??
                                              kDefaultAvatar,
                                        ),
                                      )
                                    : LocalBorderAvatar(
                                        imagePath: _avatar!,
                                        radius: 12.5.w,
                                        borderWidth: 0.0,
                                        borderColor: Colors.white,
                                      ),
                                Positioned(
                                  top: 9.1.h,
                                  left: 12.0.w,
                                  child: ElevatedButton.icon(
                                    style: ElevatedButton.styleFrom(
                                      elevation: 0.0,
                                      padding: EdgeInsets.only(left: 2.1.w),
                                      shape: const CircleBorder(),
                                    ),
                                    onPressed: () async {
                                      _avatar = await ImageService.getImage();
                                      setState(() {});
                                    },
                                    label: const Text(''),
                                    icon: Icon(
                                      Icons.photo_camera_outlined,
                                      size: 3.8.h,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 2.0.h),
                        MaterialTextField(
                          controller: _nameController,
                          labelText: 'Name',
                          hintText: 'Enter full name',
                          onChanged: (_) => setState(() {}),
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 2.5.h),
                        DropdownTextField(
                          labelText: 'Gender',
                          items: kGenderOptions,
                          hintText: 'Select Gender',
                          controller: _genderController,
                          onChanged: (_) => setState(() {}),
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          readOnly: true,
                          focusNode: AlwaysDisabledFocusNode(),
                          onTap: () {
                            showDateTimeSelector(
                              context,
                              initialDate: _dob,
                              title: 'Date Of Birth',
                              controller: _dobController,
                              mode: CupertinoDatePickerMode.date,
                              format: DateFormat.yMMMMd('en_GB'),
                              onDateChanged: (DateTime date) => _dob = date,
                            );
                          },
                          labelText: 'Date Of Birth',
                          hintText: 'Select date',
                          controller: _dobController,
                          onChanged: (_) => setState(() {}),
                          prefixIcon: const Icon(Icons.event_outlined),
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 2.5.h),
                        DropdownTextField(
                          items: kMaritalStatus,
                          labelText: 'Marital Status',
                          hintText: 'Enter marital status',
                          controller: _maritalStatusController,
                          onChanged: (_) => setState(() {}),
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 3.0.h),
                        const TextHeader('Contact Details'),
                        SizedBox(height: 2.0.h),
                        MaterialTextField(
                          labelText: 'Phone',
                          controller: _phoneController,
                          hintText: 'Enter phone number',
                          onChanged: (_) => setState(() {}),
                          textInputType: TextInputType.phone,
                          validator: (String? val) => val.simpleValidateNumber,
                        ),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          isOptional: true,
                          labelText: 'Email',
                          controller: _emailController,
                          hintText: 'Enter email address',
                          onChanged: (_) => setState(() {}),
                          textInputType: TextInputType.emailAddress,
                        ),
                        SizedBox(height: 3.0.h),
                        const TextHeader('Address'),
                        SizedBox(height: 2.0.h),
                        MaterialTextField(
                          labelText: 'Address number',
                          hintText: 'Enter address number',
                          controller: _addressNumberController,
                          onChanged: (_) => setState(() {}),
                          textInputType: TextInputType.number,
                          validator: (String? val) => val.simpleValidateNumber,
                        ),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          labelText: 'Street Name',
                          hintText: 'Enter street name',
                          controller: _streetNameController,
                          onChanged: (_) => setState(() {}),
                          textInputType: TextInputType.streetAddress,
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          labelText: 'Suburb',
                          hintText: 'Enter suburb',
                          controller: _suburbController,
                          onChanged: (_) => setState(() {}),
                          textInputType: TextInputType.streetAddress,
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          labelText: 'City',
                          hintText: 'Enter city',
                          controller: _cityController,
                          onChanged: (_) => setState(() {}),
                          textInputType: TextInputType.streetAddress,
                          validator: (String? val) => val.simpleValidate,
                          onFieldSubmitted: (_) {
                            final String area = _buildAddress().fullAddress();
                            context
                                .read<LocationBloc>()
                                .add(FetchLocationByArea(area: area));
                            FocusManager.instance.primaryFocus?.unfocus();
                          },
                        ),
                        SizedBox(height: 2.0.h),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: TextButton.icon(
                            focusNode: AlwaysDisabledFocusNode(),
                            style: TextButton.styleFrom(
                              minimumSize: Size.zero,
                              padding: EdgeInsets.zero,
                              tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                            ),
                            onPressed: () {
                              showOneButtonDialog(
                                context,
                                title: 'Warning',
                                content:
                                    'Its very important to get the precise address on the map. This will help Care Providers with directions.',
                                buttonText: 'Ok',
                                onPressed: () => Navigator.pop(context),
                              );
                            },
                            icon: Icon(
                              Icons.info_outline,
                              size: 3.2.h,
                            ),
                            label: Text(
                              "Drag or tap marker to Care Recipient's address",
                              style: theme.labelMedium!.copyWith(
                                  color: Theme.of(context).primaryColor),
                            ),
                          ),
                        ),
                        SizedBox(height: 2.0.h),
                        BlocProvider<LocationBloc>.value(
                          value: context.read<LocationBloc>(),
                          child: MapWidget(
                            initialLocation: _location,
                            onLocationChanged: (LatLng location) =>
                                _location = location,
                          ),
                        ),
                        SizedBox(height: 3.0.h),
                        const TextHeader('Health & Wellness'),
                        SizedBox(height: 2.0.h),
                        MaterialTextField(
                          readOnly: true,
                          focusNode: AlwaysDisabledFocusNode(),
                          onTap: () {
                            showMultiSelectBottomSheet(
                              context,
                              items: kConditions,
                              title: 'Medical Conditions',
                              controller: _conditionsController,
                              onSelectedItems: (_) {},
                            );
                          },
                          labelText: 'Medical Conditions',
                          hintText: 'Select medical conditions',
                          controller: _conditionsController,
                          onChanged: (_) => setState(() {}),
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 2.5.h),
                        DropdownTextField(
                          labelText: 'Mobility',
                          items: kMobilityOptions,
                          hintText: 'Select mobility',
                          controller: _mobilityController,
                          onChanged: (_) => setState(() {}),
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          isOptional: true,
                          labelText: 'Allergies',
                          hintText: 'Enter allergies',
                          controller: _allergiesController,
                          onChanged: (_) => setState(() {}),
                          textInputType: TextInputType.text,
                          helperText: 'Separate allergies by comma.',
                        ),
                        SizedBox(height: 2.5.h),
                        DropdownTextField(
                          items: kRelations,
                          labelText: 'Relation',
                          hintText: 'Select relation',
                          controller: _relationController,
                          onChanged: (_) => setState(() {}),
                          validator: (String? val) => val.simpleValidate,
                          helperText:
                              'Your relationship with the care recipient.',
                        ),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          minLines: 3,
                          maxLines: null,
                          isOptional: true,
                          labelText: 'Bio',
                          controller: _bioController,
                          onChanged: (_) => setState(() {}),
                          textInputAction: TextInputAction.newline,
                          hintText:
                              'Enter any additional info about the care recipient.',
                        ),
                        SizedBox(height: 6.5.h),
                        ActionButton(
                          title: 'Save',
                          onPressed: _onSaveButtonTap,
                        ),
                        SizedBox(height: 2.5.h),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

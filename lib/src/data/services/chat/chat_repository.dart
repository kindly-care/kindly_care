abstract class ChatRepository {
  /// send chat notification to device.
  Future<void> sendChatNotification({
    required String message,
    required String sender,
    required String recipientId,
  });

/// fetches StreamChat user token. Throws [FetchDataException]
  Future<String> fetchStreamUserToken();
}

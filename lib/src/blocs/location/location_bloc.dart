import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../common/constants/constants.dart';
import '../../data/local/location/location_repository.dart';

part 'location_event.dart';
part 'location_state.dart';

class LocationBloc extends Bloc<LocationEvent, LocationState> {
  final LocationRepository _locationRepository;
  LocationBloc({required LocationRepository locationRepository})
      : _locationRepository = locationRepository,
        super(const LocationState()) {
    on<FetchLocationByArea>(_onFetchLocationByArea);
  }

  Future<void> _onFetchLocationByArea(
      FetchLocationByArea event, Emitter<LocationState> emit) async {
    emit(const LocationState());

    try {
      final LatLng location =
          await _locationRepository.getPositionFromArea(event.area);
      emit(LocationState(status: LocationStatus.success, location: location));
    } on LocationException catch (e) {
      emit(LocationState(
        message: e.toString(),
        status: LocationStatus.error,
      ));
    }
  }
}

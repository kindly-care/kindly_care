import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kindly_components/kindly_components.dart';

import 'location_repository.dart';

class LocationService implements LocationRepository {
  @override
  Future<LatLng> getCurrentPosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      throw LocationException(
          'Location services are disabled. Please enable location services and try again.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied ||
          permission == LocationPermission.deniedForever) {
        throw LocationException(
            'Denied location permission. Please grant permission and try again.');
      }
    }

    final Position position = await Geolocator.getCurrentPosition();
    return LatLng(position.latitude, position.longitude);
  }

  @override
  Future<double> getDistanceFromCurrentPosition(LatLng destination) async {
    try {
      final LatLng position = await getCurrentPosition();
      final double distMeters = Geolocator.distanceBetween(
        position.latitude,
        position.longitude,
        destination.latitude,
        destination.longitude,
      );

      return distMeters / 1000;
    } on Exception catch (e) {
      throw LocationException(e.toString());
    }
  }

  @override
  Future<LatLng> getPositionFromArea(String area) async {
    try {
      final List<Location> locations =
          await locationFromAddress(area, localeIdentifier: 'en_ZW');
      if (locations.isEmpty) {
        throw LocationException('Failed to get location');
      } else {
        return LatLng(locations.first.latitude, locations.first.longitude);
      }
    } on Exception catch (e) {
      throw LocationException(e.toString());
    }
  }
}

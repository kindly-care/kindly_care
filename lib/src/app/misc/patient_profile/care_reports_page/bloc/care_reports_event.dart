part of 'care_reports_bloc.dart';

abstract class CareReportsEvent extends Equatable {
  const CareReportsEvent();

  @override
  List<Object> get props => <Object>[];
}

class FetchCareReports extends CareReportsEvent {
  final int month;
  final String patientId;
  const FetchCareReports({required this.month, required this.patientId});

  @override
  List<Object> get props => <Object>[month, patientId];
}

class FetchCareReport extends CareReportsEvent {
  final String reportId;
  final String patientId;
  const FetchCareReport({required this.reportId, required this.patientId});

  @override
  List<Object> get props => <Object>[reportId, patientId];
}

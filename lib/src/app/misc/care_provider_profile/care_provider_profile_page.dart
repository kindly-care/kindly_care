import 'package:cached_network_image/cached_network_image.dart';
import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../common/common.dart';
import '../../../common/constants/constants.dart';
import '../../../cubits/cubits.dart';
import 'about_care_provider_tab/about_care_provider_tab.dart';
import 'care_provider_availability_tab/care_provider_availability_tab.dart';
import 'care_provider_reviews_tab/care_provider_reviews_tab.dart';

enum _PopupAction { call, message, requestInterview, review }

class CareProviderProfilePage extends StatefulWidget {
  final CareProvider careProvider;
  const CareProviderProfilePage({super.key, required this.careProvider});

  @override
  CareProviderProfilePageState createState() => CareProviderProfilePageState();
}

class CareProviderProfilePageState extends State<CareProviderProfilePage>
    with SingleTickerProviderStateMixin {
  late TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark,
        ),
        actions: <Widget>[
          _PopupButton(
            careProvider: widget.careProvider,
            onAddReview: () {
              Navigator.pushNamed<void>(
                context,
                kSubmitReviewPageRoute,
                arguments: widget.careProvider,
              );
            },
            onRequestInterview: () {
              Navigator.pushNamed(context, kRequestInterviewPageRoute,
                  arguments: widget.careProvider.uid);
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 9.5.h),
            CircleAvatar(
              radius: 12.5.w,
              backgroundImage: CachedNetworkImageProvider(
                widget.careProvider.avatar,
              ),
            ),
            SizedBox(height: 2.0.h),
            Text(widget.careProvider.name, style: theme.titleLarge),
            SizedBox(height: 0.5.h),
            Text(
              widget.careProvider.occupation.join(', '),
              style: theme.bodyLarge,
            ),
            SizedBox(height: 1.0.h),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ColoredPill(
                  title: getAvailability(widget.careProvider),
                ),
                Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: 2.8.w, vertical: 0.8.h),
                    margin: EdgeInsets.symmetric(horizontal: 1.0.w),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12.0),
                      color: Colors.teal.shade100,
                    ),
                    child: RatingStar(rating: widget.careProvider.rating)),
                Visibility(
                  visible: widget.careProvider.address?.city != null,
                  child: ColoredPill(
                      title: widget.careProvider.address?.city ?? ''),
                ),
              ],
            ),
            SizedBox(height: 1.5.h),
            TabBar(
              controller: _controller,
              physics: const NeverScrollableScrollPhysics(),
              labelStyle: TextStyle(
                fontSize: 19.1.sp,
                fontWeight: FontWeight.w500,
              ),
              labelPadding: EdgeInsets.symmetric(horizontal: 8.5.w),
              indicatorColor: Theme.of(context).primaryColor,
              labelColor: Colors.black,
              indicatorSize: TabBarIndicatorSize.tab,
              unselectedLabelColor: Colors.grey,
              isScrollable: true,
              tabs: const <Widget>[
                Tab(text: 'Reviews'),
                Tab(text: 'About'),
                Tab(text: 'Availability'),
              ],
            ),
            SizedBox(
              height: 52.5.h,
              child: TabBarView(
                physics: const BouncingScrollPhysics(),
                controller: _controller,
                children: <Widget>[
                  CareProviderReviewsTab(
                      careProviderId: widget.careProvider.uid),
                  AboutCareProviderTab(widget.careProvider),
                  CareProviderAvailabilityTab(
                      careProvider: widget.careProvider),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _PopupButton extends StatelessWidget {
  final CareProvider careProvider;
  final VoidCallback onAddReview;
  final VoidCallback onRequestInterview;
  const _PopupButton({
    Key? key,
    required this.careProvider,
    required this.onAddReview,
    required this.onRequestInterview,
  }) : super(key: key);

  void _onPopupAction(BuildContext context, _PopupAction action, AppUser user) {
    switch (action) {
      case _PopupAction.call:
        onMakeCall(careProvider.phone);
        break;
      case _PopupAction.message:
        onCreateChannel(context, uid: user.uid, otherId: careProvider.uid);
        break;
      case _PopupAction.requestInterview:
        onRequestInterview();
        break;
      case _PopupAction.review:
        onAddReview();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final List<Patient> patients =
        context.select((AccountCubit cubit) => cubit.state.patients);
    return PopupMenuButton<_PopupAction>(
      icon: Icon(Icons.more_vert_outlined, size: 3.8.h),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0)),
      onSelected: (_PopupAction value) => _onPopupAction(context, value, user),
      itemBuilder: (BuildContext context) {
        return <PopupMenuEntry<_PopupAction>>[
          const PopupMenuItem<_PopupAction>(
            value: _PopupAction.call,
            child: IconMenuItem(icon: FeatherIcons.phone, title: 'Call'),
          ),
          const PopupMenuDivider(),
          const PopupMenuItem<_PopupAction>(
            value: _PopupAction.message,
            child: IconMenuItem(icon: Boxicons.bx_envelope, title: 'Message'),
          ),
          const PopupMenuDivider(),
          const PopupMenuItem<_PopupAction>(
            value: _PopupAction.requestInterview,
            child: IconMenuItem(
                icon: Boxicons.bx_donate_heart, title: 'Request Interview'),
          ),
          const PopupMenuDivider(),
          PopupMenuItem<_PopupAction>(
            enabled: patients.any((Patient patient) =>
                patient.assignedCareProviders.keys.contains(careProvider.uid)),
            value: _PopupAction.review,
            child: IconMenuItem(
              icon: Icons.reviews_outlined,
              title: 'Add Review',
              enabled: patients.any((Patient patient) => patient
                  .assignedCareProviders.keys
                  .contains(careProvider.uid)),
            ),
          ),
        ];
      },
    );
  }
}

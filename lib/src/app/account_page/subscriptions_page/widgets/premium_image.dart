import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class PremiumImage extends StatelessWidget {
  const PremiumImage({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 1.8.w, vertical: 2.5.h),
      height: 16.0.h,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        image: DecorationImage(
          fit: BoxFit.cover,
          alignment: FractionalOffset.center,
          image: AssetImage('assets/images/premium_bg.png'),
        ),
      ),
    );
  }
}

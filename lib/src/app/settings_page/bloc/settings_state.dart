part of 'settings_bloc.dart';

abstract class SettingsState extends Equatable {
  const SettingsState();

  @override
  List<Object> get props => <Object>[];
}

class SettingsInitial extends SettingsState {}

class DeleteAccountInProgress extends SettingsState {}

class DeleteAccountSuccess extends SettingsState {
  final String message;
  const DeleteAccountSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class DeleteAccountError extends SettingsState {
  final String error;
  const DeleteAccountError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../common/constants/constants.dart';
import '../../cubits/account_cubit/account_cubit.dart';
import '../../cubits/chats_cubit/chat_cubit.dart';
import 'widgets/widgets.dart';

class AccountPage extends StatefulWidget {
  const AccountPage({super.key});

  @override
  State<AccountPage> createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  void _onAvatarTap() {
    HapticFeedback.lightImpact();
    Navigator.pushNamed(context, kUserProfilePageRoute);
  }

  void _onPatientsTap(AppUser user) {
    Navigator.pushNamed(context, kPatientsPageRoute);
  }

  void _onCareProvidersTap() {
    Navigator.of(context).pushNamed(kCareProvidersPageRoute);
  }

  void _onTimeLogsTap(List<Patient> patients) {
    if (patients.length == 1) {
      Navigator.of(context)
          .pushNamed(kTimeLogsPageRoute, arguments: patients.first);
    } else {
      Navigator.of(context).pushNamed(kPatientSelectPageRoute,
          arguments: DestinationPage.timelogs);
    }
  }

  void _onSubscriptionsTap(AppUser user, List<Patient> patients) {
    final List<Patient> pats = patients
        .where((Patient patient) => patient.nextOfKin.contains(user.uid))
        .toList();

    if (pats.length == 1) {
      Navigator.pushNamed(context, kSubscriptionsPageRoute,
          arguments: pats.first);
    } else {
      Navigator.pushNamed(context, kPatientSelectPageRoute,
          arguments: DestinationPage.subscriptions);
    }
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final List<CareProvider> careProviders =
        context.select((AccountCubit cubit) => cubit.state.careProviders);
    final List<Patient> patients =
        context.select((AccountCubit cubit) => cubit.state.patients);

    return Scaffold(
      body: CustomScrollView(
        physics: const BouncingScrollPhysics(),
        slivers: <Widget>[
          SliverToBoxAdapter(
            child: SizedBox(height: 8.0.h),
          ),
          SliverList(
            delegate: SliverChildListDelegate(<Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                    child: BorderAvatar(
                      imageUrl: user.avatar,
                      radius: 62.5,
                      borderWidth: 3.0,
                      borderColor: Colors.blueGrey,
                    ),
                    onTap: () => _onAvatarTap(),
                  ),
                  SizedBox(height: 1.5.h),
                  Text(
                    user.name,
                    style: Theme.of(context).textTheme.headlineSmall,
                  ),
                ],
              ),
              SizedBox(height: 1.8.h),
              Row(
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () => _onPatientsTap(user),
                      child: StatWidget(
                        title: kCareRecipients,
                        stat: patients.length.toString(),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5.0.h,
                    child: const VerticalDivider(thickness: 2),
                  ),
                  Expanded(
                    child: GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: _onCareProvidersTap,
                      child: StatWidget(
                        title: kCareProviders,
                        stat: careProviders.length.toString(),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 1.0.h),
              Divider(height: 1.h),
              IconListTile(
                title: 'Time Logs',
                subtitle: 'View Time Logs',
                icon: Icons.pending_actions_outlined,
                onTap: () => _onTimeLogsTap(patients),
              ),
              Divider(indent: 18.w),
              IconListTile(
                title: kCareRecipients,
                subtitle: 'View and manage $kCareRecipients',
                icon: Icons.elderly,
                onTap: () => _onPatientsTap(user),
              ),
              Divider(indent: 18.w),
              IconListTile(
                title: kCareProviders,
                subtitle: 'View and manage $kCareProviders',
                icon: Icons.health_and_safety_outlined,
                onTap: _onCareProvidersTap,
              ),
              Divider(indent: 18.w),
              Visibility(
                visible: patients.any(
                    (Patient patient) => patient.nextOfKin.contains(user.uid)),
                child: Column(
                  children: <Widget>[
                    IconListTile(
                      title: 'Subscriptions',
                      subtitle: 'View and manage subscriptions',
                      icon: Icons.payment_outlined,
                      onTap: () => _onSubscriptionsTap(user, patients),
                    ),
                    Divider(indent: 18.w),
                  ],
                ),
              ),
              IconListTile(
                title: 'Logout',
                subtitle: 'Logout of App',
                icon: Boxicons.bx_log_out,
                onTap: _handleLogOutButtonTap,
              ),
              SizedBox(height: 1.0.h),
            ]),
          ),
        ],
      ),
    );
  }

  void _handleLogOutButtonTap() {
    showTwoButtonDialog(
      context,
      title: 'Log Out',
      content: 'Are you sure you want to log out of the app?',
      buttonText1: 'Cancel',
      buttonText2: 'Ok',
      onPressed1: () => Navigator.pop(context),
      onPressed2: () {
        Navigator.pop(context);
        context.read<ChatCubit>().disconnectChat();
        context.read<AuthBloc>().add(Logout());
      },
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../common/constants/constants.dart';

enum _PopupAction { edit }

class UserProfile extends StatelessWidget {
  const UserProfile({super.key});

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final TextTheme theme = Theme.of(context).textTheme;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark,
        ),
        actions: <Widget>[
          _PopupButton(user),
        ],
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: <Widget>[
            SizedBox(height: 9.5.h),
            CircleAvatar(
              radius: 12.5.w,
              backgroundImage: CachedNetworkImageProvider(user.avatar),
            ),
            SizedBox(height: 2.5.h),
            Text(user.name, style: theme.titleLarge),
            SizedBox(height: 6.5.h),
            CustomListTile(
              title: 'Name',
              subtitle: user.name,
              icon: Icons.account_circle_outlined,
            ),
            CustomListTile(
              icon: MdiIcons.genderMaleFemale,
              title: 'Gender',
              subtitle: user.gender ?? 'Unknown',
            ),
            CustomListTile(
              title: 'Phone',
              subtitle: user.phone,
              icon: Icons.phone_outlined,
            ),
            CustomListTile(
              title: 'Email',
              subtitle: user.email,
              icon: Icons.email_outlined,
              onTap: () => onEmail(user.email ?? ''),
            ),
            CustomListTile(
              title: 'Home Address',
              subtitle: user.address?.fullAddress() ?? kUnknownAddress,
              icon: Boxicons.bx_home_heart,
            ),
          ],
        ),
      ),
    );
  }
}

class _PopupButton extends StatefulWidget {
  final AppUser user;
  const _PopupButton(this.user, {Key? key}) : super(key: key);

  @override
  State<_PopupButton> createState() => _PopupButtonState();
}

class _PopupButtonState extends State<_PopupButton> {
  Future<void> _onPopupAction(_PopupAction action) async {
    switch (action) {
      case _PopupAction.edit:
        Navigator.pushNamed(context, kEditUserProfilePageRoute);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<_PopupAction>(
      icon: Icon(Icons.more_vert_outlined, size: 3.8.h),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0)),
      onSelected: (_PopupAction action) => _onPopupAction(action),
      itemBuilder: (BuildContext context) {
        return <PopupMenuEntry<_PopupAction>>[
          const PopupMenuItem<_PopupAction>(
            value: _PopupAction.edit,
            child: IconMenuItem(icon: Icons.edit_outlined, title: 'Edit'),
          ),
        ];
      },
    );
  }
}

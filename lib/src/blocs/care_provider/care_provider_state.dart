part of 'care_provider_bloc.dart';

abstract class CareProviderState extends Equatable {
  const CareProviderState();

  @override
  List<Object> get props => <Object>[];
}

class CareProviderInitial extends CareProviderState {}

class CareProvidersLoading extends CareProviderState {}

class CareProvidersLoadSuccess extends CareProviderState {
  final List<CareProvider> careProviders;
  const CareProvidersLoadSuccess({required this.careProviders});

  @override
  List<Object> get props => <Object>[careProviders];
}

class CareProvidersLoadError extends CareProviderState {
  final String error;
  const CareProvidersLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class AssignCareProviderInProgress extends CareProviderState {}

class AssignCareProviderSuccess extends CareProviderState {
  final String message;
  const AssignCareProviderSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class AssignCareProviderError extends CareProviderState {
  final String error;
  const AssignCareProviderError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class DismissCareProviderInProgress extends CareProviderState {}

class DismissCareProviderSuccess extends CareProviderState {
  final String message;
  const DismissCareProviderSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class DismissCareProviderError extends CareProviderState {
  final String error;
  const DismissCareProviderError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

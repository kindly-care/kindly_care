part of 'circle_member_bloc.dart';

abstract class CircleMemberEvent extends Equatable {
  const CircleMemberEvent();

  @override
  List<Object> get props => <Object>[];
}

class FetchCircleMembers extends CircleMemberEvent {
  final List<String> memberIds;
  const FetchCircleMembers({required this.memberIds});

  @override
  List<Object> get props => <Object>[memberIds];
}

class AddCircleMember extends CircleMemberEvent {
  final CircleMemberData data;
  const AddCircleMember({required this.data});

  @override
  List<Object> get props => <Object>[data];
}

class RemoveCircleMember extends CircleMemberEvent {
  final String patientId, memberId;
  const RemoveCircleMember({required this.patientId, required this.memberId});

  @override
  List<Object> get props => <Object>[patientId, memberId];
}

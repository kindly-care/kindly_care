import 'package:flutter/material.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class AboutCareProviderTab extends StatelessWidget {
  final CareProvider careProvider;
  const AboutCareProviderTab(this.careProvider, {super.key});

  String _getGenderAge() {
    if (careProvider.dob != null) {
      final int age = DateTime.now().year - careProvider.dob!.year;
      return '${careProvider.gender} ($age years old)';
    } else {
      return 'N/A';
    }
  }

  String _getYearsActive() {
    if (careProvider.yearStarted != null) {
      final int age = DateTime.now().year - careProvider.yearStarted!;
      return age.toString();
    } else {
      return 'N/A';
    }
  }

  String _getAddress() {
    final Address? address = careProvider.address;
    if (address == null) {
      return 'N/A';
    } else {
      final int number = address.number;
      final String street = address.streetName;
      final String suburb = address.suburb;
      final String city = address.city;

      if (number != 0 &&
          street.isNotEmpty &&
          suburb.isNotEmpty &&
          city.isNotEmpty) {
        return address.fullAddress();
      } else {
        return address.townAddress();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle? tileStyle =
        Theme.of(context).textTheme.titleMedium?.copyWith(
              fontSize: 17.5.sp,
              color: Colors.black.withOpacity(0.5),
            );
    final TextStyle? style = Theme.of(context).textTheme.titleSmall?.copyWith(
          fontSize: 17.5.sp,
          color: Colors.black.withOpacity(0.65),
        );
    return ListView(
      physics: const BouncingScrollPhysics(),
      padding: EdgeInsets.all(3.0.w),
      children: <Widget>[
        ListTileSubTitle(
          title: 'Occupation',
          icon: Icons.medical_services_outlined,
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: careProvider.occupation
                .map((String qualification) => Padding(
                      padding: EdgeInsets.symmetric(vertical: 0.2.h),
                      child: Text(qualification, style: style),
                    ))
                .toList(),
          ),
        ),
        CustomListTile(
          icon: MdiIcons.genderMaleFemale,
          title: 'Gender',
          subtitle: _getGenderAge(),
        ),
        CustomListTile(
          icon: MdiIcons.accountReactivateOutline,
          title: 'Years active',
          subtitle: _getYearsActive(),
        ),
        CustomListTile(
          icon: Icons.place_outlined,
          title: 'Address',
          subtitle: _getAddress(),
        ),
        CustomListTile(
          title: 'Phone',
          icon: Icons.phone_outlined,
          subtitle: careProvider.phone,
          onTap: () => onMakeCall(careProvider.phone),
        ),
        CustomListTile(
          title: 'Email',
          icon: Icons.mail_outline,
          subtitle: careProvider.email,
          onTap: () => onEmail(careProvider.email ?? 'Unknown email'),
        ),
        CustomListTile(
          icon: Icons.masks_outlined,
          title: 'Covid Vaccinated',
          subtitle: careProvider.isCovidVaccinated ? 'Yes' : 'No',
        ),
        CustomListTile(
          title: 'Driver License',
          icon: MdiIcons.cardAccountDetailsOutline,
          subtitle: careProvider.hasDriverLicense ? 'Yes' : 'No',
        ),
        ListTileSubTitle(
          title: 'Medical Qualifications',
          icon: Icons.school_outlined,
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: careProvider.certificates
                .map((String qualification) => Padding(
                      padding: EdgeInsets.symmetric(vertical: 0.2.h),
                      child: Text(qualification, style: style),
                    ))
                .toList(),
          ),
        ),
        SizedBox(height: 2.0.h),
        CustomListTile(
          title: 'Services',
          icon: Icons.manage_accounts_outlined,
          subtitle: careProvider.services.join(', '),
        ),
        SizedBox(height: 2.0.h),
        CustomListTile(
          title: 'Duties',
          icon: Icons.assignment_outlined,
          subtitle: careProvider.duties.join(', '),
        ),
        SizedBox(height: 2.0.h),
        CustomListTile(
          title: 'Bio',
          subtitle: careProvider.bio,
          icon: Icons.info_outline,
        ),
        Visibility(
          visible: careProvider.images.isNotEmpty,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 3.5.h),
              Padding(
                padding: EdgeInsets.only(left: 3.5.w),
                child: Text('Images', style: tileStyle),
              ),
              SizedBox(height: 2.0.h),
              ImageContainer(imageURLs: careProvider.images),
            ],
          ),
        ),
      ],
    );
  }
}

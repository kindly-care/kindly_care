part of 'location_bloc.dart';

abstract class LocationEvent extends Equatable {
  const LocationEvent();

  @override
  List<Object> get props => <Object>[];
}

class FetchLocationByArea extends LocationEvent {
  final String area;
  const FetchLocationByArea({required this.area});

  @override
  List<Object> get props => <Object>[area];
}

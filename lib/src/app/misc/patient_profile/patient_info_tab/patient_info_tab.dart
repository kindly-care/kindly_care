import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../blocs/care_provider/care_provider_bloc.dart';
import '../../../../common/constants/constants.dart';
import '../../../../data/services/services.dart';

class PatientInfoTab extends StatelessWidget {
  final Patient patient;
  const PatientInfoTab({super.key, required this.patient});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CareProviderBloc>(
      create: (BuildContext context) {
        final List<String> providerIds = patient.assignedCareProviders.values
            .map((AssignedCareProvider provider) => provider.careProviderId)
            .toList();
        return CareProviderBloc(
          patientRepository: context.read<PatientService>(),
          notificationRepository: context.read<NotificationService>(),
          careProviderRepository: context.read<CareProviderService>(),
        )..add(FetchAssignedCareProviders(providerIds: providerIds));
      },
      child: _PatientInfoTabView(patient),
    );
  }
}

class _PatientInfoTabView extends StatefulWidget {
  final Patient patient;
  const _PatientInfoTabView(this.patient, {Key? key}) : super(key: key);

  @override
  _PatientInfoTabViewState createState() => _PatientInfoTabViewState();
}

class _PatientInfoTabViewState extends State<_PatientInfoTabView>
    with AutomaticKeepAliveClientMixin {
  void _onFetchCareProviders() {
    final List<String> providerIds =
        widget.patient.assignedCareProviders.keys.toList();
    context
        .read<CareProviderBloc>()
        .add(FetchAssignedCareProviders(providerIds: providerIds));
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<CareProviderBloc, CareProviderState>(
      builder: (BuildContext context, CareProviderState state) {
        if (state is CareProvidersLoadError) {
          return LoadErrorWidget(
            message: state.error,
            onRetry: _onFetchCareProviders,
          );
        } else if (state is CareProvidersLoadSuccess) {
          return _PageContent(
            patient: widget.patient,
            careProviders: state.careProviders,
          );
        } else {
          return const LoadingIndicator();
        }
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class _PageContent extends StatelessWidget {
  final Patient patient;
  final List<CareProvider> careProviders;
  const _PageContent(
      {Key? key, required this.patient, required this.careProviders})
      : super(key: key);

  String _getGenderAge() {
    final int age = DateTime.now().year - patient.dob!.year;
    return '${patient.gender} ($age years old)';
  }

  String _getAllergies() {
    if (patient.allergies.isEmpty) {
      return 'None specified';
    } else {
      return patient.allergies.join(', ');
    }
  }

  String _getCondition() {
    if (patient.conditions.isEmpty) {
      return 'None specified';
    } else {
      return patient.conditions.join(', ');
    }
  }

  String _getBio() {
    if (patient.bio.isNotEmpty) {
      return patient.bio;
    } else {
      return '--';
    }
  }

  List<CustomListTile> _getAssignedAgents(BuildContext context) {
    if (patient.assignedCareProviders.isNotEmpty) {
      return patient.assignedCareProviders.values
          .map((AssignedCareProvider provider) => CustomListTile(
                icon: MdiIcons.doctor,
                title: provider.careProviderName,
                subtitle: '${provider.shift} Shift',
                trailing: Icon(Icons.chevron_right, size: 3.8.h),
                onTap: () => _onCareProviderTap(context, provider),
              ))
          .toList();
    } else {
      return <CustomListTile>[
        const CustomListTile(
          title: 'None',
          icon: Icons.account_circle_outlined,
        )
      ];
    }
  }

  void _onCareProviderTap(BuildContext context, AssignedCareProvider provider) {
    final CareProvider careProvider = careProviders.firstWhere(
        (CareProvider careProvider) =>
            careProvider.uid == provider.careProviderId);
    Navigator.of(context)
        .pushNamed(kCareProviderProfilePageRoute, arguments: careProvider);
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: const BouncingScrollPhysics(),
      padding: EdgeInsets.all(3.0.w),
      children: <Widget>[
        CustomListTile(
          title: 'Name',
          subtitle: patient.name,
          icon: Icons.account_circle_outlined,
        ),
        CustomListTile(
          icon: MdiIcons.genderMaleFemale,
          title: 'Gender',
          subtitle: _getGenderAge(),
        ),
        CustomListTile(
          icon: MdiIcons.ring,
          title: 'Marital Status',
          subtitle: patient.maritalStatus,
        ),
        const Divider(),
        const _CustomHeading(heading: 'Address & Contact'),
        CustomListTile(
          textColor: Colors.teal.shade300,
          title: 'Home Address',
          subtitle: patient.address?.fullAddress() ?? kUnknownAddress,
          icon: Boxicons.bx_home_heart,
          onTap: () {
            Navigator.pushNamed(context, kPatientMapPageRoute,
                arguments: patient);
          },
        ),
        CustomListTile(
          title: 'Phone',
          subtitle: patient.phone,
          icon: Icons.phone_outlined,
        ),
        const Divider(),
        const _CustomHeading(heading: 'Health & Wellness'),
        CustomListTile(
          title: 'Condition',
          icon: MdiIcons.medicalBag,
          subtitle: _getCondition(),
        ),
        CustomListTile(
          title: 'Allergies',
          icon: MdiIcons.allergy,
          subtitle: _getAllergies(),
        ),
        CustomListTile(
          title: 'Mobility',
          subtitle: patient.mobility,
          icon: Icons.directions_walk_outlined,
        ),
        const Divider(),
        const _CustomHeading(heading: 'Other'),
        CustomListTile(
          title: 'Bio',
          subtitle: _getBio(),
          icon: Icons.info_outline,
        ),
        const Divider(),
        const _CustomHeading(heading: 'Care Providers'),
        ..._getAssignedAgents(context),
      ],
    );
  }
}

class _CustomHeading extends StatelessWidget {
  final String heading;
  const _CustomHeading({Key? key, required this.heading}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 4.5.w, vertical: 1.0.h),
      child: Text(
        heading,
        style: theme.bodyLarge
            ?.copyWith(fontWeight: FontWeight.w500, fontSize: 18.5.sp),
      ),
    );
  }
}

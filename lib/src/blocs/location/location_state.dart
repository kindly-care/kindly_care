part of 'location_bloc.dart';

enum LocationStatus { loading, success, error }

class LocationState {
  final String message;
  final LatLng location;
  final LocationStatus status;
  const LocationState({
    this.message = '',
    this.location = kDefaultLocation,
    this.status = LocationStatus.loading,
  });
}

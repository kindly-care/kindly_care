import * as functions from "firebase-functions";
import admin = require("firebase-admin");
const db = admin.firestore();

const users = "users";
const testUsers = "care_users";

exports.updateRatings = functions.firestore
    .document("users/{uid}/reviews/{id}")
    .onCreate(async (snapshot) => {
      try {
        const careProviderId: string = snapshot.get("careProviderId");
        await updateCareProviderRating(careProviderId);
        console.log("ratings successfully calculated.");
      } catch (error) {
        console.log("failed to calculate ratings:");
        console.log(error);
      }
    });
exports.updateRatingsTest = functions.firestore
    .document("care_users/{uid}/reviews/{id}")
    .onCreate(async (snapshot) => {
      try {
        const careProviderId: string = snapshot.get("careProviderId");
        await updateCareProviderRating(careProviderId);
        console.log("ratings successfully calculated (test)");
      } catch (error) {
        console.log("failed to calculate ratings (test)");
        console.log(error);
      }
    });
/**
 * updates CareProvider rating
 * @param {string} careProviderId The CareProvider ID.
 */
export async function updateCareProviderRating(
    careProviderId: string): Promise<void> {
  const query = await db.collection(users)
      .doc(careProviderId).collection("reviews").get();
  const ratings = query.docs.map((doc) => doc.get("rating") as number);
  const totalRatings = ratings
      .reduce((accumulator, currentValue) => accumulator + currentValue);
  const averageRating = totalRatings / ratings.length;
  await db.collection(users).doc(careProviderId).update(
      {
        "rating": averageRating,
      }
  );
}
/**
 * updates CareProvider rating
 * @param {string} careProviderId The CareProvider ID.
 */
export async function updateCareProviderRatingTest(
    careProviderId: string): Promise<void> {
  const query = await db.collection(testUsers)
      .doc(careProviderId).collection("reviews").get();
  const ratings = query.docs.map((doc) => doc.get("rating") as number);
  const totalRatings = ratings
      .reduce((accumulator, currentValue) => accumulator + currentValue);
  const averageRating = totalRatings / ratings.length;
  await db.collection(testUsers).doc(careProviderId).update(
      {
        "rating": averageRating,
      }
  );
}


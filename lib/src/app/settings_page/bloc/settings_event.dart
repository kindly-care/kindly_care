part of 'settings_bloc.dart';

abstract class SettingsEvent extends Equatable {
  const SettingsEvent();

  @override
  List<Object> get props => <Object>[];
}

class DeleteAccountData extends SettingsEvent {
  final List<String> patientIds;
  const DeleteAccountData({required this.patientIds});

  @override
  List<Object> get props => <Object>[patientIds];
}

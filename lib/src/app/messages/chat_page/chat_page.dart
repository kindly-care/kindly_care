import 'dart:async';

import 'package:dart_date/dart_date.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:stream_chat_flutter_core/stream_chat_flutter_core.dart';

import '../widgets/widgets.dart';

class ChatPage extends StatelessWidget {
  final Channel channel;
  const ChatPage({super.key, required this.channel});

  @override
  Widget build(BuildContext context) {
    return StreamChannel(
      channel: channel,
      child: const _ChatPageView(),
    );
  }
}

class _ChatPageView extends StatefulWidget {
  const _ChatPageView({Key? key}) : super(key: key);

  @override
  ChatPageViewState createState() => ChatPageViewState();
}

class ChatPageViewState extends State<_ChatPageView> {
  late StreamSubscription<int> unreadCountSubscription;

  @override
  void initState() {
    super.initState();

    unreadCountSubscription = StreamChannel.of(context)
        .channel
        .state!
        .unreadCountStream
        .listen(_unreadCountHandler);
  }

  Future<void> _unreadCountHandler(int count) async {
    if (count > 0) {
      await StreamChannel.of(context).channel.markRead();
    }
  }

  @override
  void dispose() {
    unreadCountSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: ChatAppBar(user: user),
        body: Column(
          children: <Widget>[
            Expanded(
              child: MessageListCore(
                loadingBuilder: (_) => const LoadingIndicator(),
                emptyBuilder: (_) => const SizedBox.shrink(),
                errorBuilder: (_, __) =>
                    const NoDataWidget('Oops, something went wrong'),
                messageListBuilder:
                    (BuildContext context, List<Message> messages) =>
                        _ChatList(user: user, messages: messages),
              ),
            ),
            ChatInputBar(user: user),
          ],
        ),
      ),
    );
  }
}

class _ChatList extends StatelessWidget {
  final AppUser user;
  final List<Message> messages;

  const _ChatList({Key? key, required this.user, required this.messages})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(3.0.w),
      child: ListView.separated(
        reverse: true,
        itemCount: messages.length + 1,
        separatorBuilder: (BuildContext context, int index) {
          if (index == messages.length - 1) {
            return ChatDateLabel(dateTime: messages[index].createdAt);
          }
          if (messages.length == 1) {
            return const SizedBox.shrink();
          } else if (index >= messages.length - 1) {
            return const SizedBox.shrink();
          } else if (index <= messages.length) {
            final Message message = messages[index];
            final Message nextMessage = messages[index + 1];
            if (!message.createdAt.isSameDay(nextMessage.createdAt)) {
              return ChatDateLabel(dateTime: message.createdAt);
            } else {
              return const SizedBox.shrink();
            }
          } else {
            return const SizedBox.shrink();
          }
        },
        itemBuilder: (BuildContext context, int index) {
          if (index < messages.length) {
            final Message message = messages[index];

            if (message.user?.id == user.uid) {
              return OwnChatBubble(message: message);
            } else {
              return OtherChatBubble(message: message);
            }
          } else {
            return const SizedBox.shrink();
          }
        },
      ),
    );
  }
}

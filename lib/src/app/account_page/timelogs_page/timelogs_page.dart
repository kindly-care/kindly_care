import 'package:collection/collection.dart';
import 'package:dart_date/dart_date.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../data/services/services.dart';

import '../widgets/user_popup_button.dart';
import 'bloc/timelogs_bloc.dart';

class TimeLogsPage extends StatelessWidget {
  final Patient patient;
  const TimeLogsPage({super.key, required this.patient});

  @override
  Widget build(BuildContext context) {
    final int month = DateTime.now().month;
    return BlocProvider<TimelogsBloc>(
      create: (BuildContext context) => TimelogsBloc(
        patientRepository: context.read<PatientService>(),
      )..add(FetchTimeLogs(
          month: month,
          patientId: patient.uid,
          careProviderId:
              patient.assignedCareProviders.keys.firstOrNull ?? 'placeholder',
        )),
      child: _TimeLogsPageView(patient: patient),
    );
  }
}

class _TimeLogsPageView extends StatefulWidget {
  final Patient patient;
  const _TimeLogsPageView({Key? key, required this.patient}) : super(key: key);

  @override
  State<_TimeLogsPageView> createState() => _TimeLogsPageViewState();
}

class _TimeLogsPageViewState extends State<_TimeLogsPageView> {
  String _careProviderId = '';
  int _month = DateTime.now().month;

  @override
  void initState() {
    super.initState();
    _careProviderId =
        widget.patient.assignedCareProviders.keys.firstOrNull ?? 'placeholder';
  }

  void _onLoadTimeLogs() {
    context.read<TimelogsBloc>().add(FetchTimeLogs(
          month: _month,
          patientId: widget.patient.uid,
          careProviderId: _careProviderId,
        ));
  }

  @override
  Widget build(BuildContext context) {
    final String month =
        DateFormat.yMMMM('en_GB').format(DateTime.now().setMonth(_month));
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text(month),
        actions: <Widget>[
          MonthPopupButton(
            icon: Icons.today_outlined,
            onChanged: (int month) {
              setState(() => _month = month);
              _onLoadTimeLogs();
            },
          ),
          UserPopupButton(
            careProvider: widget.patient.assignedCareProviders.values.toList(),
            onChanged: (AssignedCareProvider careProvider) {
              setState(() => _careProviderId = careProvider.careProviderId);
              _onLoadTimeLogs();
            },
          )
        ],
      ),
      body: BlocBuilder<TimelogsBloc, TimelogsState>(
        builder: (BuildContext context, TimelogsState state) {
          if (state is TimelogsLoadError) {
            return LoadErrorWidget(
                message: state.error, onRetry: _onLoadTimeLogs);
          } else if (state is TimelogsLoadSuccess) {
            return _PageContent(month: _month, timeLogs: state.timelogs);
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _PageContent extends StatelessWidget {
  final int month;
  final List<TimeLog> timeLogs;
  const _PageContent({Key? key, required this.month, required this.timeLogs})
      : super(key: key);

  String _getHoursFromMinutes(int totalMinutes) {
    final int hour = totalMinutes ~/ 60;
    final int minutes = totalMinutes % 60;

    if (hour < 10 && minutes < 10) {
      return '0$hour:0${minutes.toString()}';
    } else if (hour < 10 && minutes > 10) {
      return '0$hour:${minutes.toString()}';
    } else if (hour > 10 && minutes < 10) {
      return '$hour:0${minutes.toString()}';
    } else {
      return '${hour.toString()}:${minutes.toString()}';
    }
  }

  static List<String> columns = <String>[
    'Care Pro',
    'Start',
    'Finish',
    'Hours',
  ];

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final DateTime date = DateTime.now().setMonth(month);
    final String monthYear = DateFormat.yMMMM('en_GB').format(date);
    if (timeLogs.isEmpty) {
      return NoDataWidget('No Time Logs found for $monthYear.');
    } else {
      return InteractiveViewer(
        alignPanAxis: true,
        constrained: false,
        scaleEnabled: false,
        child: DataTable(
          columnSpacing: 6.5.w,
          horizontalMargin: 2.8.w,
          columns: columns
              .map((String item) => DataColumn(
                      label: Text(
                    item,
                    style: theme.bodyMedium?.copyWith(
                      fontWeight: FontWeight.w600,
                      color: Colors.black.withOpacity(0.7),
                    ),
                  )))
              .toList(),
          rows: timeLogs.map((TimeLog timeLog) {
            final String start = DateFormat('dd/MM', 'en_GB')
                .add_jm()
                .format(timeLog.startTime!);
            final String finish = DateFormat('dd/MM', 'en_GB')
                .add_jm()
                .format(timeLog.finishTime!);

            final int diffInMinutes =
                timeLog.finishTime!.difference(timeLog.startTime!).inMinutes;

            final String totalHours = _getHoursFromMinutes(diffInMinutes);

            final String name = timeLog.careProviderName.firstNameLastInitial();

            final List<String> cells = <String>[
              name,
              start,
              finish,
              totalHours,
            ];
            return DataRow(
              cells: cells
                  .map((String item) =>
                      DataCell(Text(item, style: theme.bodyMedium)))
                  .toList(),
            );
          }).toList()
            ..add(_totalRow(context)),
        ),
      );
    }
  }

  DataRow _totalRow(BuildContext context) {
    final TextStyle? style = Theme.of(context).textTheme.bodyMedium?.copyWith(
          fontWeight: FontWeight.w600,
          color: Colors.black.withOpacity(0.7),
        );
    return DataRow(cells: <DataCell>[
      DataCell(Text('Total Hours', style: style)),
      const DataCell(Text('')),
      const DataCell(Text('')),
      DataCell(Text(getHoursMinutesFromTimeLogs(timeLogs), style: style)),
    ]);
  }
}

import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../blocs/care_task/care_task_bloc.dart';
import '../../../../common/common.dart';
import '../../../../data/services/care_task/care_task_service.dart';

import 'create_task/create_task.dart';

class CareTasksPage extends StatelessWidget {
  final Patient patient;

  const CareTasksPage({super.key, required this.patient});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CareTaskBloc>(
      create: (BuildContext context) => CareTaskBloc(
        careTaskRepository: context.read<CareTaskService>(),
      )..add(FetchCareTasks(patientId: patient.uid)),
      child: _CareTasksPageView(patient),
    );
  }
}

class _CareTasksPageView extends StatelessWidget {
  final Patient patient;
  const _CareTasksPageView(this.patient, {Key? key}) : super(key: key);

  void _onLoadCareTasks(BuildContext context) {
    context.read<CareTaskBloc>().add(FetchCareTasks(patientId: patient.uid));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<CareTaskBloc, CareTaskState>(
        buildWhen: (_, CareTaskState current) {
          return current is CareTasksLoading ||
              current is CareTasksLoadSuccess ||
              current is CareTasksLoadError;
        },
        builder: (BuildContext context, CareTaskState state) {
          if (state is CareTasksLoadError) {
            return LoadErrorWidget(
              message: state.error,
              onRetry: () => _onLoadCareTasks(context),
            );
          } else if (state is CareTasksLoadSuccess) {
            return _PageContent(patient: patient, tasks: state.tasks);
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _PageContent extends StatefulWidget {
  final Patient patient;
  final List<CareTask> tasks;
  const _PageContent({Key? key, required this.patient, required this.tasks})
      : super(key: key);

  @override
  State<_PageContent> createState() => _PageContentState();
}

class _PageContentState extends State<_PageContent> {
  void _onCreateGeneralTask({CareTask? task}) {
    Navigator.of(context).push(
      CupertinoPageRoute<String>(
        fullscreenDialog: true,
        builder: (_) =>
            CreateGeneralTaskPage(task: task, patientId: widget.patient.uid),
      ),
    );
  }

  Future<void> _onCreateMealTask({MealTask? task}) async {
    final bool? result = await Navigator.of(context).push<bool>(
      CupertinoPageRoute<bool>(
        fullscreenDialog: true,
        builder: (_) =>
            CreateMealTaskPage(task: task, patientId: widget.patient.uid),
      ),
    );

    if (mounted && result != null && result) {
      successSnackbar(context, 'Task successfully updated.');
    }
  }

  Future<void> _onCreateMedicationTask({MedicationTask? task}) async {
    final bool? result = await Navigator.of(context).push<bool>(
      CupertinoPageRoute<bool>(
        fullscreenDialog: true,
        builder: (_) =>
            CreateMedicationTaskPage(task: task, patientId: widget.patient.uid),
      ),
    );

    if (mounted && result != null && result) {
      successSnackbar(context, 'Task successfully updated.');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: const Text('Care Tasks'),
        actions: <Widget>[
          IconButton(
            onPressed: _showSelectTaskTypeBottomSheet,
            icon: Icon(Icons.add, size: 3.8.h),
          ),
        ],
      ),
      body: widget.tasks.isEmpty
          ? NoDataWidget(
              'No Care Tasks found for ${widget.patient.name.firstName()}. Press the (+) button to add tasks.')
          : ListView.builder(
              shrinkWrap: true,
              itemCount: widget.tasks.length,
              physics: const BouncingScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: 2.0.w, vertical: 1.1.h),
              itemBuilder: (BuildContext context, int index) {
                final CareTask task = widget.tasks[index];
                return Padding(
                  padding: EdgeInsets.only(bottom: 1.5.h),
                  child: CareTaskCard(
                    task,
                    onLongPress: () => _onCareTaskLongPress(task),
                  ),
                );
              },
            ),
    );
  }

  void _onCareTaskLongPress(CareTask task) {
    HapticFeedback.lightImpact();
    optionsBottomSheet(
      height: 32.0.h,
      context: context,
      title: task.title,
      options: <Widget>[
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            if (task is MedicationTask) {
              _onCreateMedicationTask(task: task);
            } else if (task is MealTask) {
              _onCreateMealTask(task: task);
            } else {
              _onCreateGeneralTask(task: task);
            }
          },
          title: 'Edit Task',
          showTrailing: false,
          icon: FeatherIcons.edit,
        ),
        Divider(indent: 18.w),
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            _showRemoveCareTaskConfirmDialog(task);
          },
          title: 'Remove Task',
          showTrailing: false,
          icon: FeatherIcons.trash2,
        ),
      ],
    );
  }

  void _showRemoveCareTaskConfirmDialog(CareTask task) {
    showTwoButtonDialog(
      context,
      title: 'Confirm Action',
      content: 'Are you sure you want to remove this Care Task?',
      isDestructiveAction: true,
      buttonText1: 'Cancel',
      buttonText2: 'Remove',
      onPressed1: () => Navigator.pop(context),
      onPressed2: () {
        Navigator.pop(context);
        context.read<CareTaskBloc>().add(RemoveCareTask(task: task));
      },
    );
  }

  void _showSelectTaskTypeBottomSheet() {
    final TextTheme theme = Theme.of(context).textTheme;
    showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      shape: bottomSheetShape(),
      builder: (_) {
        return SizedBox(
          height: 45.0.h,
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.symmetric(horizontal: 3.0.w, vertical: 1.0.h),
            physics: const BouncingScrollPhysics(),
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Create new task',
                    style: theme.titleSmall?.copyWith(
                      fontSize: 19.0.sp,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  IconButton(
                    onPressed: () => Navigator.pop(context),
                    icon: Icon(Icons.clear, size: 3.8.h),
                  )
                ],
              ),
              SizedBox(height: 2.0.h),
              Text(
                'Task Type',
                style: theme.bodyLarge?.copyWith(fontSize: 18.5.sp),
              ),
              SizedBox(height: 1.0.h),
              TaskListTile(
                title: 'General',
                icon: Icons.task_outlined,
                onTap: () {
                  Navigator.pop(context);
                  _onCreateGeneralTask();
                },
              ),
              TaskListTile(
                title: 'Meal',
                icon: Icons.dinner_dining_outlined,
                onTap: () {
                  Navigator.pop(context);
                  _onCreateMealTask();
                },
              ),
              TaskListTile(
                title: 'Medication',
                icon: Boxicons.bx_capsule,
                onTap: () {
                  Navigator.pop(context);
                  _onCreateMedicationTask();
                },
              ),
            ],
          ),
        );
      },
    );
  }
}

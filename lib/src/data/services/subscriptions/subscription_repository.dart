abstract class SubscriptionRepository {
  /// purchases a subscription. Throws [UpdateDataException]
  Future<void> updateSubscription(
      {required DateTime subEnds, required String patientId});
}

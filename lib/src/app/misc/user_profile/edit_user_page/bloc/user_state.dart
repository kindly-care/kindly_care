part of 'user_bloc.dart';

abstract class UserState extends Equatable {
  const UserState();

  @override
  List<Object> get props => <Object>[];
}

class UserInitial extends UserState {}

class UserLoadingState extends UserState {}

class UserUpdateSuccess extends UserState {
  final String message;
  const UserUpdateSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class UserUpdateError extends UserState {
  final String error;
  const UserUpdateError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

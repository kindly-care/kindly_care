import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dart_date/dart_date.dart';
import 'package:equatable/equatable.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../common/constants/constants.dart';
import '../../data/services/services.dart';

part 'subscription_event.dart';
part 'subscription_state.dart';

class SubscriptionBloc extends Bloc<SubscriptionEvent, SubscriptionState> {
  final InAppPurchase _iap;
  final PatientRepository _patientRepository;
  final SubscriptionRepository _subscriptionRepository;

  StreamSubscription<List<PurchaseDetails>>? _subscription;
  SubscriptionBloc({
    required InAppPurchase iap,
    required PatientRepository patientRepository,
    required SubscriptionRepository subscriptionRepository,
  })  : _iap = iap,
        _patientRepository = patientRepository,
        _subscriptionRepository = subscriptionRepository,
        super(const SubscriptionInitial()) {
    on<InitializePurchases>(_onInitializePurchases);
    on<FetchPurchases>(_onFetchPurchases);
    on<PurchaseSubscription>(_onPurchaseSubscription);
  }

  Future<void> _onInitializePurchases(
      InitializePurchases event, Emitter<SubscriptionState> emit) async {
    _subscription = _iap.purchaseStream.listen(
      (List<PurchaseDetails> details) {
        for (final PurchaseDetails detail in details) {
          _handlePurchase(detail, event.patient);
        }
      },
      onDone: _updateStreamOnDone,
      onError: _updateStreamOnError,
    );
  }

  Future<void> _onFetchPurchases(
      FetchPurchases event, Emitter<SubscriptionState> emit) async {
    emit(const PurchasesLoading());

    try {
      final bool paymentPlatformAvailable = await _iap.isAvailable();
      if (!paymentPlatformAvailable) {
        emit(const PurchasesLoadError(
          message: 'Service not available.',
          error: PurchasesErrorType.serviceUnavailable,
        ));
      } else {
        final ProductDetailsResponse response =
            await _iap.queryProductDetails(Set<String>.from(kProductList));

        final List<Product> products = response.productDetails
            .map((ProductDetails e) => Product(e))
            .toList();

        await emit.forEach<Patient?>(
          _patientRepository.fetchPatient(event.patientId),
          onData: (Patient? patient) {
            return PurchasesLoadSuccess(
              patient: patient!,
              products: products,
            );
          },
          onError: (_, __) => const PurchasesLoadError(
            message: 'Failed to load data',
            error: PurchasesErrorType.network,
          ),
        );
      }
    } on Exception catch (e) {
      emit(PurchasesLoadError(
        message: e.toString(),
        error: PurchasesErrorType.network,
      ));
    }
  }

  Future<void> _onPurchaseSubscription(
      PurchaseSubscription event, Emitter<SubscriptionState> emit) async {
    final PurchaseParam purchaseParam =
        PurchaseParam(productDetails: event.product.productDetails);
    await _iap.buyConsumable(purchaseParam: purchaseParam);
  }

  void _updateStreamOnDone() {}

  void _updateStreamOnError(dynamic error) {}

  Future<void> _handlePurchase(
      PurchaseDetails purchaseDetails, Patient patient) async {
    final DateTime subEnds = patient.subscriptionEnds;
    final DateTime date = subEnds.isPast ? DateTime.now() : subEnds;

    if (purchaseDetails.status == PurchaseStatus.purchased) {
      switch (purchaseDetails.productID) {
        case kProdOneMonth:
          await _subscriptionRepository.updateSubscription(
            subEnds: date.addDays(30),
            patientId: patient.uid,
          );
          break;
        case kProdThreeMonths:
          await _subscriptionRepository.updateSubscription(
            subEnds: date.addMonths(3),
            patientId: patient.uid,
          );
          break;
        case kProdSixMonths:
          await _subscriptionRepository.updateSubscription(
            subEnds: date.addMonths(12),
            patientId: patient.uid,
          );
          break;
      }
    }

    if (purchaseDetails.pendingCompletePurchase) {
      _iap.completePurchase(purchaseDetails);
    }
  }

  @override
  Future<void> close() {
    _subscription?.cancel();
    return super.close();
  }
}

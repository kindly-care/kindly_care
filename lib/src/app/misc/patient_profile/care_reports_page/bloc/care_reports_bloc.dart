import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../../../data/services/services.dart';

part 'care_reports_event.dart';
part 'care_reports_state.dart';

class CareReportsBloc extends Bloc<CareReportsEvent, CareReportsState> {
  final ReportRepository _reportRepository;
  final VitalsRepository _vitalsRepository;
  final PatientRepository _patientRepository;

  CareReportsBloc({
    required VitalsRepository vitalsRepository,
    required ReportRepository reportRepository,
    required PatientRepository patientRepository,
  })  : _reportRepository = reportRepository,
        _vitalsRepository = vitalsRepository,
        _patientRepository = patientRepository,
        super(CareReportsInitial()) {
    on<FetchCareReports>(_onFetchCareReports);
    on<FetchCareReport>(_onFetchCareReport);
  }

  Future<void> _onFetchCareReports(
      FetchCareReports event, Emitter<CareReportsState> emit) async {
    emit(CareReportsLoading());

    try {
      final List<Report> reports = await _reportRepository.fetchReportsByMonth(
          patientId: event.patientId, month: event.month);

      emit(CareReportsLoadSuccess(reports: reports));
    } on FetchDataException catch (e) {
      emit(CareReportsLoadError(error: e.toString()));
    }
  }

  Future<void> _onFetchCareReport(
      FetchCareReport event, Emitter<CareReportsState> emit) async {
    emit(CareReportLoading());
    final String reportId = event.reportId;
    final String patientId = event.patientId;

    try {
      final Report? report = await _reportRepository.fetchReport(
          patientId: patientId, reportId: reportId);

      final Patient? patient =
          await _patientRepository.fetchPatientById(patientId);

      if (report != null && patient != null) {
        final List<TemperatureRecord> temperature =
            await _vitalsRepository.fetchTemperatureByDate(
          patientId: patientId,
          date: report.createdAt,
          careProviderId: report.careProviderId,
        );

        final List<WeightRecord> weight =
            await _vitalsRepository.fetchWeightByDate(
          patientId: patientId,
          date: report.createdAt,
          careProviderId: report.careProviderId,
        );

        final List<BloodPressureRecord> bloodPressure =
            await _vitalsRepository.fetchBloodPressureByDate(
          patientId: patientId,
          date: report.createdAt,
          careProviderId: report.careProviderId,
        );

        final List<BloodGlucoseRecord> bloodSugar =
            await _vitalsRepository.fetchBloodSugarByDate(
          patientId: patientId,
          date: report.createdAt,
          careProviderId: report.careProviderId,
        );

        emit(CareReportLoadSuccess(
          report: report,
          patient: patient,
          weight: _getWeight(weight),
          temperature: _getTemp(temperature),
          bloodSugar: _getBloodSugar(bloodSugar),
          bloodPressure: _getBloodPressure(bloodPressure),
        ));
      } else {
        emit(const CareReportNotFound(message: 'Care Report not found.'));
      }
    } on FetchDataException catch (e) {
      emit(CareReportLoadError(error: e.toString()));
    }
  }

  String _getWeight(List<WeightRecord> weight) {
    if (weight.isEmpty) {
      return '--';
    } else {
      return '${weight.last.weight} Kgs';
    }
  }

  String _getTemp(List<TemperatureRecord> temp) {
    if (temp.isEmpty) {
      return '--';
    } else {
      return '${temp.last.temperature} °C';
    }
  }

  String _getBloodSugar(List<BloodGlucoseRecord> bloodGlucose) {
    if (bloodGlucose.isEmpty) {
      return '--';
    } else {
      return '${bloodGlucose.last.bloodGlucose} mmol/L';
    }
  }

  String _getBloodPressure(List<BloodPressureRecord> bp) {
    if (bp.isEmpty) {
      return '--';
    } else {
      return '${bp.last.systolic}/${bp.last.diastolic} mmHg';
    }
  }
}

import 'package:kindly_components/kindly_components.dart';

abstract class VitalsRepository {
  /// fetches [WeightRecord]s. Throws [FetchDataException].
  Future<List<WeightRecord>> fetchWeightByMonth(
      {required int month, required String patientId});

  /// fetches [TemperatureRecord]s. Throws [FetchDataException].
  Future<List<TemperatureRecord>> fetchTemperatureByMonth(
      {required int month, required String patientId});

  /// fetches [BloodPressureRecord]s. Throws [FetchDataException].
  Future<List<BloodPressureRecord>> fetchBloodPressureByMonth(
      {required int month, required String patientId});

  /// fetches [BloodGlucoseRecord]s. Throws [FetchDataException].
  Future<List<BloodGlucoseRecord>> fetchBloodGlucoseByMonth(
      {required int month, required String patientId});

  /// fetches [TemperatureRecord]s by date. Throws [FetchDataException].
  Future<List<TemperatureRecord>> fetchTemperatureByDate({
    required DateTime date,
    required String patientId,
    required String careProviderId,
  });

  /// fetches [WeightRecord]s by date. Throws [FetchDataException].
  Future<List<WeightRecord>> fetchWeightByDate({
    required DateTime date,
    required String patientId,
    required String careProviderId,
  });

  /// fetches [BloodPressureRecord]s by date. Throws [FetchDataException].
  Future<List<BloodPressureRecord>> fetchBloodPressureByDate({
    required DateTime date,
    required String patientId,
    required String careProviderId,
  });

  /// fetches [BloodGlucoseRecord]s by date. Throws [FetchDataException].
  Future<List<BloodGlucoseRecord>> fetchBloodSugarByDate({
    required DateTime date,
    required String patientId,
    required String careProviderId,
  });
}

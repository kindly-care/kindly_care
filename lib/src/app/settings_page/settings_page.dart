import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:in_app_review/in_app_review.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:share_plus/share_plus.dart';
import 'package:wiredash/wiredash.dart';

import '../../common/constants/constants.dart';
import '../../cubits/account_cubit/account_cubit.dart';
import '../../data/services/services.dart';
import 'bloc/settings_bloc.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: <BlocProvider<dynamic>>[
        BlocProvider<SettingsBloc>(
          create: (BuildContext context) => SettingsBloc(
            authRepository: context.read<AuthService>(),
            patientRepository: context.read<PatientService>(),
          ),
        ),
      ],
      child: const SettingsPageView(),
    );
  }
}

class SettingsPageView extends StatefulWidget {
  const SettingsPageView({Key? key}) : super(key: key);

  @override
  State<SettingsPageView> createState() => _SettingsPageViewState();
}

class _SettingsPageViewState extends State<SettingsPageView> {
  final InAppReview _appReview = InAppReview.instance;

  Future<void> _onAboutTap() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    showAboutDialog(
      context: context,
      applicationName: 'Kindly Care',
      applicationVersion: info.version,
      applicationLegalese: kAbout,
    );
  }

  void _onRecommendAppTap() => Share.share(kAppURL);

  Future<void> _onFeedbackTap(AppUser user) async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    if (mounted) {
      final WiredashController wiredashController = Wiredash.of(context);

      wiredashController.setBuildProperties(
        buildVersion: info.version,
        buildNumber: info.buildNumber,
      );
      wiredashController.setUserProperties(
        userId: user.uid,
        userEmail: user.email,
      );

      wiredashController.show();
    }
  }

  void _onDeleteAccount(List<Patient> patients) {
    final List<String> patientIds =
        patients.map((Patient patient) => patient.uid).toList();
    context.read<SettingsBloc>().add(DeleteAccountData(patientIds: patientIds));
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final List<Patient> patients = context
        .select((AccountCubit cubit) => cubit.state.patients)
        .where((Patient patient) => patient.nextOfKin.contains(user.uid))
        .toList();

    final bool isLoading = context.select((SettingsBloc bloc) => bloc.state)
        is DeleteAccountInProgress;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
      ),
      body: LoadingOverlay(
        isLoading: isLoading,
        child: BlocListener<SettingsBloc, SettingsState>(
          listener: (BuildContext context, SettingsState state) {
            if (state is DeleteAccountError) {
              _showDeleteAccountErrorDialog(patients);
            } else if (state is DeleteAccountSuccess) {
              context.read<AuthBloc>().add(DeleteAccount());
            }
          },
          child: ListView(
            physics: const BouncingScrollPhysics(),
            padding: EdgeInsets.all(3.0.w),
            children: <Widget>[
              TextListTile(
                title: 'Feedback',
                subtitle: 'Give Feedback',
                onTap: () => _onFeedbackTap(user),
              ),
              const Divider(),
              TextListTile(
                title: 'Share App',
                subtitle: 'Share App With Others',
                onTap: _onRecommendAppTap,
              ),
              const Divider(),
              TextListTile(
                title: 'About',
                subtitle: 'About Kindly Care',
                onTap: _onAboutTap,
              ),
              const Divider(),
              TextListTile(
                title: 'Rate App',
                subtitle: 'Rate This App',
                onTap: _appReview.openStoreListing,
              ),
              const Divider(),
              TextListTile(
                title: 'Get in touch',
                subtitle: 'Get in touch with us',
                onTap: _showGetInTouchBottomSheet,
              ),
              const Divider(),
              TextListTile(
                title: 'Delete Account',
                subtitle: 'Delete Account & All User Data',
                onTap: () => _showDeleteAccountConfirmDialog(patients),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _showDeleteAccountConfirmDialog(List<Patient> patients) {
    showTwoButtonDialog(
      context,
      isDestructiveAction: true,
      title: 'Delete Account',
      content:
          'Are you sure you want to delete your account together with all your data? This includes any Care Recipients you may have.',
      buttonText1: 'Cancel',
      buttonText2: 'Delete',
      onPressed1: () => Navigator.pop(context),
      onPressed2: () {
        Navigator.of(context).pop();
        _onDeleteAccount(patients);
      },
    );
  }

  void _showDeleteAccountErrorDialog(List<Patient> patients) {
    showTwoButtonDialog(
      context,
      title: 'Error',
      content:
          'Failed to delete account. Please check your network and try again.',
      buttonText1: 'Cancel',
      buttonText2: 'Retry',
      onPressed1: () => Navigator.pop(context),
      onPressed2: () {
        Navigator.of(context).pop();
        _onDeleteAccount(patients);
      },
    );
  }

  void _showGetInTouchBottomSheet() {
    optionsBottomSheet(
      title: 'Get In Touch With Us',
      height: 40.0.h,
      context: context,
      options: <Widget>[
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onMakeCall(kContactPhone);
          },
          title: 'Phone',
          showTrailing: false,
          subtitle: kContactPhone,
          icon: FeatherIcons.phone,
        ),
        Divider(indent: 18.w),
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onEmail(kContactEmail);
          },
          title: 'Email',
          showTrailing: false,
          subtitle: kContactEmail,
          icon: FeatherIcons.mail,
        ),
      ],
    );
  }
}

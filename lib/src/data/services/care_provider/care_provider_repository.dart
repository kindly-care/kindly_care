import 'package:kindly_components/kindly_components.dart';

abstract class CareProviderRepository {
  /// fetches [CareProvider]s [Review]s. Throws [FetchDataException].
  Stream<List<Review>> fetchReviews(String careProviderId);

  /// attempts to submit [Review]. Throws [UpdateDataException].
  Future<void> submitReview(Review review);

  /// fetches [CareProvider] by id. Throws [FetchDataException].
  Stream<CareProvider?> fetchCareProviderById(String id);

  /// fetches [CareProvider] by email. Throws [FetchDataException].
  Future<CareProvider?> fetchCareProviderByEmail(String email);

  /// fetches [CareProvider]s . Throws [FetchDataException].
  Stream<List<CareProvider>> fetchCareProviders(List<String> ids);

  /// fetches all [CareProvider]s . Throws [FetchDataException].
  Future<List<CareProvider>> fetchAllCareProviders();

  /// adds an existing [CareProvider] to a [Patient]. Throws [UpdateDataException].
  Future<void> assignCareProvider({
    required AppUser client,
    required AssignedPatient assignedPatient,
    required AssignedCareProvider assignedCareProvider,
  });
}

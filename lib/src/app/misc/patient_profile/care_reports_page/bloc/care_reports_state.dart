part of 'care_reports_bloc.dart';

abstract class CareReportsState extends Equatable {
  const CareReportsState();

  @override
  List<Object> get props => <Object>[];
}

class CareReportsInitial extends CareReportsState {}

class CareReportsLoading extends CareReportsState {}

class CareReportsLoadSuccess extends CareReportsState {
  final List<Report> reports;
  const CareReportsLoadSuccess({required this.reports});

  @override
  List<Object> get props => <Object>[reports];
}

class CareReportsLoadError extends CareReportsState {
  final String error;
  const CareReportsLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class CareReportLoading extends CareReportsState {}

class CareReportLoadSuccess extends CareReportsState {
  final Report report;
  final String weight;
  final Patient patient;
  final String bloodSugar;
  final String temperature;
  final String bloodPressure;

  const CareReportLoadSuccess({
    required this.report,
    required this.weight,
    required this.patient,
    required this.bloodSugar,
    required this.temperature,
    required this.bloodPressure,
  });

  @override
  List<Object> get props => <Object>[
        report,
        weight,
        patient,
        bloodSugar,
        temperature,
        bloodPressure,
      ];
}

class CareReportLoadError extends CareReportsState {
  final String error;
  const CareReportLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class CareReportNotFound extends CareReportsState {
  final String message;
  const CareReportNotFound({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

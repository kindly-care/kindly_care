import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../common/constants/constants.dart';
import '../../../cubits/account_cubit/account_cubit.dart';

class CareRecipientSelectPage extends StatelessWidget {
  final DestinationPage destination;
  const CareRecipientSelectPage({super.key, required this.destination});

  void _onCareRecipientTap(BuildContext context, Patient patient) {
    switch (destination) {
      case DestinationPage.records:
        Navigator.of(context)
            .pushNamed(kPatientRecordsPageRoute, arguments: patient);
        break;
      case DestinationPage.timelogs:
        Navigator.of(context).pushNamed(kTimeLogsPageRoute, arguments: patient);
        break;
      case DestinationPage.subscriptions:
        Navigator.of(context)
            .pushNamed(kSubscriptionsPageRoute, arguments: patient);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final List<Patient> patients =
        context.select((AccountCubit cubit) => cubit.state.patients);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Select Care Recipient'),
      ),
      body: patients.isEmpty
          ? const NoDataWidget('You currently do not have any Care Recipients.')
          : ListView.separated(
              shrinkWrap: true,
              itemCount: patients.length,
              padding: EdgeInsets.all(3.0.w),
              physics: const BouncingScrollPhysics(),
              separatorBuilder: (_, __) =>
                  Divider(indent: 2.0.w, endIndent: 2.0.w),
              itemBuilder: (BuildContext context, int index) {
                final Patient patient = patients[index];
                return PatientCard(
                  patient: patient,
                  onTap: () => _onCareRecipientTap(context, patient),
                  onLongPress: () {},
                );
              },
            ),
    );
  }
}

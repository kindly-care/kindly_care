import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../blocs/blocs.dart';
import '../../common/constants/constants.dart';
import '../../cubits/account_cubit/account_cubit.dart';

import 'widgets/widgets.dart';

class PatientsOverviewPage extends StatefulWidget {
  final String uid;
  const PatientsOverviewPage({super.key, required this.uid});

  @override
  State<PatientsOverviewPage> createState() => _PatientsOverviewPageState();
}

class _PatientsOverviewPageState extends State<PatientsOverviewPage> {
  @override
  void initState() {
    super.initState();
    context.read<NotificationBloc>().add(FetchNotifications(uid: widget.uid));
  }

  void _onLoadCareRecipients() {
    context.read<AccountCubit>().fetchAccountData();
  }

  void _onNotificationTap(AppUser user) {
    Navigator.pushNamed(context, kNotificationsPageRoute, arguments: user.uid);
  }

  bool _showNotificationBadge(List<PushNotification> notifications) {
    final List<PushNotification> notSeenNotifications = notifications
        .where((PushNotification notification) => !notification.isSeen)
        .toList();

    if (notSeenNotifications.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  void _onAddPatientTap() {
    Navigator.of(context).pushNamed(kAddPatientPageRoute);
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final List<PushNotification> notifications =
        context.select((NotificationBloc bloc) => bloc.notifications);
    final List<Patient> patients =
        context.select((AccountCubit cubit) => cubit.state.patients);

    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        title: const Text('Care Recipients'),
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark,
          statusBarColor: Theme.of(context).backgroundColor,
        ),
        actions: <Widget>[
          Visibility(
            visible: patients.isNotEmpty,
            child: IconButton(
              icon: Stack(
                children: <Widget>[
                  Icon(Boxicons.bx_bell, size: 3.8.h),
                  Visibility(
                    visible: _showNotificationBadge(notifications),
                    child: Positioned(
                      top: 0.1.h,
                      right: 0.8.w,
                      child: Icon(Icons.brightness_1,
                          size: 1.5.h, color: Colors.redAccent),
                    ),
                  ),
                ],
              ),
              onPressed: () => _onNotificationTap(user),
            ),
          ),
          Visibility(
            visible: patients.isEmpty,
            child: IconButton(
              onPressed: _onAddPatientTap,
              icon: Icon(Icons.add, size: 3.8.h),
            ),
          ),
        ],
      ),
      body: BlocBuilder<AccountCubit, AccountState>(
        builder: (BuildContext context, AccountState state) {
          switch (state.status) {
            case AccountStatus.error:
              return LoadErrorWidget(
                message: state.message,
                onRetry: _onLoadCareRecipients,
              );
            case AccountStatus.loading:
              return const LoadingIndicator();
            case AccountStatus.success:
              return const _PageContent();
          }
        },
      ),
    );
  }
}

class _PageContent extends StatelessWidget {
  const _PageContent({Key? key}) : super(key: key);

  void _handlePatientTap(BuildContext context, Patient patient) {
    Navigator.of(context)
        .pushNamed(kPatientProfilePageRoute, arguments: patient.uid);
  }

  @override
  Widget build(BuildContext context) {
    final List<Patient> patients =
        context.select((AccountCubit cubit) => cubit.state.patients);
    if (patients.isEmpty) {
      return const NoDataWidget(
          'You currently do not have any Care Recipients. Press the (+) button to add a Care Recipient.');
    } else {
      return ListView.builder(
        shrinkWrap: true,
        itemCount: patients.length,
        padding: EdgeInsets.all(1.5.w),
        physics: const BouncingScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          final Patient patient = patients[index];
          return _PatientCard(
            patient: patient,
            onTap: () => _handlePatientTap(context, patient),
            onLongPress: () {},
          );
        },
      );
    }
  }
}

class _PatientCard extends StatelessWidget {
  final Patient patient;
  final VoidCallback onTap;
  final VoidCallback onLongPress;

  const _PatientCard({
    Key? key,
    required this.patient,
    required this.onTap,
    required this.onLongPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 0.5.h),
      child: Card(
        elevation: 0.1,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.5)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ListTile(
              onTap: onTap,
              contentPadding: EdgeInsets.symmetric(horizontal: 2.0.w),
              leading: CircleAvatar(
                radius: 3.8.h,
                backgroundImage: CachedNetworkImageProvider(patient.avatar),
              ),
              title: Text(patient.name, style: theme.titleMedium),
              subtitle: Text(
                patient.address?.shortAddress() ?? kUnknownAddress,
                style: theme.titleSmall,
              ),
            ),
            Divider(indent: 2.0.w, endIndent: 2.0.w),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 2.5.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Meals(patient: patient),
                  Divider(indent: 2.0.w, endIndent: 2.0.w),
                  HealthRecords(patient: patient),
                ],
              ),
            ),
            SizedBox(height: 1.0.h),
          ],
        ),
      ),
    );
  }
}

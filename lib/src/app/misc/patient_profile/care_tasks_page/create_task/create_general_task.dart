import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:uuid/uuid.dart';

import '../../../../../blocs/care_task/care_task_bloc.dart';
import '../../../../../common/constants/constants.dart';
import '../../../../../data/services/care_task/care_task_service.dart';

class CreateGeneralTaskPage extends StatelessWidget {
  final CareTask? task;
  final String patientId;

  const CreateGeneralTaskPage({super.key, this.task, required this.patientId});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CareTaskBloc>(
      create: (BuildContext context) => CareTaskBloc(
        careTaskRepository: context.read<CareTaskService>(),
      ),
      child: _CreateGeneralTaskPageView(task: task, patientId: patientId),
    );
  }
}

class _CreateGeneralTaskPageView extends StatefulWidget {
  final CareTask? task;
  final String patientId;

  const _CreateGeneralTaskPageView({
    Key? key,
    this.task,
    required this.patientId,
  }) : super(key: key);

  @override
  _CreateGeneralTaskPageViewState createState() =>
      _CreateGeneralTaskPageViewState();
}

class _CreateGeneralTaskPageViewState
    extends State<_CreateGeneralTaskPageView> {
  late DateTime _time;
  late GlobalKey<FormState> _formKey;
  late TextEditingController _timeController;
  late TextEditingController _titleController;
  late TextEditingController _repeatDaysController;
  late TextEditingController _descriptionController;

  @override
  void initState() {
    super.initState();
    _formKey = GlobalKey<FormState>();
    final CareTask? task = widget.task;
    _time = task?.time ?? DateTime.now();
    _timeController = TextEditingController(
      text: task != null ? DateFormat.jm().format(task.time) : null,
    );
    _titleController = TextEditingController(text: task?.title);
    _descriptionController = TextEditingController(text: task?.description);
    _repeatDaysController =
        TextEditingController(text: task?.repeatDays.join(', '));
  }

  Future<void> _onSaveButtonTap(AppUser user) async {
    final bool isValid = _formKey.currentState?.validate() ?? false;
    if (isValid) {
      final CareTask task = CareTask(
        time: _time,
        type: CareTaskType.general,
        taskId: widget.task?.taskId ?? const Uuid().v4(),
        title: widget.task?.title ?? _titleController.text,
        patientId: widget.patientId,
        repeatDays:
            widget.task?.repeatDays ?? _repeatDaysController.text.split(', '),
        lastUpdated: DateTime.now(),
        lastUpdatedBy: user.name,
        description: widget.task?.description ?? _descriptionController.text,
      );

      context.read<CareTaskBloc>().add(CreateCareTask(task: task));
    }
  }

  void _onErrorState(AppUser user, String message) {
    showTwoButtonDialog(
      context,
      title: 'Error',
      content: message,
      buttonText1: 'Exit',
      buttonText2: 'Retry',
      onPressed1: () {
        Navigator.pop(context);
        Navigator.pop(context);
      },
      onPressed2: () {
        Navigator.pop(context);
        _onSaveButtonTap(user);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final bool isLoading = context.select((CareTaskBloc bloc) => bloc.state)
        is CreateCareTaskInProgress;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.task == null ? 'Add Task' : 'Edit Task'),
      ),
      body: LoadingOverlay(
        isLoading: isLoading,
        child: BlocListener<CareTaskBloc, CareTaskState>(
          listener: (BuildContext context, CareTaskState state) {
            if (state is CreateCareTaskError) {
              _onErrorState(user, state.error);
            } else if (state is CreateCareTaskSuccess) {
              Navigator.pop(context);
              successSnackbar(context, state.message);
            }
          },
          child: Form(
            key: _formKey,
            child: ListView(
              physics: const BouncingScrollPhysics(),
              padding: EdgeInsets.all(3.0.w),
              children: <Widget>[
                SizedBox(height: 1.0.h),
                MaterialTextField(
                  controller: _titleController,
                  labelText: 'Title',
                  hintText: 'Enter task title',
                  onChanged: (_) => setState(() {}),
                  validator: (String? val) => val.simpleValidate,
                ),
                SizedBox(height: 2.5.h),
                MaterialTextField(
                  readOnly: true,
                  enableInteractiveSelection: false,
                  focusNode: AlwaysDisabledFocusNode(),
                  onTap: () {
                    showDateTimeSelector(
                      context,
                      title: 'Task Time',
                      initialDate: _time,
                      controller: _timeController,
                      format: DateFormat.jm('en_GB'),
                      mode: CupertinoDatePickerMode.time,
                      onDateChanged: (DateTime val) {
                        _time = val;
                        _timeController.text =
                            DateFormat.jm('en_GB').format(val);
                      },
                    );
                  },
                  labelText: 'Time',
                  hintText: 'Select time',
                  controller: _timeController,
                  onChanged: (_) => setState(() {}),
                  prefixIcon: const Icon(Icons.access_time_outlined),
                  validator: (String? val) => val.simpleValidate,
                ),
                SizedBox(height: 2.5.h),
                MaterialTextField(
                  minLines: 3,
                  maxLines: null,
                  isOptional: true,
                  labelText: 'Description',
                  hintText: 'Enter description',
                  onChanged: (_) => setState(() {}),
                  controller: _descriptionController,
                  textInputAction: TextInputAction.newline,
                ),
                SizedBox(height: 2.5.h),
                MaterialTextField(
                  readOnly: true,
                  labelText: 'Recurring Days',
                  hintText: 'Enter recurring days',
                  controller: _repeatDaysController,
                  enableInteractiveSelection: false,
                  focusNode: AlwaysDisabledFocusNode(),
                  onChanged: (_) => setState(() {}),
                  onTap: () {
                    showMultiSelectBottomSheet(
                      context,
                      title: 'Recurring Days',
                      items: kLongWeekDays,
                      controller: _repeatDaysController,
                      onSelectedItems: (_) {
                        setState(() {});
                      },
                    );
                  },
                  validator: (String? val) => val.simpleValidate,
                ),
                SizedBox(height: 10.0.h),
                ActionButton(
                  title: 'Save Task',
                  onPressed: () => _onSaveButtonTap(user),
                  enabled: _isButtonEnabled(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool _isButtonEnabled() {
    if (_timeController.text.isNotEmpty &&
        _titleController.text.isNotEmpty &&
        _repeatDaysController.text.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  @override
  void dispose() {
    _timeController.dispose();
    _titleController.dispose();
    _descriptionController.dispose();
    _repeatDaysController.dispose();
    super.dispose();
  }
}

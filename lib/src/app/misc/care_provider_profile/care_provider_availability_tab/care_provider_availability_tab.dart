import 'package:flutter/material.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../widgets/widgets.dart';

class CareProviderAvailabilityTab extends StatelessWidget {
  final CareProvider careProvider;
  const CareProviderAvailabilityTab({super.key, required this.careProvider});

  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: const BouncingScrollPhysics(),
      padding: EdgeInsets.all(3.0.w),
      children: <Widget>[
        SizedBox(height: 1.5.h),
        AvailabilityItem(
            title: 'Sunday', value: careProvider.sundayAvailability),
        SizedBox(height: 3.5.h),
        AvailabilityItem(
            title: 'Monday', value: careProvider.mondayAvailability),
        SizedBox(height: 3.5.h),
        AvailabilityItem(
            title: 'Tuesday', value: careProvider.tuesdayAvailability),
        SizedBox(height: 3.5.h),
        AvailabilityItem(
            title: 'Wednesday', value: careProvider.wednesdayAvailability),
        SizedBox(height: 3.5.h),
        AvailabilityItem(
            title: 'Thursday', value: careProvider.thursdayAvailability),
        SizedBox(height: 3.5.h),
        AvailabilityItem(
            title: 'Friday', value: careProvider.fridayAvailability),
        SizedBox(height: 3.5.h),
        AvailabilityItem(
            title: 'Saturday', value: careProvider.saturdayAvailability),
      ],
    );
  }
}

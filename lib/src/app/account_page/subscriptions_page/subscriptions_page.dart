import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../blocs/subscription/subscription_bloc.dart';
import '../../../data/services/services.dart';
import 'widgets/widgets.dart';

class SubscriptionsPage extends StatelessWidget {
  final Patient patient;
  const SubscriptionsPage({super.key, required this.patient});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SubscriptionBloc>(
      create: (BuildContext context) {
        return SubscriptionBloc(
          iap: InAppPurchase.instance,
          patientRepository: context.read<PatientService>(),
          subscriptionRepository: context.read<SubscriptionService>(),
        )
          ..add(InitializePurchases(patient: patient))
          ..add(FetchPurchases(patientId: patient.uid));
      },
      child: _SubscriptionsPageView(patient: patient),
    );
  }
}

class _SubscriptionsPageView extends StatelessWidget {
  final Patient patient;
  const _SubscriptionsPageView({Key? key, required this.patient})
      : super(key: key);

  void _onLoadPurchases(BuildContext context) {
    context
        .read<SubscriptionBloc>()
        .add(FetchPurchases(patientId: patient.uid));
  }

  void _onGoogleServicesNotAvailable(BuildContext context, String message) {
    showOneButtonDialog(
      context,
      title: 'Service Not Available',
      content: message,
      buttonText: 'Ok',
      onPressed: () => Navigator.pop(context),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<SubscriptionBloc, SubscriptionState>(
        listener: (BuildContext context, SubscriptionState state) {
          if (state is PurchasesLoadError &&
              state.error == PurchasesErrorType.serviceUnavailable) {
            _onGoogleServicesNotAvailable(context, state.message);
          }
        },
        builder: (BuildContext context, SubscriptionState state) {
          if (state is PurchasesLoadError &&
              state.error == PurchasesErrorType.network) {
            return LoadErrorWidget(
              message: state.message,
              onRetry: () => _onLoadPurchases(context),
            );
          } else if (state is PurchasesLoadError &&
              state.error == PurchasesErrorType.serviceUnavailable) {
            return Container();
          } else if (state is PurchasesLoadSuccess) {
            return _PageContent(
              patient: state.patient,
              products: state.products,
            );
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _PageContent extends StatelessWidget {
  final Patient patient;
  final List<Product> products;

  const _PageContent({Key? key, required this.patient, required this.products})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final DateTime subscriptionEnds = patient.subscriptionEnds;
    return CustomScrollView(
      physics: const BouncingScrollPhysics(),
      slivers: <Widget>[
        SliverAppBar(
          pinned: false,
          floating: true,
          elevation: 0.0,
          title: Text(patient.name),
        ),
        SliverPadding(
          padding: EdgeInsets.all(1.0.w),
          sliver: SliverList(
            delegate: SliverChildListDelegate(
              <Widget>[
                const PremiumImage(),
                SizedBox(height: 4.0.h),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 1.5.h),
                  child: Text(
                    'Subscription',
                    style: theme.headlineSmall
                        ?.copyWith(fontWeight: FontWeight.w500),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(1.5.h),
                  child: Text("Here's what you get:", style: theme.bodyLarge),
                ),
                const Features(),
                NextPayment(subscriptionEnds: subscriptionEnds),
                SizedBox(
                  height: 21.0.h,
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: products.length,
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext context, int index) {
                      final Product product = products[index];
                      return SubscriptionTier(
                        price: product.price,
                        onTap: () {
                          context
                              .read<SubscriptionBloc>()
                              .add(PurchaseSubscription(product: product));
                        },
                        period: product.subscriptionPeriod,
                      );
                    },
                  ),
                ),
                SizedBox(height: 2.0.h),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

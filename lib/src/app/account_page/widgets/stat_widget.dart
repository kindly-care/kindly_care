import 'package:flutter/material.dart';

class StatWidget extends StatelessWidget {
  final String stat;
  final String title;
  const StatWidget({super.key, required this.stat, required this.title});

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Column(
      children: <Widget>[
        Text(
          title,
          style:
              theme.bodyLarge?.copyWith(color: Colors.black.withOpacity(0.5)),
        ),
        Text(
          stat,
          style: theme.bodyLarge?.copyWith(fontWeight: FontWeight.w600),
        ),
      ],
    );
  }
}

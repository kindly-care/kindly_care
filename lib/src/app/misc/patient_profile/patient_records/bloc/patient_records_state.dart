part of 'patient_records_bloc.dart';

abstract class PatientRecordsState extends Equatable {
  const PatientRecordsState();

  @override
  List<Object> get props => <Object>[];
}

class PatientRecordsInitial extends PatientRecordsState {}

class PatientRecordsLoading extends PatientRecordsState {}

class PatientRecordsLoadError extends PatientRecordsState {
  final String error;
  const PatientRecordsLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class PatientWeightRecordsLoadSuccess extends PatientRecordsState {
  final List<WeightRecord> records;
  const PatientWeightRecordsLoadSuccess({required this.records});

  @override
  List<Object> get props => <Object>[records];
}

class PatientTemperatureRecordsLoadSuccess extends PatientRecordsState {
  final List<TemperatureRecord> records;
  const PatientTemperatureRecordsLoadSuccess({required this.records});

  @override
  List<Object> get props => <Object>[records];
}

class PatientBloodPressureRecordsLoadSuccess extends PatientRecordsState {
  final List<BloodPressureRecord> records;
  const PatientBloodPressureRecordsLoadSuccess({required this.records});

  @override
  List<Object> get props => <Object>[records];
}

class PatientBloodGlucoseRecordsLoadSuccess extends PatientRecordsState {
  final List<BloodGlucoseRecord> records;
  const PatientBloodGlucoseRecordsLoadSuccess({required this.records});

  @override
  List<Object> get props => <Object>[records];
}

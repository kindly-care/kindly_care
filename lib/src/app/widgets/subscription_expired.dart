import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../common/constants/constants.dart';

class SubscriptionExpired extends StatelessWidget {
  final String content;
  final String detail;
  final Patient patient;
  const SubscriptionExpired({
    super.key,
    required this.content,
    required this.patient,
    required this.detail,
  });

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final String patientName = patient.name.firstName();
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final bool isNextOfKin = patient.nextOfKin.contains(user.uid);

    return Center(
      child: isNextOfKin
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Subscription for $patientName has expired. Please renew subscription to view $content.',
                  style: theme.bodyText1,
                  textAlign: TextAlign.center,
                ),
                TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, kSubscriptionsPageRoute,
                        arguments: patient);
                  },
                  child: Text(
                    'Renew Subscription',
                    style: theme.bodyText1?.copyWith(
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                ),
              ],
            )
          : Text(
              detail,
              style: theme.bodyText1,
              textAlign: TextAlign.center,
            ),
    );
  }
}

part of 'care_task_bloc.dart';

abstract class CareTaskEvent extends Equatable {
  const CareTaskEvent();

  @override
  List<Object> get props => <Object>[];
}

class FetchCareTasks extends CareTaskEvent {
  final String patientId;

  const FetchCareTasks({required this.patientId});

  @override
  List<Object> get props => <Object>[patientId];
}

class CreateCareTask extends CareTaskEvent {
  final CareTask task;

  const CreateCareTask({required this.task});

  @override
  List<Object> get props => <Object>[task];
}

class RemoveCareTask extends CareTaskEvent {
  final CareTask task;

  const RemoveCareTask({required this.task});

  @override
  List<Object> get props => <Object>[task];
}

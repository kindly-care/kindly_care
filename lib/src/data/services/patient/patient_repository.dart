import 'package:kindly_components/kindly_components.dart';

abstract class PatientRepository {
  /// creates/updates [Patient]. Throws [UpdateDataException].
  Future<void> createPatient(Patient patient, {bool isNewPatient = false});

  /// fetches assigned [CareProvider]s. Throws [FetchDataException].
  Future<List<CareProvider>> fetchAssignedCareProviders(List<String> ids);

  /// fetch [Patient]s. Throws [FetchDataException].
  Stream<List<Patient>> fetchCirclePatients(String uid);

  /// fetches a single [Patient]. Throws [FetchDataException].
  Stream<Patient?> fetchPatient(String patientId);

  /// fetches a single [Patient]. Throws [FetchDataException].
  Future<Patient?> fetchPatientById(String patientId);

  /// completely removes [Patient] from database. Throws [UpdateDataException].
  Future<void> deleteCareRecipient(String patientId);

  /// completely removes [Patient]s from database. Throws [UpdateDataException].
  Future<void> deletePatients(List<String> patientIds);

  /// terminates [CareProvider] from [Patient]. Throws [UpdateDataException].
  Future<void> removeCareProvider(
      {required Patient patient, required CareProvider careProvider});

  /// fetches a [Patient]'s [TimeLog]s by month. Throws [FetchDataException].
  Future<List<TimeLog>> fetchTimeLogsByMonth(
      {required String patientId,
      required int month,
      required String careProviderId});
}

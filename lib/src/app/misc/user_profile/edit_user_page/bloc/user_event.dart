part of 'user_bloc.dart';

abstract class UserEvent extends Equatable {
  const UserEvent();

  @override
  List<Object> get props => <Object>[];
}

class UpdateUser extends UserEvent {
  final AppUser user;
  const UpdateUser({required this.user});

  @override
  List<Object> get props => <Object>[user];
}

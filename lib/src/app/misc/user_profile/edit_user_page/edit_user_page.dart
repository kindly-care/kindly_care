import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../data/services/user/user_service.dart';
import 'bloc/user_bloc.dart';

class EditUserPage extends StatelessWidget {
  const EditUserPage({super.key});

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    return MultiBlocProvider(
      providers: <BlocProvider<dynamic>>[
        BlocProvider<UserBloc>(
          create: (BuildContext context) => UserBloc(
            userRepository: context.read<UserService>(),
          ),
        ),
      ],
      child: _EditUserPageView(user: user),
    );
  }
}

class _EditUserPageView extends StatefulWidget {
  final AppUser user;
  const _EditUserPageView({Key? key, required this.user}) : super(key: key);

  @override
  State<_EditUserPageView> createState() => _EditUserPageViewState();
}

class _EditUserPageViewState extends State<_EditUserPageView> {
  File? _avatar;
  DateTime? _dob;
  late TextEditingController _nameController;
  final GlobalKey<FormState> _formKey = GlobalKey();
  late TextEditingController _phoneController;
  late TextEditingController _emailController;
  late TextEditingController _genderController;
  late TextEditingController _dobController;
  late TextEditingController _addressNumberController;
  late TextEditingController _streetNameController;
  late TextEditingController _suburbController;
  late TextEditingController _cityController;

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController(text: widget.user.name);
    _phoneController = TextEditingController(text: widget.user.phone);
    _emailController = TextEditingController(text: widget.user.email);
    _genderController = TextEditingController(text: _initGender());
    _dobController = TextEditingController(text: _initDOB());
    _addressNumberController =
        TextEditingController(text: _initAddressNumber());
    _streetNameController = TextEditingController(text: _initStreetName());
    _suburbController = TextEditingController(text: _initSuburb());
    _cityController = TextEditingController(text: _initCity());
  }

  String? _initGender() {
    return widget.user.gender;
  }

  String? _initDOB() {
    if (widget.user.dob != null) {
      return DateFormat.yMMMMd('en_GB').format(widget.user.dob!);
    } else {
      return null;
    }
  }

  String? _initAddressNumber() {
    final AppUser user = widget.user;
    if (user.address?.number != null) {
      return user.address!.number.toString();
    } else {
      return null;
    }
  }

  String? _initStreetName() {
    final AppUser user = widget.user;
    if (user.address?.streetName != null) {
      return user.address!.streetName;
    } else {
      return null;
    }
  }

  String? _initSuburb() {
    final AppUser user = widget.user;
    if (user.address?.suburb != null) {
      return user.address!.suburb;
    } else {
      return null;
    }
  }

  String? _initCity() {
    final AppUser user = widget.user;
    if (user.address?.city != null) {
      return user.address!.city;
    } else {
      return null;
    }
  }

  @override
  void dispose() {
    _nameController.dispose();
    _phoneController.dispose();
    _emailController.dispose();
    _genderController.dispose();
    _dobController.dispose();
    _addressNumberController.dispose();
    _streetNameController.dispose();
    _suburbController.dispose();
    _cityController.dispose();
    super.dispose();
  }

  Future<void> _onSaveButtonTap() async {
    final FormState form = _formKey.currentState!;
    final bool isValid = form.validate();

    if (isValid) {
      context.read<UserBloc>().add(UpdateUser(user: _buildAppUser()));
    }
  }

  AppUser _buildAppUser() {
    return widget.user.copyWith(
      dob: _dob,
      uid: widget.user.uid,
      localAvatar: _avatar,
      address: _buildAddress(),
      name: _nameController.text,
      avatar: widget.user.avatar,
      phone: _phoneController.text,
      email: _emailController.text,
      contacts: widget.user.contacts,
      gender: _genderController.text,
      joinedDate: widget.user.joinedDate,
      messageToken: widget.user.messageToken,
    );
  }

  Address _buildAddress() {
    return Address(
      number: int.tryParse(_addressNumberController.text) ?? 0,
      streetName: _streetNameController.text,
      suburb: _suburbController.text,
      city: _cityController.text,
    );
  }

  @override
  Widget build(BuildContext context) {
    final bool isLoading =
        context.select((UserBloc bloc) => bloc.state) is UserLoadingState;
    return Scaffold(
      body: BlocListener<UserBloc, UserState>(
        listener: (BuildContext context, UserState state) {
          if (state is UserUpdateError) {
            errorSnackbar(context, state.error);
          } else if (state is UserUpdateSuccess) {
            Navigator.pop(context);
            successSnackbar(context, state.message);
          }
        },
        child: LoadingOverlay(
          isLoading: isLoading,
          child: CustomScrollView(
            physics: const BouncingScrollPhysics(),
            slivers: <Widget>[
              const SliverAppBar(
                pinned: false,
                floating: true,
                elevation: 0.0,
                title: Text('Edit Account'),
              ),
              Form(
                key: _formKey,
                child: SliverList(
                  delegate: SliverChildListDelegate(
                    <Widget>[
                      Center(
                        child: SizedBox(
                          height: 16.5.h,
                          width: 27.5.w,
                          child: Stack(
                            children: <Widget>[
                              _avatar == null
                                  ? CircleAvatar(
                                      radius: 12.5.w,
                                      backgroundImage:
                                          CachedNetworkImageProvider(
                                        widget.user.avatar,
                                      ),
                                    )
                                  : LocalBorderAvatar(
                                      imagePath: _avatar!,
                                      radius: 12.5.w,
                                      borderWidth: 0.0,
                                      borderColor: Colors.white,
                                    ),
                              Positioned(
                                top: 9.1.h,
                                left: 12.0.w,
                                child: ElevatedButton.icon(
                                  style: ElevatedButton.styleFrom(
                                    elevation: 0.0,
                                    padding: EdgeInsets.only(left: 2.1.w),
                                    shape: const CircleBorder(),
                                  ),
                                  onPressed: () async {
                                    _avatar = await ImageService.getImage();
                                    setState(() {});
                                  },
                                  label: const Text(''),
                                  icon: Icon(
                                    Icons.photo_camera_outlined,
                                    size: 3.8.h,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 2.0.h),
                      CupertinoFormSection.insetGrouped(
                        backgroundColor: Theme.of(context).backgroundColor,
                        margin: const EdgeInsets.all(12.0),
                        header: const Text('Account'),
                        children: <Widget>[
                          CupertinoFormRow(
                            prefix: const Text('Name'),
                            child: CupertinoTextFormFieldRow(
                              placeholder: 'Name',
                              controller: _nameController,
                              keyboardType: TextInputType.name,
                              textInputAction: TextInputAction.next,
                              validator: (String? val) => val.simpleValidate,
                            ),
                          ),
                          CupertinoFormRow(
                            prefix: const Text('Gender'),
                            child: CupertinoTextFormFieldRow(
                              readOnly: true,
                              placeholder: 'Gender',
                              controller: _genderController,
                              textInputAction: TextInputAction.next,
                              onTap: () {
                                showSingleSelectPicker(
                                  context,
                                  title: 'Gender',
                                  labelText: 'Gender',
                                  controller: _genderController,
                                  items: const <String>['Female', 'Male'],
                                );
                              },
                              validator: (String? val) => val.simpleValidate,
                            ),
                          ),
                          CupertinoFormRow(
                            prefix: const Text('DOB'),
                            child: CupertinoTextFormFieldRow(
                              readOnly: true,
                              controller: _dobController,
                              placeholder: 'Date of birth',
                              textInputAction: TextInputAction.next,
                              onTap: () {
                                showDateTimeSelector(
                                  context,
                                  initialDate: _dob,
                                  title: 'Date Of Birth',
                                  controller: _dobController,
                                  mode: CupertinoDatePickerMode.date,
                                  onDateChanged: (DateTime date) => _dob = date,
                                );
                              },
                              validator: (String? val) => val.simpleValidate,
                            ),
                          ),
                        ],
                      ),
                      CupertinoFormSection.insetGrouped(
                        backgroundColor: Theme.of(context).backgroundColor,
                        margin: const EdgeInsets.all(12.0),
                        header: const Text('Contact'),
                        children: <Widget>[
                          CupertinoFormRow(
                            prefix: const Text('Phone'),
                            child: CupertinoTextFormFieldRow(
                              placeholder: 'Phone number',
                              controller: _phoneController,
                              keyboardType: TextInputType.number,
                              textInputAction: TextInputAction.next,
                              validator: (String? val) =>
                                  val.simpleValidateNumber,
                            ),
                          ),
                          CupertinoFormRow(
                            prefix: const Text('Email'),
                            child: CupertinoTextFormFieldRow(
                              placeholder: 'Email address',
                              controller: _emailController,
                              textInputAction: TextInputAction.next,
                              keyboardType: TextInputType.emailAddress,
                              validator: (String? val) =>
                                  val.simpleValidateEmail,
                            ),
                          ),
                        ],
                      ),
                      CupertinoFormSection.insetGrouped(
                        backgroundColor: Theme.of(context).backgroundColor,
                        margin: const EdgeInsets.all(12.0),
                        header: const Text('Address'),
                        children: <Widget>[
                          CupertinoFormRow(
                            prefix: const Text('Address number'),
                            child: CupertinoTextFormFieldRow(
                              placeholder: 'Address number',
                              keyboardType: TextInputType.number,
                              controller: _addressNumberController,
                              textInputAction: TextInputAction.next,
                              validator: (String? val) =>
                                  val.simpleValidateNumber,
                            ),
                          ),
                          CupertinoFormRow(
                            prefix: const Text('Street Name'),
                            child: CupertinoTextFormFieldRow(
                              placeholder: 'Street Name',
                              controller: _streetNameController,
                              textInputAction: TextInputAction.next,
                              keyboardType: TextInputType.streetAddress,
                              validator: (String? val) => val.simpleValidate,
                            ),
                          ),
                          CupertinoFormRow(
                            prefix: const Text('Suburb'),
                            child: CupertinoTextFormFieldRow(
                              placeholder: 'Suburb',
                              controller: _suburbController,
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              validator: (String? val) => val.simpleValidate,
                            ),
                          ),
                          CupertinoFormRow(
                            prefix: const Text('City'),
                            child: CupertinoTextFormFieldRow(
                              placeholder: 'City',
                              controller: _cityController,
                              textInputAction: TextInputAction.next,
                              validator: (String? val) => val.simpleValidate,
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 3.2.w, vertical: 3.0.h),
                        child: ActionButton(
                          title: 'Save',
                          onPressed: _onSaveButtonTap,
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

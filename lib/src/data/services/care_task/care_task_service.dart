import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/analytics/analytics.dart';
import '../../../common/constants/constants.dart';
import '../../references.dart';
import 'care_task_repository.dart';

class CareTaskService implements CareTaskRepository {
  @override
  Stream<List<CareTask>> fetchCareTasks(String patientId) {
    try {
      final Stream<QuerySnapshot<Map<String, dynamic>>> query = userCollection
          .doc(patientId)
          .collection(kCareTasks)
          .orderBy(kLastUpdated, descending: true)
          .snapshots();

      return query.map((QuerySnapshot<Map<String, dynamic>> snapshot) =>
          snapshot.docs.map((QueryDocumentSnapshot<Map<String, dynamic>> doc) {
            if (doc.data()[kType] == kMedication) {
              return MedicationTask.fromJson(doc.data());
            } else if (doc.data()[kType] == kMeal) {
              return MealTask.fromJson(doc.data());
            } else {
              return CareTask.fromJson(doc.data());
            }
          }).toList());
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Future<void> createCareTask(CareTask task) async {
    try {
      await userCollection
          .doc(task.patientId)
          .collection(kCareTasks)
          .doc(task.taskId)
          .set(task.toJson(), SetOptions(merge: true))
          .timeout(kTimeOut);
      analytics.logEvent(name: kCareTaskCreatedEvt);
    } on Exception {
      throw UpdateDataException(kOperationFailed);
    }
  }

  @override
  Future<void> removeCareTask(CareTask task) async {
    try {
      await userCollection
          .doc(task.patientId)
          .collection(kCareTasks)
          .doc(task.taskId)
          .delete()
          .timeout(kTimeOut);
    } on Exception {
      throw UpdateDataException(kOperationFailed);
    }
  }
}

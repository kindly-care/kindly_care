import 'package:flutter/material.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../common/constants/constants.dart';

class PatientRecordsPage extends StatelessWidget {
  final Patient patient;

  const PatientRecordsPage({super.key, required this.patient});

  void _onTemperatureTap(BuildContext context) {
    Navigator.pushNamed(context, kTemperaturePageRoute, arguments: patient);
  }

  void _onWeightTap(BuildContext context) {
    Navigator.pushNamed(context, kWeightPageRoute, arguments: patient);
  }

  void _onBloodGlucoseTap(BuildContext context) {
    Navigator.pushNamed(context, kBloodGlucosePageRoute, arguments: patient);
  }

  void _onBloodPressureTap(BuildContext context) {
    Navigator.pushNamed(context, kBloodPressurePageRoute, arguments: patient);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${patient.name.firstName()}'s Vitals"),
      ),
      body: ListView(
        padding: EdgeInsets.all(3.0.w),
        physics: const BouncingScrollPhysics(),
        children: <Widget>[
          TextListTile(
            title: 'Blood Pressure',
            subtitle: 'View Blood Pressure Records',
            onTap: () => _onBloodPressureTap(context),
          ),
          Divider(indent: 5.w, endIndent: 5.0.w),
          TextListTile(
            title: 'Temperature',
            subtitle: 'View Temperature Records',
            onTap: () => _onTemperatureTap(context),
          ),
          Divider(indent: 5.w, endIndent: 5.0.w),
          TextListTile(
            title: 'Blood Glucose',
            subtitle: 'View Blood Glucose Records',
            onTap: () => _onBloodGlucoseTap(context),
          ),
          Divider(indent: 5.w, endIndent: 5.0.w),
          TextListTile(
            title: 'Weight',
            subtitle: 'View Weight Records',
            onTap: () => _onWeightTap(context),
          ),
        ],
      ),
    );
  }
}

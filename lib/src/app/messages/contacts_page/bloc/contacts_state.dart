part of 'contacts_bloc.dart';

abstract class ContactsState extends Equatable {
  const ContactsState();

  @override
  List<Object> get props => <Object>[];
}

class ContactsInitial extends ContactsState {}

class ContactsLoading extends ContactsState {}

class ContactsLoadSuccess extends ContactsState {
  final List<AppUser> contacts;
  const ContactsLoadSuccess({required this.contacts});

  @override
  List<Object> get props => <Object>[contacts];
}

class ContactsLoadError extends ContactsState {
  final String message;
  const ContactsLoadError({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class AddContactInProgress extends ContactsState {}

class AddContactSuccess extends ContactsState {
  final String message;
  const AddContactSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class AddContactError extends ContactsState {
  final String message;
  const AddContactError({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class RemoveContactInProgress extends ContactsState {}

class RemoveContactSuccess extends ContactsState {
  final String message;
  const RemoveContactSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class RemoveContactError extends ContactsState {
  final String message;
  const RemoveContactError({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../data/services/patient/patient_repository.dart';

part 'patient_event.dart';
part 'patient_state.dart';

class PatientBloc extends Bloc<PatientEvent, PatientState> {
  final PatientRepository _patientRepository;

  PatientBloc({required PatientRepository patientRepository})
      : _patientRepository = patientRepository,
        super(PatientInitial()) {
    on<AddPatient>(_onAddPatient);
    on<RemovePatient>(_onRemovePatient);
    on<FetchPatient>(_onFetchPatient);
  }

  Future<void> _onAddPatient(
      AddPatient event, Emitter<PatientState> emit) async {
    emit(AddPatientInProgress());
    final bool isNewPatient = event.isNewPatient;

    try {
      await _patientRepository.createPatient(event.patient,
          isNewPatient: isNewPatient);
      emit(AddPatientSuccess(
        message: isNewPatient
            ? 'Care recipient created.'
            : 'Care recipient updated.',
      ));
    } on UpdateDataException catch (e) {
      emit(AddPatientError(error: e.toString()));
    }
  }

  Future<void> _onRemovePatient(
      RemovePatient event, Emitter<PatientState> emit) async {
    emit(RemovePatientInProgress());

    try {
      await _patientRepository.deleteCareRecipient(event.patientId);
      emit(const RemovePatientSuccess(message: 'Care Recipient removed.'));
    } on UpdateDataException catch (e) {
      emit(RemovePatientError(error: e.toString()));
    }
  }

  Future<void> _onFetchPatient(
      FetchPatient event, Emitter<PatientState> emit) async {
    emit(PatientLoading());

    await emit.forEach<Patient?>(
      _patientRepository.fetchPatient(event.patientId),
      onData: (Patient? patient) => PatientLoadSuccess(patient: patient),
      onError: (_, __) => const PatientLoadError(error: 'Failed to load data'),
    );
  }
}

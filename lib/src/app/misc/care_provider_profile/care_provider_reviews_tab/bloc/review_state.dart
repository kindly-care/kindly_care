part of 'review_bloc.dart';

abstract class ReviewState extends Equatable {
  const ReviewState();

  @override
  List<Object> get props => <Object>[];
}

class ReviewInitial extends ReviewState {}

class ReviewsLoading extends ReviewState {}

class ReviewsLoadSuccess extends ReviewState {
  final List<Review> reviews;
  const ReviewsLoadSuccess({required this.reviews});

  @override
  List<Object> get props => <Object>[reviews];
}

class ReviewsLoadError extends ReviewState {
  final String error;
  const ReviewsLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class SubmitReviewInProgress extends ReviewState {}

class SubmitReviewSuccess extends ReviewState {
  final String message;
  const SubmitReviewSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class SubmitReviewError extends ReviewState {
  final String error;
  const SubmitReviewError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

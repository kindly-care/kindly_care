part of 'subscription_bloc.dart';

abstract class SubscriptionEvent extends Equatable {
  const SubscriptionEvent();

  @override
  List<Object> get props => <Object>[];
}

class InitializePurchases extends SubscriptionEvent {
  final Patient patient;
  const InitializePurchases({required this.patient});

  @override
  List<Object> get props => <Object>[patient];
}

class FetchPurchases extends SubscriptionEvent {
  final String patientId;
  const FetchPurchases({required this.patientId});

  @override
  List<Object> get props => <Object>[patientId];
}

class PurchaseSubscription extends SubscriptionEvent {
  final Product product;
  const PurchaseSubscription({required this.product});

  @override
  List<Object> get props => <Object>[product];
}

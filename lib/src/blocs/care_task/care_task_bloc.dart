import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../data/services/care_task/care_task_repository.dart';

part 'care_task_event.dart'; 
part 'care_task_state.dart';

class CareTaskBloc extends Bloc<CareTaskEvent, CareTaskState> {
  final CareTaskRepository _careTaskRepository;

  CareTaskBloc({required CareTaskRepository careTaskRepository})
      : _careTaskRepository = careTaskRepository,
        super(CareTaskInitial()) {
    on<FetchCareTasks>(_onFetchCareTasks);
    on<CreateCareTask>(_onCreateCareTask);
    on<RemoveCareTask>(_onRemoveCareTask);
  }

  Future<void> _onFetchCareTasks(
      FetchCareTasks event, Emitter<CareTaskState> emit) async {
    emit(const CareTasksLoading());

    await emit.forEach<List<CareTask>>(
      _careTaskRepository.fetchCareTasks(event.patientId),
      onData: (List<CareTask> tasks) => CareTasksLoadSuccess(tasks: tasks),
      onError: (_, __) =>
          const CareTasksLoadError(error: 'Failed to load data'),
    );
  }

  Future<void> _onCreateCareTask(
      CreateCareTask event, Emitter<CareTaskState> emit) async {
    emit(CreateCareTaskInProgress());
    try {
      await _careTaskRepository.createCareTask(event.task);
      emit(const CreateCareTaskSuccess(message: 'Care Tasks updated.'));
    } on UpdateDataException catch (e) {
      emit(CreateCareTaskError(error: e.toString()));
    }
  }

  Future<void> _onRemoveCareTask(
      RemoveCareTask event, Emitter<CareTaskState> emit) async {
    emit(RemoveCareTaskInProgress());
    try {
      await _careTaskRepository.removeCareTask(event.task);
      emit(const RemoveCareTaskSuccess(message: 'Care Tasks updated.'));
    } on UpdateDataException catch (e) {
      emit(RemoveCareTaskError(error: e.toString()));
    }
  }
}

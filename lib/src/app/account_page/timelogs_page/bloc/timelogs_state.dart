part of 'timelogs_bloc.dart';

abstract class TimelogsState extends Equatable {
  const TimelogsState();

  @override
  List<Object> get props => <Object>[];
}

class TimelogsInitial extends TimelogsState {}

class TimelogsLoading extends TimelogsState {}

class TimelogsLoadSuccess extends TimelogsState {
  final List<TimeLog> timelogs;
  const TimelogsLoadSuccess({required this.timelogs});

  @override
  List<Object> get props => <Object>[timelogs];
}

class TimelogsLoadError extends TimelogsState {
  final String error;
  const TimelogsLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

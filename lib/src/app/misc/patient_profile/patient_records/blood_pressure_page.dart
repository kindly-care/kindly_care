import 'package:collection/collection.dart';
import 'package:dart_date/dart_date.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../data/services/services.dart';
import '../../../../data/services/vitals/vitals_services.dart';
import '../../../widgets/subscription_expired.dart';
import 'bloc/patient_records_bloc.dart';

class BloodPressurePage extends StatelessWidget {
  final Patient patient;
  const BloodPressurePage({super.key, required this.patient});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PatientRecordsBloc>(
      create: (BuildContext context) {
        return PatientRecordsBloc(
          vitalsRepository: context.read<VitalsService>(),
        )..add(FetchBloodPressureRecords(
            patientId: patient.uid, month: DateTime.now().month));
      },
      child: _BloodPressurePageView(patient: patient),
    );
  }
}

class _BloodPressurePageView extends StatefulWidget {
  final Patient patient;
  const _BloodPressurePageView({Key? key, required this.patient})
      : super(key: key);

  @override
  State<_BloodPressurePageView> createState() => _BloodPressurePageViewState();
}

class _BloodPressurePageViewState extends State<_BloodPressurePageView> {
  int _month = DateTime.now().month;

  void _onFetchBloodPressure() {
    context.read<PatientRecordsBloc>().add(FetchBloodPressureRecords(
        month: _month, patientId: widget.patient.uid));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        title: const Text('Blood Pressure'),
        actions: <Widget>[
          MonthPopupButton(
            icon: Icons.today_outlined,
            onChanged: (int month) {
              setState(() => _month = month);
              _onFetchBloodPressure();
            },
          ),
        ],
      ),
      body: BlocBuilder<PatientRecordsBloc, PatientRecordsState>(
        builder: (BuildContext context, PatientRecordsState state) {
          if (state is PatientRecordsLoadError) {
            return LoadErrorWidget(
              message: state.error,
              onRetry: _onFetchBloodPressure,
            );
          } else if (state is PatientBloodPressureRecordsLoadSuccess) {
            if (widget.patient.subscriptionEnds.isPast) {
              return SubscriptionExpired(
                patient: widget.patient,
                content: 'Blood Pressure records',
                detail: 'Blood Pressure records currently not available.',
              );
            } else {
              return _BloodPressureTableView(
                month: _month,
                records: state.records,
              );
            }
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _BloodPressureTableView extends StatelessWidget {
  final int month;
  final List<BloodPressureRecord> records;
  const _BloodPressureTableView(
      {Key? key, required this.month, required this.records})
      : super(key: key);

  static List<String> columns = <String>[
    'Care Pro',
    'Date',
    'Blood Pressure',
  ];

  @override
  Widget build(BuildContext context) {
    final DateTime date = DateTime.now().setMonth(month);
    final TextTheme theme = Theme.of(context).textTheme;
    final String monthYear = DateFormat.yMMMM('en_GB').format(date);
    if (records.isEmpty) {
      return NoDataWidget('No Blood Pressure records found for $monthYear.');
    } else {
      return InteractiveViewer(
        alignPanAxis: true,
        constrained: false,
        scaleEnabled: false,
        child: DataTable(
          columnSpacing: 17.5.w,
          horizontalMargin: 2.5.w,
          columns: columns
              .mapIndexed((int index, String item) => DataColumn(
                    label: Text(
                      item,
                      style: theme.bodyMedium?.copyWith(
                        fontWeight: FontWeight.w600,
                        color: Colors.black.withOpacity(0.7),
                      ),
                    ),
                    numeric: index == 2,
                  ))
              .toList(),
          rows: records.map((BloodPressureRecord record) {
            final String systolic = '${record.systolic.truncate()}';
            final String diastolic = '${record.diastolic.truncate()}';
            final String bloodPressure = '$systolic/$diastolic mmHg';

            final String date =
                DateFormat('dd/MM', 'en_GB').add_jm().format(record.takenAt);

            final String name = record.careProviderName.firstNameLastInitial();

            final List<String> cells = <String>[
              name,
              date,
              bloodPressure,
            ];
            return DataRow(
              cells: cells
                  .map((String item) => DataCell(Text(
                        item,
                        style: theme.bodyMedium,
                      )))
                  .toList(),
            );
          }).toList(),
        ),
      );
    }
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../blocs/care_provider/care_provider_bloc.dart';
import '../../../../common/constants/constants.dart';
import '../../../../cubits/account_cubit/account_cubit.dart';
import '../../../../data/services/services.dart';

class AssignCareProviderPage extends StatelessWidget {
  const AssignCareProviderPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CareProviderBloc>(
      create: (BuildContext context) => CareProviderBloc(
        patientRepository: context.read<PatientService>(),
        notificationRepository: context.read<NotificationService>(),
        careProviderRepository: context.read<CareProviderService>(),
      ),
      child: const _AssignCareProviderPageView(),
    );
  }
}

class _AssignCareProviderPageView extends StatefulWidget {
  const _AssignCareProviderPageView({Key? key}) : super(key: key);

  @override
  State<_AssignCareProviderPageView> createState() =>
      _AssignCareProviderPageViewState();
}

class _AssignCareProviderPageViewState
    extends State<_AssignCareProviderPageView> {
  String? _shift;
  late GlobalKey<FormState> _formKey;
  late TextEditingController _emailController;
  late TextEditingController _patientController;

  @override
  void initState() {
    super.initState();
    _formKey = GlobalKey<FormState>();
    _emailController = TextEditingController();
    _patientController = TextEditingController();
  }

  Future<void> _onAddButtonTap(AppUser user, List<Patient> patients) async {
    final bool isValid = _formKey.currentState?.validate() ?? false;

    if (isValid) {
      final Patient patient = patients.firstWhere(
          (Patient patient) => patient.name == _patientController.text);
      context.read<CareProviderBloc>().add(AssignCareProvider(
            client: user,
            patient: patient,
            shift: _shift ?? kDay,
            email: _emailController.text,
          ));
    }
  }

  void _onErrorState(AppUser user, String message, List<Patient> patients) {
    showTwoButtonDialog(
      context,
      title: 'Error',
      content: message,
      buttonText1: 'Cancel',
      buttonText2: 'Retry',
      onPressed1: () => Navigator.pop(context),
      onPressed2: () {
        Navigator.pop(context);
        _onAddButtonTap(user, patients);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final List<Patient> patients = context
        .select((AccountCubit cubit) => cubit.state.patients)
        .where((Patient patient) => patient.nextOfKin.contains(user.uid))
        .toList();
    final bool isLoading = context.select((CareProviderBloc bloc) => bloc.state)
        is AssignCareProviderInProgress;
    final List<String> patientNames =
        patients.map((Patient patient) => patient.name).toList();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add Care Provider'),
      ),
      body: LoadingOverlay(
        isLoading: isLoading,
        child: BlocListener<CareProviderBloc, CareProviderState>(
          listener: (BuildContext context, CareProviderState state) {
            if (state is AssignCareProviderError) {
              _onErrorState(user, state.error, patients);
            } else if (state is AssignCareProviderSuccess) {
              Navigator.pop(context);
              successSnackbar(context, state.message);
            }
          },
          child: Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(3.0.w),
              physics: const BouncingScrollPhysics(),
              children: <Widget>[
                SizedBox(height: 1.0.h),
                MaterialTextField(
                  labelText: 'Email',
                  controller: _emailController,
                  onChanged: (_) => setState(() {}),
                  textInputType: TextInputType.emailAddress,
                  hintText: "Enter Care Provider's email address",
                  validator: (String? val) => val.simpleValidateEmail,
                ),
                SizedBox(height: 2.5.h),
                DropdownTextField(
                  items: patientNames,
                  labelText: 'Care Recipient',
                  controller: _patientController,
                  hintText: 'Select care recipient',
                  onChanged: (_) => setState(() {}),
                  validator: (String? val) => val.simpleValidate,
                ),
                SizedBox(height: 2.5.h),
                DropdownTextField(
                  items: kCareShifts,
                  labelText: 'Care Shift',
                  hintText: 'Select care shift',
                  onChanged: (String val) {
                    setState(() => _shift = val);
                  },
                  validator: (String? val) => val.simpleValidate,
                ),
                SizedBox(height: 24.0.h),
                ActionButton(
                  title: 'Add Care Provider',
                  enabled: _isButtonEnabled(),
                  onPressed: () => _onAddButtonTap(user, patients),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool _isButtonEnabled() {
    if (_emailController.text.isNotEmpty &&
        _patientController.text.isNotEmpty &&
        _shift != null) {
      return true;
    } else {
      return false;
    }
  }

  @override
  void dispose() {
    _emailController.dispose();
    _patientController.dispose();
    super.dispose();
  }
}

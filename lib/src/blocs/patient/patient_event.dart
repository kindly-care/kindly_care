part of 'patient_bloc.dart';

abstract class PatientEvent extends Equatable {
  const PatientEvent();

  @override
  List<Object> get props => <Object>[];
}

class AddPatient extends PatientEvent {
  final Patient patient;
  final bool isNewPatient;
  const AddPatient({required this.patient, required this.isNewPatient});

  @override
  List<Object> get props => <Object>[patient, isNewPatient];
}

class FetchPatient extends PatientEvent {
  final String patientId;
  const FetchPatient({required this.patientId});

  @override
  List<Object> get props => <Object>[patientId];
}

class RemovePatient extends PatientEvent {
  final String patientId;
  const RemovePatient({required this.patientId});

  @override
  List<Object> get props => <Object>[patientId];
}

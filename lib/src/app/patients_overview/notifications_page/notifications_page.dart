import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../blocs/notification/notification_bloc.dart';
import '../../misc/patient_profile/care_reports_page/care_report_detail_page/care_report_detail_page.dart';

enum _NotificationsPopupAction { delete }

class NotificationsPage extends StatefulWidget {
  final String uid;

  const NotificationsPage({super.key, required this.uid});

  @override
  State<NotificationsPage> createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  @override
  void initState() {
    super.initState();
    _fetchNotifications();
  }

  void _fetchNotifications() {
    context.read<NotificationBloc>().add(FetchNotifications(uid: widget.uid));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Notifications'),
        actions: const <Widget>[
          _NotificationsPopupButton(),
        ],
      ),
      body: BlocConsumer<NotificationBloc, NotificationState>(
        listenWhen: (NotificationState previous, NotificationState current) {
          return current is DeleteNotificationSuccess ||
              current is DeleteNotificationError;
        },
        listener: (BuildContext context, NotificationState state) {
          if (state is DeleteNotificationSuccess) {
            successSnackbar(context, state.message);
          } else if (state is DeleteNotificationError) {
            errorSnackbar(context, state.message);
          }
        },
        buildWhen: (_, NotificationState current) {
          return current is NotificationsLoading ||
              current is NotificationsLoadSuccess ||
              current is NotificationsLoadError;
        },
        builder: (BuildContext context, NotificationState state) {
          if (state is NotificationsLoadError) {
            return LoadErrorWidget(
              message: state.message,
              onRetry: _fetchNotifications,
            );
          } else if (state is NotificationsLoadSuccess) {
            return _NotificationsPageView(
              uid: widget.uid,
              notifications: state.notifications,
            );
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _NotificationsPageView extends StatefulWidget {
  final String uid;
  final List<PushNotification> notifications;
  const _NotificationsPageView(
      {Key? key, required this.uid, required this.notifications})
      : super(key: key);

  @override
  _NotificationsPageViewState createState() => _NotificationsPageViewState();
}

class _NotificationsPageViewState extends State<_NotificationsPageView> {
  void _onNotificationTap(PushNotification notification) {
    if (!notification.isSeen) {
      context.read<NotificationBloc>().add(MarkNotificationAsSeen(
          uid: widget.uid, notificationId: notification.id));
    }

    if (notification is ReportNotification) {
      _onViewCareReport(notification);
    } else {
      _showGenericNotificationDialog(notification);
    }
  }

  void _onNotificationLongPress(PushNotification notification) {
    HapticFeedback.lightImpact();
    if (notification is ReportNotification) {
      _onReportNotificationLongPress(notification);
    } else {
      _onPushNotificationLongPress(notification);
    }
  }

  void _onViewCareReport(ReportNotification notification) {
    Navigator.of(context).push(
      CupertinoPageRoute<void>(
        builder: (_) => CareReportDetailPage(
          reportId: notification.reportId,
          patientId: notification.patientId,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (widget.notifications.isEmpty) {
      return const NoDataWidget('You currently do not have any notifications.');
    } else {
      return ListView.builder(
        padding: EdgeInsets.symmetric(horizontal: 3.0.w, vertical: 1.0.h),
        physics: const BouncingScrollPhysics(),
        itemCount: widget.notifications.length,
        itemBuilder: (BuildContext context, int index) {
          final PushNotification notification = widget.notifications[index];
          return NotificationCard(
            notification: notification,
            onTap: () => _onNotificationTap(notification),
            onLongPress: () => _onNotificationLongPress(notification),
          );
        },
      );
    }
  }

  void _onReportNotificationLongPress(ReportNotification notification) {
    if (!notification.isSeen) {
      context.read<NotificationBloc>().add(MarkNotificationAsSeen(
          uid: widget.uid, notificationId: notification.id));
    }

    optionsBottomSheet(
      height: 32.0.h,
      context: context,
      title: notification.title,
      options: <Widget>[
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            _onViewCareReport(notification);
          },
          title: 'View Report',
          showTrailing: false,
          icon: FeatherIcons.eye,
        ),
        Divider(indent: 18.w),
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            context.read<NotificationBloc>().add(DeleteNotification(
                  uid: widget.uid,
                  notificationId: notification.id,
                ));
          },
          title: 'Delete Notification',
          showTrailing: false,
          icon: FeatherIcons.trash2,
        ),
      ],
    );
  }

  void _onPushNotificationLongPress(PushNotification notification) {
    if (!notification.isSeen) {
      context.read<NotificationBloc>().add(MarkNotificationAsSeen(
          uid: widget.uid, notificationId: notification.id));
    }

    final String name = notification.authorName.firstName();
    optionsBottomSheet(
      height: 32.0.h,
      context: context,
      title: notification.title,
      options: <Widget>[
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onMakeCall(notification.authorPhone);
          },
          title: 'Call $name',
          showTrailing: false,
          icon: FeatherIcons.phone,
        ),
        Divider(indent: 18.w),
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            context.read<NotificationBloc>().add(DeleteNotification(
                  uid: widget.uid,
                  notificationId: notification.id,
                ));
          },
          title: 'Delete Notification',
          showTrailing: false,
          icon: FeatherIcons.trash2,
        ),
      ],
    );
  }

  void _showGenericNotificationDialog(PushNotification notification) {
    showOneButtonDialog(
      context,
      title: notification.title,
      content: notification.text,
      buttonText: 'Ok',
      onPressed: () => Navigator.pop(context),
    );
  }
}

class _NotificationsPopupButton extends StatelessWidget {
  const _NotificationsPopupButton({Key? key}) : super(key: key);

  void _showConfirmDeleteDialog(BuildContext context, AppUser user) {
    showTwoButtonDialog(
      context,
      isDestructiveAction: true,
      title: 'Delete Notifications',
      content: 'Are you sure you want to delete all notifications?',
      buttonText1: 'Cancel',
      buttonText2: 'Delete',
      onPressed1: () => Navigator.pop(context),
      onPressed2: () {
        Navigator.pop(context);
        context
            .read<NotificationBloc>()
            .add(DeleteAllNotifications(uid: user.uid));
      },
    );
  }

  void _onPopupAction(
      BuildContext context, _NotificationsPopupAction action, AppUser user) {
    switch (action) {
      case _NotificationsPopupAction.delete:
        _showConfirmDeleteDialog(context, user);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final bool notificationsIsNotEmpty = context
        .select((NotificationBloc bloc) => bloc.notifications)
        .isNotEmpty;

    return PopupMenuButton<_NotificationsPopupAction>(
      icon: Icon(Icons.more_vert_outlined, size: 3.8.h),
      onSelected: (_NotificationsPopupAction value) {
        _onPopupAction(context, value, user);
      },
      itemBuilder: (BuildContext context) {
        return <PopupMenuEntry<_NotificationsPopupAction>>[
          PopupMenuItem<_NotificationsPopupAction>(
            enabled: notificationsIsNotEmpty,
            padding: EdgeInsets.only(left: 1.0.w),
            value: _NotificationsPopupAction.delete,
            child: MenuElement(
              title: 'Delete All Notifications',
              enabled: notificationsIsNotEmpty,
            ),
          ),
        ];
      },
    );
  }
}

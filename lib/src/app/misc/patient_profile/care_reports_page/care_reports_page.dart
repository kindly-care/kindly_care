import 'package:dart_date/dart_date.dart';
import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../common/common.dart';
import '../../../../data/services/services.dart';
import 'bloc/care_reports_bloc.dart';
import 'care_report_detail_page/care_report_detail_page.dart';
import 'widgets/report_tile.dart';

class CareReportsPage extends StatelessWidget {
  final Patient patient;
  const CareReportsPage({super.key, required this.patient});

  @override
  Widget build(BuildContext context) {
    final int month = DateTime.now().month;
    return BlocProvider<CareReportsBloc>(
      create: (BuildContext context) => CareReportsBloc(
        vitalsRepository: context.read<VitalsService>(),
        reportRepository: context.read<ReportService>(),
        patientRepository: context.read<PatientService>(),
      )..add(FetchCareReports(month: month, patientId: patient.uid)),
      child: _CareReportsPageView(patient),
    );
  }
}

class _CareReportsPageView extends StatefulWidget {
  final Patient patient;
  const _CareReportsPageView(this.patient, {Key? key}) : super(key: key);

  @override
  State<_CareReportsPageView> createState() => _PatientReportsPageViewState();
}

class _PatientReportsPageViewState extends State<_CareReportsPageView> {
  int _month = DateTime.now().month;

  void _onLoadCareReports() {
    context
        .read<CareReportsBloc>()
        .add(FetchCareReports(month: _month, patientId: widget.patient.uid));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Care Reports'),
        actions: <Widget>[
          MonthPopupButton(
            icon: Icons.today_outlined,
            onChanged: (int month) {
              setState(() => _month = month);
              _onLoadCareReports();
            },
          ),
        ],
      ),
      body: BlocBuilder<CareReportsBloc, CareReportsState>(
        builder: (BuildContext context, CareReportsState state) {
          if (state is CareReportsLoadError) {
            return LoadErrorWidget(
              message: 'Failed to load data',
              onRetry: _onLoadCareReports,
            );
          } else if (state is CareReportsLoadSuccess) {
            return _PageContent(
              month: _month,
              reports: state.reports,
              patient: widget.patient,
            );
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _PageContent extends StatelessWidget {
  final int month;
  final Patient patient;
  final List<Report> reports;
  const _PageContent({
    Key? key,
    required this.month,
    required this.patient,
    required this.reports,
  }) : super(key: key);

  void _onCareReportTap(BuildContext context, Report report) {
    Navigator.of(context).push(
      CupertinoPageRoute<void>(
        builder: (_) => CareReportDetailPage(
          patientId: patient.uid,
          reportId: report.reportId,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);

    if (reports.isEmpty) {
      final DateTime date = DateTime.now().setMonth(month);
      final String monthYear = DateFormat.yMMMM('en_GB').format(date);
      return NoDataWidget('No Care Reports found for $monthYear.');
    } else {
      return ListView.separated(
        itemCount: reports.length,
        physics: const BouncingScrollPhysics(),
        separatorBuilder: (_, __) => Divider(indent: 2.0.w, endIndent: 2.0.w),
        itemBuilder: (BuildContext context, int index) {
          final Report report = reports[index];
          return Padding(
            padding: EdgeInsets.symmetric(horizontal: 2.5.w, vertical: 0.8.h),
            child: ReportTile(
              report: report,
              onTap: () => _onCareReportTap(context, report),
              onLongPress: () =>
                  _onCareReportLongPress(context, report, user.uid),
            ),
          );
        },
      );
    }
  }

  void _onCareReportLongPress(BuildContext context, Report report, String uid) {
    HapticFeedback.lightImpact();
    optionsBottomSheet(
      height: 32.0.h,
      context: context,
      title: report.careProviderName,
      options: <Widget>[
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onMakeCall(report.careProviderPhone);
          },
          title: 'Call',
          showTrailing: false,
          icon: FeatherIcons.phone,
        ),
        Divider(indent: 18.w),
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onCreateChannel(context, uid: uid, otherId: report.careProviderId);
          },
          title: 'Message',
          showTrailing: false,
          icon: FeatherIcons.messageSquare,
        ),
      ],
    );
  }
}

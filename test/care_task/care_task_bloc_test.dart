import 'package:bloc_test/bloc_test.dart';
import 'package:casa_home/src/blocs/care_task/care_task_bloc.dart';
import 'package:casa_home/src/data/services/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:mocktail/mocktail.dart';

import '../test_data.dart';

class MockCareTaskRepository extends Mock implements CareTaskRepository {}

void main() {
  late CareTaskRepository careTaskRepository;

  setUp(() {
    careTaskRepository = MockCareTaskRepository();
  });

  group('CareTaskBloc', () {
    test('initial state is CareTaskInitial', () {
      final CareTaskBloc careTaskBloc = CareTaskBloc(
        careTaskRepository: careTaskRepository,
      );
      expect(careTaskBloc.state, CareTaskInitial());
      careTaskBloc.close();
    });

    group('FetchCareTasks', () {
      blocTest<CareTaskBloc, CareTaskState>(
        'emits [CareTasksLoadSuccess] when [FetchCareTasks] is added and '
        'fetchCareTasks is successfully called',
        setUp: () {
          when(() => careTaskRepository.fetchCareTasks(patientId)).thenAnswer(
              (_) => Stream<List<CareTask>>.value(<CareTask>[careTask]));
        },
        build: () => CareTaskBloc(careTaskRepository: careTaskRepository),
        act: (CareTaskBloc bloc) =>
            bloc.add(const FetchCareTasks(patientId: patientId)),
        expect: () => <Matcher>[
          isA<CareTasksLoading>(),
          isA<CareTasksLoadSuccess>(),
        ],
      );
    });

    group('CreateCareTask', () {
      blocTest<CareTaskBloc, CareTaskState>(
        'emits [CreateCareTaskError] when [CreateCareTask] is added and '
        'createCareTask throws',
        setUp: () {
          when(() => careTaskRepository.createCareTask(careTask))
              .thenThrow(UpdateDataException(errorMessage));
        },
        build: () => CareTaskBloc(careTaskRepository: careTaskRepository),
        act: (CareTaskBloc bloc) => bloc.add(CreateCareTask(task: careTask)),
        expect: () => <Matcher>[
          isA<CreateCareTaskInProgress>(),
          isA<CreateCareTaskError>(),
        ],
      );

      blocTest<CareTaskBloc, CareTaskState>(
        'emits [CreateCareTaskSuccess] when [CreateCareTask] is added and '
        'createCareTask is successfully called',
        setUp: () {
          when(() => careTaskRepository.createCareTask(careTask))
              .thenAnswer((_) async {});
        },
        build: () => CareTaskBloc(careTaskRepository: careTaskRepository),
        act: (CareTaskBloc bloc) => bloc.add(CreateCareTask(task: careTask)),
        expect: () => <Matcher>[
          isA<CreateCareTaskInProgress>(),
          isA<CreateCareTaskSuccess>(),
        ],
      );
    });

    group('RemoveCareTask', () {
      blocTest<CareTaskBloc, CareTaskState>(
        'emits [RemoveCareTaskError] when [RemoveCareTask] is added and '
        'removeCareTask throws',
        setUp: () {
          when(() => careTaskRepository.removeCareTask(careTask))
              .thenThrow(UpdateDataException(errorMessage));
        },
        build: () => CareTaskBloc(careTaskRepository: careTaskRepository),
        act: (CareTaskBloc bloc) => bloc.add(RemoveCareTask(task: careTask)),
        expect: () => <Matcher>[
          isA<RemoveCareTaskInProgress>(),
          isA<RemoveCareTaskError>(),
        ],
      );

      blocTest<CareTaskBloc, CareTaskState>(
        'emits [RemoveCareTaskSuccess] when [RemoveCareTask] is added and '
        'removeCareTask is successfully called',
        setUp: () {
          when(() => careTaskRepository.removeCareTask(careTask))
              .thenAnswer((_) async {});
        },
        build: () => CareTaskBloc(careTaskRepository: careTaskRepository),
        act: (CareTaskBloc bloc) => bloc.add(RemoveCareTask(task: careTask)),
        expect: () => <Matcher>[
          isA<RemoveCareTaskInProgress>(),
          isA<RemoveCareTaskSuccess>(),
        ],
      );
    });
  });
}

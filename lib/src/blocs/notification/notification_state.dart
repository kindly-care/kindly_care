part of 'notification_bloc.dart';

class NotificationState extends Equatable {
  const NotificationState();

  @override
  List<Object> get props => <Object>[];
}

class NotificationInitial extends NotificationState {}

class NotificationsLoading extends NotificationState {}

class NotificationsLoadSuccess extends NotificationState {
  final List<PushNotification> notifications;
  const NotificationsLoadSuccess({required this.notifications});

  @override
  List<Object> get props => <Object>[notifications];
}

class NotificationsLoadError extends NotificationState {
  final String message;
  const NotificationsLoadError({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class DeleteNotificationInProgress extends NotificationState {}

class DeleteNotificationSuccess extends NotificationState {
  final String message;
  const DeleteNotificationSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class DeleteNotificationError extends NotificationState {
  final String message;
  const DeleteNotificationError({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class SendNotificationInProgress extends NotificationState {}

class SendNotificationSuccess extends NotificationState {
  final String message;
  const SendNotificationSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class SendNotificationError extends NotificationState {
  final String message;
  const SendNotificationError({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

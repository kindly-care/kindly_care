part of 'patient_bloc.dart';

abstract class PatientState extends Equatable {
  const PatientState();

  @override
  List<Object?> get props => <Object?>[];
}

class PatientInitial extends PatientState {}

class PatientLoading extends PatientState {}

class PatientLoadSuccess extends PatientState {
  final Patient? patient;
  const PatientLoadSuccess({required this.patient});

  @override
  List<Object?> get props => <Object?>[patient];
}

class PatientLoadError extends PatientState {
  final String error;
  const PatientLoadError({required this.error});

  @override
  List<Object?> get props => <Object?>[error];
}

class AddPatientInProgress extends PatientState {}

class AddPatientSuccess extends PatientState {
  final String message;
  const AddPatientSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class AddPatientError extends PatientState {
  final String error;
  const AddPatientError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class RemovePatientInProgress extends PatientState {}

class RemovePatientSuccess extends PatientState {
  final String message;
  const RemovePatientSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class RemovePatientError extends PatientState {
  final String error;
  const RemovePatientError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

import 'package:dart_date/dart_date.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import 'item_tile.dart';

class HealthRecords extends StatelessWidget {
  final Patient patient;
  const HealthRecords({super.key, required this.patient});

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final Color color = Colors.black.withOpacity(0.65);
    return Theme(
      data: Theme.of(context).copyWith(
        dividerColor: Colors.transparent,
      ),
      child: ExpansionTile(
        title: Text('Health Records', style: theme.titleMedium),
        iconColor: color,
        textColor: color,
        collapsedIconColor: color,
        collapsedTextColor: color,
        children: <Widget>[
          ItemTile(
            itemType: 'Weight',
            item: _getLastWeight(),
            time: _getLastWeightTime(),
          ),
          SizedBox(height: 1.0.h),
          ItemTile(
            itemType: 'Temperature',
            item: _getLastTemperature(),
            time: _getLastTemperatureTime(),
          ),
          SizedBox(height: 1.0.h),
          ItemTile(
            itemType: 'Blood Glucose',
            item: _getLastBloodGlucose(),
            time: _getLastBloodSugarTime(),
          ),
          SizedBox(height: 1.0.h),
          ItemTile(
            itemType: 'Blood Pressure',
            item: _getLastBloodPressure(),
            time: _getLastBloodPressureTime(),
          ),
        ],
      ),
    );
  }

  String _getLastWeight() {
    final num? weight = patient.lastWeightRecord?.weight;
    if (weight == null) {
      return 'No record';
    } else if (patient.subscriptionEnds.isPast) {
      return 'Data currently not available.';
    } else {
      return '$weight kg';
    }
  }

  String? _getLastWeightTime() {
    final DateTime? date = patient.lastWeightRecord?.takenAt;
    if (date == null) {
      return null;
    } else {
      return DateFormat('EEE, d MMM H:m', 'en_GB').format(date);
    }
  }

  String _getLastTemperature() {
    final num? temperature = patient.lastTemperatureRecord?.temperature;
    if (temperature == null) {
      return 'No record';
    } else if (patient.subscriptionEnds.isPast) {
      return 'Data currently not available.';
    } else {
      return '$temperature °C';
    }
  }

  String? _getLastTemperatureTime() {
    final DateTime? date = patient.lastTemperatureRecord?.takenAt;
    if (date == null) {
      return null;
    } else {
      return DateFormat('EEE, d MMM H:m', 'en_GB').format(date);
    }
  }

  String _getLastBloodPressure() {
    final BloodPressureRecord? bloodPressure = patient.lastBloodPressureRecord;
    if (bloodPressure == null) {
      return 'No record';
    } else if (patient.subscriptionEnds.isPast) {
      return 'Data currently not available.';
    } else {
      final num systolic = patient.lastBloodPressureRecord!.systolic;
      final num diastolic = patient.lastBloodPressureRecord!.diastolic;
      return '$systolic/$diastolic mmHg';
    }
  }

  String? _getLastBloodPressureTime() {
    final DateTime? date = patient.lastBloodPressureRecord?.takenAt;
    if (date == null) {
      return null;
    } else {
      return DateFormat('EEE, d MMM H:m', 'en_GB').format(date);
    }
  }

  String _getLastBloodGlucose() {
    final BloodGlucoseRecord? bloodGlucose = patient.lastBloodGlucoseRecord;
    if (bloodGlucose == null) {
      return 'No record';
    } else if (patient.subscriptionEnds.isPast) {
      return 'Data currently not available.';
    } else {
      final num bloodGlucose = patient.lastBloodGlucoseRecord!.bloodGlucose;

      return '$bloodGlucose mmol/L';
    }
  }

  String? _getLastBloodSugarTime() {
    final DateTime? date = patient.lastBloodGlucoseRecord?.takenAt;
    if (date == null) {
      return null;
    } else {
      return DateFormat('EEE, d MMM H:m', 'en_GB').format(date);
    }
  }
}

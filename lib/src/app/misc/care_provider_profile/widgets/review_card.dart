import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class ReviewCard extends StatelessWidget {
  final Review review;
  final VoidCallback onTap;
  const ReviewCard({super.key, required this.review, required this.onTap});

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final String createdAt = DateFormat.yMd('en_GB').format(review.createdAt);
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.only(
          left: 2.8.w,
          right: 2.8.w,
          top: 2.5.h,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(12.0),
          child: Material(
            elevation: 0.8,
            child: SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.all(1.2.h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          review.name,
                          style: theme.titleSmall?.copyWith(fontSize: 17.5.sp),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 0.5.w),
                          decoration: BoxDecoration(
                            color: Colors.teal.shade50,
                            borderRadius: BorderRadius.circular(4.5),
                          ),
                          child: Text(
                            createdAt,
                            style: theme.labelLarge?.copyWith(
                              fontSize: 17.0.sp,
                              fontWeight: FontWeight.w500,
                              color: Colors.black.withOpacity(0.6),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 0.5.h),
                    RatingBar.builder(
                      initialRating: review.rating,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemSize: 2.1.h,
                      unratedColor: Colors.grey.shade400,
                      itemBuilder: (BuildContext context, _) =>
                          const Icon(Icons.star, color: Color(0xFFFE7404)),
                      onRatingUpdate: (_) {},
                    ),
                    SizedBox(height: 1.0.h),
                    Text(
                      review.text,
                      style: theme.bodyMedium?.copyWith(
                        fontSize: 17.5.sp,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

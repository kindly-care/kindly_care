import 'package:flutter/cupertino.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:stream_chat_flutter_core/stream_chat_flutter_core.dart';

import '../../app/account_page/care_providers/care_providers.dart';
import '../../app/account_page/patients/patients.dart';
import '../../app/account_page/subscriptions_page/subscriptions_page.dart';
import '../../app/account_page/timelogs_page/timelogs_page.dart';
import '../../app/home_page/home_page.dart';
import '../../app/messages/chat_page/chat_page.dart';
import '../../app/messages/contacts_page/contacts_page.dart';
import '../../app/misc/care_provider_profile/care_provider_profile.dart';
import '../../app/misc/care_recipient_select/care_recipient_select_page.dart';
import '../../app/misc/patient_map/patient_map_page.dart';
import '../../app/misc/patient_profile/care_reports_page/care_reports.dart';
import '../../app/misc/patient_profile/care_tasks_page/care_tasks_page.dart';
import '../../app/misc/patient_profile/patient_profile_page.dart';
import '../../app/misc/patient_profile/patient_records/care_recipient_records.dart';
import '../../app/misc/user_profile/edit_user_page/edit_user_page.dart';
import '../../app/misc/user_profile/user_profile.dart';
import '../../app/patients_overview/notifications_page/notifications_page.dart';
import '../../app/settings_page/settings_page.dart';
import '../constants/constants.dart';
import 'error_route_page.dart';

class AppRoute {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final Object? args = settings.arguments;
    switch (settings.name) {
      case kCreateAccountPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => const CreateAccountPage(),
        );
      case kHomePageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => const HomePage(),
        );
      case kResetPasswordPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => const ResetPasswordPage(),
        );
      case kAddCircleMemberPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          fullscreenDialog: true,
          builder: (_) => AddCircleMemberPage(patientId: args as String),
        );
      case kLoginPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (BuildContext context) => LoginPage(
            onAuthSuccess: (_) =>
                Navigator.popAndPushNamed(context, kHomePageRoute),
          ),
        );
      case kContactsPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          fullscreenDialog: true,
          builder: (_) => const ContactsPage(),
        );

      case kNotificationsPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => NotificationsPage(uid: args as String),
        );
      case kPatientsPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => const PatientsPage(),
        );
      case kSubmitReviewPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          fullscreenDialog: true,
          builder: (_) => SubmitReviewPage(careProvider: args as CareProvider),
        );
      case kAssignCareProviderPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          fullscreenDialog: true,
          builder: (_) => const AssignCareProviderPage(),
        );
      case kRequestInterviewPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          fullscreenDialog: true,
          builder: (_) => RequestInterviewPage(careProviderId: args as String),
        );

      case kChatPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => ChatPage(channel: args as Channel),
        );

      case kPatientProfilePageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => PatientProfilePage(patientId: args as String),
        );

      case kCareProvidersPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => const CareProvidersPage(),
        );

      case kPatientSelectPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) =>
              CareRecipientSelectPage(destination: args as DestinationPage),
        );

      case kCareTasksPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => CareTasksPage(patient: args as Patient),
        );

      case kPatientReportsPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => CareReportsPage(patient: args as Patient),
        );

      case kCareProviderProfilePageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) =>
              CareProviderProfilePage(careProvider: args as CareProvider),
        );

      case kFindCarePageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => const FindCareProvidersPage(),
        );

      case kEditUserProfilePageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => const EditUserPage(),
        );
      case kAddPatientPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => AddPatientPage(patient: args as Patient?),
        );
      case kUserProfilePageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => const UserProfile(),
        );
      case kTemperaturePageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => TemperaturePage(patient: args as Patient),
        );
      case kWeightPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => WeightPage(patient: args as Patient),
        );
      case kBloodGlucosePageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => BloodGlucosePage(patient: args as Patient),
        );
      case kBloodPressurePageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => BloodPressurePage(patient: args as Patient),
        );

      case kSubscriptionsPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => SubscriptionsPage(patient: args as Patient),
        );
      case kTimeLogsPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => TimeLogsPage(patient: args as Patient),
        );

      case kSettingsPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => const SettingsPage(),
        );

      case kPatientMapPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => PatientMapPage(patient: args as Patient),
        );
      case kPatientRecordsPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => PatientRecordsPage(patient: args as Patient),
        );

      default:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => const ErrorRoutePage(),
        );
    }
  }
}

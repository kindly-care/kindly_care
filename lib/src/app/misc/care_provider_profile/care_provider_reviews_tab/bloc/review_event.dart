part of 'review_bloc.dart';

abstract class ReviewEvent extends Equatable {
  const ReviewEvent();

  @override
  List<Object> get props => <Object>[];
}

class FetchReviews extends ReviewEvent {
  final String careProviderId;
  const FetchReviews({required this.careProviderId});

  @override
  List<Object> get props => <Object>[careProviderId];
}

class SubmitReview extends ReviewEvent {
  final Review review;
  const SubmitReview({required this.review});

  @override
  List<Object> get props => <Object>[review];
}

part of 'contacts_bloc.dart';

abstract class ContactsEvent extends Equatable {
  const ContactsEvent();

  @override
  List<Object> get props => <Object>[];
}

class FetchContacts extends ContactsEvent {
  final List<String> contactIds;
  const FetchContacts({required this.contactIds});

  @override
  List<Object> get props => <Object>[contactIds];
}

class AddContact extends ContactsEvent {
  final String uid, email;
  const AddContact({required this.uid, required this.email});

  @override
  List<Object> get props => <Object>[uid, email];
}

class RemoveContact extends ContactsEvent {
  final String uid, contactId;
  const RemoveContact({required this.uid, required this.contactId});

  @override
  List<Object> get props => <Object>[uid, contactId];
}

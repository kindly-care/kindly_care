import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dart_date/dart_date.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/constants/constants.dart';
import '../../references.dart';
import 'report_repository.dart';

class ReportService implements ReportRepository {
  @override
  Future<List<Report>> fetchReportsByMonth(
      {required String patientId, required int month}) async {
    final DateTime day = DateTime.now().setMonth(month);

    final String start = DateFormat(kYMD).format(day.startOfMonth);
    final String end = DateFormat(kYMD).format(day.endOfMonth);

    try {
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .doc(patientId)
          .collection(kReports)
          .where(kDateCreated, isGreaterThanOrEqualTo: start)
          .where(kDateCreated, isLessThanOrEqualTo: end)
          .orderBy(kDateCreated, descending: true)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((DocumentSnapshot<Map<String, dynamic>> doc) =>
              Report.fromJson(doc.data()!))
          .toList();
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Future<Report?> fetchReport(
      {required String patientId, required String reportId}) async {
    try {
      final DocumentSnapshot<Map<String, dynamic>> queryDoc =
          await userCollection
              .doc(patientId)
              .collection(kReports)
              .doc(reportId)
              .get()
              .timeout(kTimeOut);

      if (queryDoc.exists) {
        return Report.fromJson(queryDoc.data()!);
      } else {
        return null;
      }
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }
}

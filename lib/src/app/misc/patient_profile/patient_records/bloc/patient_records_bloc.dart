import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../../../data/services/vitals/vitals_repository.dart';

part 'patient_records_event.dart';
part 'patient_records_state.dart';

class PatientRecordsBloc
    extends Bloc<PatientRecordsEvent, PatientRecordsState> {
  final VitalsRepository _vitalsRepository;

  PatientRecordsBloc({required VitalsRepository vitalsRepository})
      : _vitalsRepository = vitalsRepository,
        super(PatientRecordsInitial()) {
    on<FetchWeightRecords>(_onFetchWeightRecords);
    on<FetchTemperatureRecords>(_onFetchTemperatureRecords);
    on<FetchBloodPressureRecords>(_onFetchBloodPressureRecords);
    on<FetchBloodGlucoseRecords>(_onFetchBloodGlucoseRecords);
  }

  Future<void> _onFetchWeightRecords(
      FetchWeightRecords event, Emitter<PatientRecordsState> emit) async {
    emit(PatientRecordsLoading());

    try {
      final List<WeightRecord> records = await _vitalsRepository
          .fetchWeightByMonth(patientId: event.patientId, month: event.month);

      emit(PatientWeightRecordsLoadSuccess(records: records));
    } on FetchDataException catch (e) {
      emit(PatientRecordsLoadError(error: e.toString()));
    }
  }

  Future<void> _onFetchTemperatureRecords(
      FetchTemperatureRecords event, Emitter<PatientRecordsState> emit) async {
    emit(PatientRecordsLoading());
    try {
      final List<TemperatureRecord> records =
          await _vitalsRepository.fetchTemperatureByMonth(
              patientId: event.patientId, month: event.month);

      emit(PatientTemperatureRecordsLoadSuccess(records: records));
    } on FetchDataException catch (e) {
      emit(PatientRecordsLoadError(error: e.toString()));
    }
  }

  Future<void> _onFetchBloodPressureRecords(FetchBloodPressureRecords event,
      Emitter<PatientRecordsState> emit) async {
    emit(PatientRecordsLoading());
    try {
      final List<BloodPressureRecord> records =
          await _vitalsRepository.fetchBloodPressureByMonth(
              patientId: event.patientId, month: event.month);

      emit(PatientBloodPressureRecordsLoadSuccess(records: records));
    } on FetchDataException catch (e) {
      emit(PatientRecordsLoadError(error: e.toString()));
    }
  }

  Future<void> _onFetchBloodGlucoseRecords(
      FetchBloodGlucoseRecords event, Emitter<PatientRecordsState> emit) async {
    emit(PatientRecordsLoading());
    try {
      final List<BloodGlucoseRecord> records =
          await _vitalsRepository.fetchBloodGlucoseByMonth(
              patientId: event.patientId, month: event.month);

      emit(PatientBloodGlucoseRecordsLoadSuccess(records: records));
    } on FetchDataException catch (e) {
      emit(PatientRecordsLoadError(error: e.toString()));
    }
  }
}

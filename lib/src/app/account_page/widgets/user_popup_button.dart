import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class UserPopupButton extends StatefulWidget {
  final List<AssignedCareProvider> careProvider;
  final Function(AssignedCareProvider) onChanged;
  const UserPopupButton(
      {super.key, required this.careProvider, required this.onChanged});

  @override
  UserPopupButtonState createState() => UserPopupButtonState();
}

class UserPopupButtonState extends State<UserPopupButton> {
  late String _groupValue;

  @override
  void initState() {
    super.initState();
    _groupValue = widget.careProvider.firstOrNull?.careProviderName ?? '';
  }

  void _onCareProviderChanged(String? name) {
    final AssignedCareProvider careProvider = widget.careProvider.firstWhere(
        (AssignedCareProvider provider) => provider.careProviderName == name);
    Navigator.pop(context);
    widget.onChanged(careProvider);
    setState(() => _groupValue = name!);
  }

  List<PopupMenuItem<String>> _getCareProviders() {
    final TextTheme theme = Theme.of(context).textTheme;
    final Color primaryColor = Theme.of(context).primaryColor;

    return widget.careProvider.map((AssignedCareProvider agent) {
      return PopupMenuItem<String>(
        child: RadioListTile<String>(
          controlAffinity: ListTileControlAffinity.trailing,
          activeColor: primaryColor,
          title: Text(agent.careProviderName, style: theme.titleSmall),
          groupValue: _groupValue,
          value: agent.careProviderName,
          onChanged: _onCareProviderChanged,
        ),
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<String>(
      icon: Icon(Icons.person_outline,
          size: 3.8.h, color: Colors.black.withOpacity(0.6)),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0)),
      itemBuilder: (BuildContext context) {
        return <PopupMenuEntry<String>>[..._getCareProviders()];
      },
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../blocs/care_provider/care_provider_bloc.dart';
import '../../../../common/common.dart';
import '../../../../common/constants/constants.dart';
import '../../../../data/services/services.dart';

class FindCareProvidersPage extends StatelessWidget {
  const FindCareProvidersPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CareProviderBloc>(
      create: (BuildContext context) => CareProviderBloc(
        patientRepository: context.read<PatientService>(),
        notificationRepository: context.read<NotificationService>(),
        careProviderRepository: context.read<CareProviderService>(),
      )..add(const FetchAllCareProviders()),
      child: const FindCareProvidersPageView(),
    );
  }
}

class FindCareProvidersPageView extends StatelessWidget {
  const FindCareProvidersPageView({super.key});

  void _onLoadAgents(BuildContext context) {
    context.read<CareProviderBloc>().add(const FetchAllCareProviders());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text(kCareProviders)),
      body: BlocBuilder<CareProviderBloc, CareProviderState>(
        builder: (BuildContext context, CareProviderState state) {
          if (state is CareProvidersLoadError) {
            return LoadErrorWidget(
              message: state.error,
              onRetry: () => _onLoadAgents(context),
            );
          } else if (state is CareProvidersLoadSuccess) {
            return _PageContent(key: key, careProviders: state.careProviders);
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _PageContent extends StatelessWidget {
  final List<CareProvider> careProviders;
  const _PageContent({super.key, required this.careProviders});

  void _onCareProviderTap(BuildContext context, CareProvider careProvider) {
    Navigator.pushNamed(context, kCareProviderProfilePageRoute,
        arguments: careProvider);
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    if (careProviders.isEmpty) {
      return const NoDataWidget(
          'No $kCareProviders found. Please check again later.');
    } else {
      return ListView.separated(
        shrinkWrap: true,
        itemCount: careProviders.length,
        padding: EdgeInsets.all(2.5.w),
        physics: const BouncingScrollPhysics(),
        separatorBuilder: (_, __) => SizedBox(height: 1.0.h),
        itemBuilder: (BuildContext context, int index) {
          final CareProvider careProvider = careProviders[index];

          return Padding(
            padding: EdgeInsets.only(bottom: 1.5.h),
            child: _CareProviderCard(
              careProvider: careProvider,
              onTap: () => _onCareProviderTap(context, careProvider),
              onLongPress: () =>
                  _onCareProviderLongPress(context, careProvider, user),
            ),
          );
        },
      );
    }
  }

  void _onCareProviderLongPress(
      BuildContext context, CareProvider careProvider, AppUser user) {
    HapticFeedback.lightImpact();
    optionsBottomSheet(
      height: 32.0.h,
      context: context,
      title: careProvider.name,
      options: <Widget>[
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onMakeCall(careProvider.phone);
          },
          title: 'Call',
          showTrailing: false,
          icon: FeatherIcons.phone,
        ),
        Divider(indent: 18.w),
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onCreateChannel(context, uid: user.uid, otherId: careProvider.uid);
          },
          title: 'Message',
          showTrailing: false,
          icon: FeatherIcons.messageSquare,
        ),
      ],
    );
  }
}

class _CareProviderCard extends StatelessWidget {
  final VoidCallback onTap;
  final VoidCallback onLongPress;
  final CareProvider careProvider;

  const _CareProviderCard({
    Key? key,
    required this.onTap,
    required this.onLongPress,
    required this.careProvider,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return ListTile(
      onTap: onTap,
      onLongPress: onLongPress,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(9.5)),
      contentPadding: EdgeInsets.symmetric(horizontal: 2.0.w, vertical: 0.5.h),
      leading: CircleAvatar(
        radius: 3.8.h,
        backgroundImage: CachedNetworkImageProvider(careProvider.avatar),
      ),
      title: Text(
        careProvider.name,
        style: theme.titleMedium,
        overflow: TextOverflow.ellipsis,
      ),
      subtitle: Padding(
        padding: EdgeInsets.only(top: 0.5.h, bottom: 0.5.h),
        child: Text(
          '${getAvailability(careProvider)}, ${careProvider.address?.city ?? ''}',
          style: theme.titleSmall?.copyWith(fontSize: 16.8.sp),
        ),
      ),
      trailing: Icon(Icons.chevron_right, size: 3.8.h),
    );
  }
}

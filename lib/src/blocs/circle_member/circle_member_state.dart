part of 'circle_member_bloc.dart';

abstract class CircleMemberState extends Equatable {
  const CircleMemberState();

  @override
  List<Object> get props => <Object>[];
}

class CircleMemberInitial extends CircleMemberState {}

class CircleMembersLoading extends CircleMemberState {}

class CircleMembersLoadSuccess extends CircleMemberState {
  final List<AppUser> circleMembers;
  const CircleMembersLoadSuccess({required this.circleMembers});

  @override
  List<Object> get props => <Object>[circleMembers];
}

class CircleMembersLoadError extends CircleMemberState {
  final String error;
  const CircleMembersLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class AddingCircleMemberInProgress extends CircleMemberState {}

class CircleMemberAddSuccess extends CircleMemberState {
  final String message;
  const CircleMemberAddSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class CircleMemberAddError extends CircleMemberState {
  final String error;
  const CircleMemberAddError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class CircleMemberRemoveSuccess extends CircleMemberState {
  final String message;
  const CircleMemberRemoveSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class CircleMemberRemoveError extends CircleMemberState {
  final String error;
  const CircleMemberRemoveError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

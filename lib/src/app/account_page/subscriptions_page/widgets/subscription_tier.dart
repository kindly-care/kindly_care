import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class SubscriptionTier extends StatelessWidget {
  final String price;
  final String period;
  final VoidCallback onTap;
  const SubscriptionTier({
    super.key,
    required this.onTap,
    required this.price,
    required this.period,
  });

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return InkWell(
      splashColor: Colors.teal.shade50,
      borderRadius: BorderRadius.circular(11.0),
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 0.8.w),
        height: 21.5.h,
        width: 18.0.h,
        decoration: BoxDecoration(
          color: theme.primaryColorLight,
          border: Border.all(
            width: 1.0.w,
            color: Colors.black12,
          ),
          borderRadius: BorderRadius.circular(11.0),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(period, style: theme.textTheme.bodyLarge),
              SizedBox(height: 1.5.h),
              Text(price, style: theme.textTheme.bodyMedium),
            ],
          ),
        ),
      ),
    );
  }
}

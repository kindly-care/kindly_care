import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../../../data/services/care_provider/care_provider_repository.dart';

part 'review_event.dart';
part 'review_state.dart';

class ReviewBloc extends Bloc<ReviewEvent, ReviewState> {
  final CareProviderRepository _careProviderRepository;

  ReviewBloc({required CareProviderRepository careProviderRepository})
      : _careProviderRepository = careProviderRepository,
        super(ReviewInitial()) {
    on<FetchReviews>(_onFetchReviews);
    on<SubmitReview>(_onSubmitReview);
  }

  Future<void> _onFetchReviews(
      FetchReviews event, Emitter<ReviewState> emit) async {
    emit(ReviewsLoading());

    await emit.forEach<List<Review>>(
      _careProviderRepository.fetchReviews(event.careProviderId),
      onData: (List<Review> reviews) => ReviewsLoadSuccess(reviews: reviews),
      onError: (_, __) => const ReviewsLoadError(error: 'Failed to load data'),
    );
  }

  Future<void> _onSubmitReview(
      SubmitReview event, Emitter<ReviewState> emit) async {
    emit(SubmitReviewInProgress());

    try {
      await _careProviderRepository.submitReview(event.review);
      emit(const SubmitReviewSuccess(message: 'Review submitted.'));
    } on UpdateDataException catch (e) {
      emit(SubmitReviewError(error: e.toString()));
    }
  }
}

import 'package:bloc_test/bloc_test.dart';
import 'package:casa_home/src/blocs/patient/patient_bloc.dart';
import 'package:casa_home/src/data/services/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:mocktail/mocktail.dart';

import '../test_data.dart';

class MockPatientRepository extends Mock implements PatientRepository {}

void main() {
  late PatientRepository patientRepository;

  setUp(() {
    patientRepository = MockPatientRepository();
  });

  group('PatientBloc', () {
    test('initial state is PatientInitial', () {
      final PatientBloc patientBloc =
          PatientBloc(patientRepository: patientRepository);
      expect(patientBloc.state, PatientInitial());
      patientBloc.close();
    });

    group('AddPatient', () {
      blocTest<PatientBloc, PatientState>(
        'emits [AddPatientError] when [AddPatient] is added and '
        'createPatient throws',
        setUp: () {
          when(() =>
                  patientRepository.createPatient(patient, isNewPatient: true))
              .thenThrow(UpdateDataException(errorMessage));
        },
        build: () => PatientBloc(patientRepository: patientRepository),
        act: (PatientBloc bloc) =>
            bloc.add(AddPatient(patient: patient, isNewPatient: true)),
        expect: () => <Matcher>[
          isA<AddPatientInProgress>(),
          isA<AddPatientError>(),
        ],
      );

      blocTest<PatientBloc, PatientState>(
        'emits [AddPatientSuccess] when [AddPatient] is added and '
        'createPatient is successfully called',
        setUp: () {
          when(() =>
                  patientRepository.createPatient(patient, isNewPatient: true))
              .thenAnswer((_) async {});
        },
        build: () => PatientBloc(patientRepository: patientRepository),
        act: (PatientBloc bloc) =>
            bloc.add(AddPatient(patient: patient, isNewPatient: true)),
        expect: () => <Matcher>[
          isA<AddPatientInProgress>(),
          isA<AddPatientSuccess>(),
        ],
      );
    });

    group('RemovePatient', () {
      blocTest<PatientBloc, PatientState>(
        'emits [RemovePatientError] when [RemovePatient] is added and '
        'deleteCareRecipient throws',
        setUp: () {
          when(() => patientRepository.deleteCareRecipient(patientId))
              .thenThrow(UpdateDataException(errorMessage));
        },
        build: () => PatientBloc(patientRepository: patientRepository),
        act: (PatientBloc bloc) =>
            bloc.add(const RemovePatient(patientId: patientId)),
        expect: () => <Matcher>[
          isA<RemovePatientInProgress>(),
          isA<RemovePatientError>(),
        ],
      );

      blocTest<PatientBloc, PatientState>(
        'emits [RemovePatientSuccess] when [RemovePatient] is added and '
        'deleteCareRecipient is successfully called',
        setUp: () {
          when(() => patientRepository.deleteCareRecipient(patientId))
              .thenAnswer((_) async {});
        },
        build: () => PatientBloc(patientRepository: patientRepository),
        act: (PatientBloc bloc) =>
            bloc.add(const RemovePatient(patientId: patientId)),
        expect: () => <Matcher>[
          isA<RemovePatientInProgress>(),
          isA<RemovePatientSuccess>(),
        ],
      );
    });

    group('FetchPatient', () {
      blocTest<PatientBloc, PatientState>(
        'emits [PatientLoadSuccess] when [FetchPatient] is added and '
        'fetchPatient is successfully called',
        setUp: () {
          when(() => patientRepository.fetchPatient(patientId))
              .thenAnswer((_) => Stream<Patient>.value(patient));
        },
        build: () => PatientBloc(patientRepository: patientRepository),
        act: (PatientBloc bloc) =>
            bloc.add(const FetchPatient(patientId: patientId)),
        expect: () => <Matcher>[
          isA<PatientLoading>(),
          isA<PatientLoadSuccess>(),
        ],
      );
    });
  });
}

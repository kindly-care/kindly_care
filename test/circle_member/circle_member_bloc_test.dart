import 'package:bloc_test/bloc_test.dart';
import 'package:casa_home/src/blocs/circle_member/circle_member_bloc.dart';

import 'package:casa_home/src/data/services/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:mocktail/mocktail.dart';

import '../test_data.dart';

class MockAuthRepository extends Mock implements AuthRepository {}

class MockCircleRepository extends Mock implements CircleRepository {}

void main() {
  late AuthRepository authRepository;
  late CircleRepository circleRepository;

  setUp(() {
    authRepository = MockAuthRepository();
    circleRepository = MockCircleRepository();
  });

  group('CircleMemberBloc', () {
    test('initial state is CareTaskInitial', () {
      final CircleMemberBloc careTaskBloc = CircleMemberBloc(
        authRepository: authRepository,
        circleRepository: circleRepository,
      );
      expect(careTaskBloc.state, CircleMemberInitial());
      careTaskBloc.close();
    });

    group('FetchCircleMembers', () {
      blocTest<CircleMemberBloc, CircleMemberState>(
        'emits [CircleMembersLoadSuccess] when [FetchCircleMembers] is added and '
        'fetchPatientCircle is successfully called',
        setUp: () {
          when(() => circleRepository.fetchPatientCircle(<String>[memberId]))
              .thenAnswer((_) => Stream<List<AppUser>>.value(<AppUser>[user]));
        },
        build: () => CircleMemberBloc(
          authRepository: authRepository,
          circleRepository: circleRepository,
        ),
        act: (CircleMemberBloc bloc) =>
            bloc.add(const FetchCircleMembers(memberIds: <String>[memberId])),
        expect: () => <Matcher>[
          isA<CircleMembersLoading>(),
          isA<CircleMembersLoadSuccess>(),
        ],
      );
    });

    group('RemoveCircleMember', () {
      blocTest<CircleMemberBloc, CircleMemberState>(
        'emits [CircleMemberRemoveError] when [RemoveCircleMember] is added and '
        'removeCircleMember throws',
        setUp: () {
          when(() => circleRepository.removeCircleMember(
                memberId: memberId,
                patientId: patientId,
              )).thenThrow(UpdateDataException(errorMessage));
        },
        build: () => CircleMemberBloc(
          authRepository: authRepository,
          circleRepository: circleRepository,
        ),
        act: (CircleMemberBloc bloc) => bloc.add(
            const RemoveCircleMember(memberId: memberId, patientId: patientId)),
        expect: () => <Matcher>[isA<CircleMemberRemoveError>()],
      );

      blocTest<CircleMemberBloc, CircleMemberState>(
        'emits [CircleMemberRemoveSuccess] when [RemoveCircleMember] is added and '
        'removeCircleMember is successfully called',
        setUp: () {
          when(() => circleRepository.removeCircleMember(
                memberId: memberId,
                patientId: patientId,
              )).thenAnswer((_) async {});
        },
        build: () => CircleMemberBloc(
          authRepository: authRepository,
          circleRepository: circleRepository,
        ),
        act: (CircleMemberBloc bloc) => bloc.add(
            const RemoveCircleMember(memberId: memberId, patientId: patientId)),
        expect: () => <Matcher>[isA<CircleMemberRemoveSuccess>()],
      );
    });
  });
}

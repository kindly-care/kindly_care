import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../data/services/services.dart';

part 'circle_member_event.dart';
part 'circle_member_state.dart';

class CircleMemberBloc extends Bloc<CircleMemberEvent, CircleMemberState> {
  final AuthRepository _authRepository;
  final CircleRepository _circleRepository;

  CircleMemberBloc({ 
    required AuthRepository authRepository,
    required CircleRepository circleRepository,
  })  : _authRepository = authRepository,
        _circleRepository = circleRepository,
        super(CircleMemberInitial()) {
    on<FetchCircleMembers>(_onFetchCircleMembers);
    on<AddCircleMember>(_onAddCircleMember);
    on<RemoveCircleMember>(_onRemoveCircleMember);
  }

  Future<void> _onFetchCircleMembers(
      FetchCircleMembers event, Emitter<CircleMemberState> emit) async {
    emit(CircleMembersLoading());

    await emit.forEach<List<AppUser>>(
      _circleRepository.fetchPatientCircle(event.memberIds),
      onData: (List<AppUser> members) =>
          CircleMembersLoadSuccess(circleMembers: members),
      onError: (_, __) =>
          const CircleMembersLoadError(error: 'Failed to load data'),
    );
  }

  Future<void> _onAddCircleMember(
      AddCircleMember event, Emitter<CircleMemberState> emit) async {
    emit(AddingCircleMemberInProgress());

    try {
      final AppUser? user =
          await _authRepository.fetchUserByEmail(event.data.email);
      if (user == null) {
        emit(
          const CircleMemberAddError(
            error:
                'The provided email address is not associated with any user. Please check email address and try again.',
          ),
        );
      } else {
        await _circleRepository
            .addCircleMember(event.data.copyWith(memberId: user.uid));

        emit(CircleMemberAddSuccess(
            message:
                '${user.name} has been successfully added as a Circle Member.'));
      }
    } on Exception catch (e) {
      emit(
        CircleMemberAddError(error: e.toString()),
      );
    }
  }

  Future<void> _onRemoveCircleMember(
      RemoveCircleMember event, Emitter<CircleMemberState> emit) async {
    try {
      await _circleRepository.removeCircleMember(
          patientId: event.patientId, memberId: event.memberId);
      emit(const CircleMemberRemoveSuccess(message: 'Circle Member removed.'));
    } on UpdateDataException catch (e) {
      emit(
        CircleMemberRemoveError(error: e.toString()),
      );
    }
  }
}

import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:uuid/uuid.dart';

import '../../../blocs/notification/notification_bloc.dart';
import '../../../common/constants/constants.dart';
import '../../../cubits/cubits.dart';


class RequestInterviewPage extends StatefulWidget {
  final String careProviderId;
  const RequestInterviewPage({super.key, required this.careProviderId});

  @override
  State<RequestInterviewPage> createState() => _RequestInterviewPageState();
}

class _RequestInterviewPageState extends State<RequestInterviewPage> {
  DateTime? _startDate;
  String _shift = 'Day';
  Patient? _selectedPatient;
  late GlobalKey<FormState> _formKey;
  late TextEditingController _dateController;
  late TextEditingController _shiftController;
  late TextEditingController _patientController;
  late TextEditingController _positionController;

  @override
  void initState() {
    super.initState();
    _formKey = GlobalKey<FormState>();
    _dateController = TextEditingController();
    _shiftController = TextEditingController();
    _patientController = TextEditingController();
    _positionController = TextEditingController();
  }

  @override
  void dispose() {
    _dateController.dispose();
    _shiftController.dispose();
    _patientController.dispose();
    _positionController.dispose();
    super.dispose();
  }

  Future<void> _onPushCareRequestNotification(AppUser user) async {
    final bool isValid = _formKey.currentState?.validate() ?? false;

    if (isValid) {
      final String date =
          DateFormat.MMMMd('en_GB').add_jm().format(_startDate!);

      final String notificationId = const Uuid().v4();
      final InterviewNotification notification = InterviewNotification(
        shift: _shift,
        id: notificationId,
        startDate: _startDate!,
        patientId: _selectedPatient!.uid,
        position: _positionController.text,
        text:
            '${user.name} is requesting a Care Interview for a  ${_positionController.text} position on $date.',
        title: 'Care Interview Request',
        authorId: user.uid,
        authorName: user.name,
        authorPhone: user.phone,
        createdAt: DateTime.now(),
        type: NotificationType.interviewRequest,
        recipients: <String>[widget.careProviderId],
      );

      context
          .read<NotificationBloc>()
          .add(SendPushNotification(notification: notification));
    }
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final List<Patient> patients = context
        .select((AccountCubit cubit) => cubit.state.patients)
        .where((Patient patient) => patient.nextOfKin.contains(user.uid))
        .toList();

    final bool isLoading = context.select((NotificationBloc bloc) => bloc.state)
        is SendNotificationInProgress;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Request Care Interview'),
      ),
      body: BlocListener<NotificationBloc, NotificationState>(
        listener: (BuildContext context, NotificationState state) {
          if (state is SendNotificationError) {
            errorSnackbar(context, state.message);
          } else if (state is SendNotificationSuccess) {
            Navigator.pop(context);
            successSnackbar(context, 'Interview request successfully sent');
          }
        },
        child: LoadingOverlay(
          isLoading: isLoading,
          child: Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(3.0.w),
              physics: const BouncingScrollPhysics(),
              children: <Widget>[
                SizedBox(height: 1.0.h),
                DropdownTextField(
                  items: kCareShifts,
                  labelText: 'Care Shift',
                  hintText: 'Select care shift',
                  controller: _shiftController,
                  focusNode: AlwaysDisabledFocusNode(),
                  onChanged: (String val) {
                    setState(() => _shift = val);
                  },
                  validator: (String? val) => val.simpleValidate,
                ),
                SizedBox(height: 2.5.h),
                DropdownTextField(
                  labelText: 'Work Position',
                  hintText: 'Select work position',
                  controller: _positionController,
                  focusNode: AlwaysDisabledFocusNode(),
                  onChanged: (String val) {
                    setState(() => _shift = val);
                  },
                  validator: (String? val) => val.simpleValidate,
                  items: const <String>['Permanent', 'Temporary'],
                ),
                SizedBox(height: 2.5.h),
                DropdownTextField(
                  labelText: 'Care Recipient',
                  controller: _patientController,
                  hintText: 'Select care recipient',
                  focusNode: AlwaysDisabledFocusNode(),
                  validator: (String? val) => val.simpleValidate,
                  onChanged: (String val) {
                    setState(() {
                      _selectedPatient = patients.firstWhereOrNull(
                          (Patient patient) =>
                              patient.name == _patientController.text);
                    });
                  },
                  items:
                      patients.map((Patient patient) => patient.name).toList(),
                ),
                SizedBox(height: 2.5.h),
                MaterialTextField(
                  readOnly: true,
                  enableInteractiveSelection: false,
                  focusNode: AlwaysDisabledFocusNode(),
                  onTap: () {
                    showDateTimeSelector(
                      context,
                      title: 'Interview Date',
                      minDate: DateTime.now(),
                      initialDate: _startDate,
                      controller: _dateController,
                      mode: CupertinoDatePickerMode.dateAndTime,
                      format: DateFormat.yMMMMd('en_GB').add_jm(),
                      onDateChanged: (DateTime date) {
                        setState(() => _startDate = date);
                      },
                    );
                  },
                  labelText: 'Interview Date',
                  hintText: 'Select date',
                  controller: _dateController,
                  onChanged: (_) => setState(() {}),
                  prefixIcon: const Icon(Icons.event_outlined),
                  validator: (String? val) => val.simpleValidate,
                ),
                SizedBox(height: 16.5.h),
                ActionButton(
                  title: 'Send Request',
                  enabled: _buttonEnabled(),
                  onPressed: () {
                    _onPushCareRequestNotification(user);
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool _buttonEnabled() {
    return _dateController.text.isNotEmpty &&
        _shiftController.text.isNotEmpty &&
        _patientController.text.isNotEmpty &&
        _positionController.text.isNotEmpty;
  }
}

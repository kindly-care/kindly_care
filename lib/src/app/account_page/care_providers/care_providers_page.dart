import 'package:cached_network_image/cached_network_image.dart';
import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../blocs/care_provider/care_provider_bloc.dart';
import '../../../common/common.dart';
import '../../../common/constants/constants.dart';
import '../../../cubits/account_cubit/account_cubit.dart';
import '../../../data/services/services.dart';

enum _PopupAction { assignCareProvider, findCare }

class CareProvidersPage extends StatelessWidget {
  const CareProvidersPage({super.key});

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    return BlocProvider<CareProviderBloc>(
      create: (BuildContext context) => CareProviderBloc(
        patientRepository: context.read<PatientService>(),
        notificationRepository: context.read<NotificationService>(),
        careProviderRepository: context.read<CareProviderService>(),
      ),
      child: CareProvidersPageView(user),
    );
  }
}

class CareProvidersPageView extends StatefulWidget {
  final AppUser user;
  const CareProvidersPageView(this.user, {super.key});

  @override
  State<CareProvidersPageView> createState() => _CareProvidersPageViewState();
}

class _CareProvidersPageViewState extends State<CareProvidersPageView> {
  void _onFindCareProvidersTap() {
    Navigator.of(context).pushNamed(kFindCarePageRoute);
  }

  void _onCareProviderTap(CareProvider careProvider) {
    Navigator.pushNamed(context, kCareProviderProfilePageRoute,
        arguments: careProvider);
  }

  void _onDismissCareProvider(CareProvider careProvider, Patient patient) {
    context
        .read<CareProviderBloc>()
        .add(DismissCareProvider(careProvider: careProvider, patient: patient));
  }

  void _onAssignCareProviderTap() {
    Navigator.pushNamed(context, kAssignCareProviderPageRoute);
  }

  @override
  Widget build(BuildContext context) {
    final List<Patient> patients = context
        .select((AccountCubit cubit) => cubit.state.patients)
        .where(
            (Patient patient) => patient.circle.keys.contains(widget.user.uid))
        .toList();
    final List<CareProvider> careProviders =
        context.select((AccountCubit cubit) => cubit.state.careProviders);
    final bool isLoading = context.select((CareProviderBloc bloc) => bloc.state)
        is DismissCareProviderInProgress;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: const Text(kCareProviders),
        actions: <Widget>[
          _PopupButton(
            onFindCareTap: _onFindCareProvidersTap,
            onAssignCareProviderTap: _onAssignCareProviderTap,
          ),
        ],
      ),
      body: careProviders.isEmpty
          ? const NoDataWidget('You currently do not have any $kCareProviders.')
          : LoadingOverlay(
              isLoading: isLoading,
              child: BlocListener<CareProviderBloc, CareProviderState>(
                listener: (BuildContext context, CareProviderState state) {
                  if (state is DismissCareProviderError) {
                    errorSnackbar(context, state.error);
                  } else if (state is DismissCareProviderSuccess) {
                    successSnackbar(context, state.message);
                  }
                },
                child: ListView.builder(
                  padding: EdgeInsets.all(1.5.w),
                  itemCount: careProviders.length,
                  physics: const BouncingScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) {
                    final CareProvider careProvider = careProviders[index];
                    final List<Patient> careProviderPatients = patients
                        .where((Patient pat) => pat.assignedCareProviders.keys
                            .contains(careProvider.uid))
                        .toList();
                    return Padding(
                      padding: EdgeInsets.symmetric(vertical: 0.8.h),
                      child: _CareProviderCard(
                        careProvider: careProvider,
                        patients: careProviderPatients,
                        onTap: () => _onCareProviderTap(careProvider),
                        onLongPress: () =>
                            _onCareProviderLongPress(careProvider, patients),
                      ),
                    );
                  },
                ),
              ),
            ),
    );
  }

  void _onCareProviderLongPress(
      CareProvider careProvider, List<Patient> patients) {
    HapticFeedback.lightImpact();
    final bool visible = patients
        .where((Patient patient) => patient.nextOfKin.contains(widget.user.uid))
        .toList()
        .isNotEmpty;

    optionsBottomSheet(
      context: context,
      title: careProvider.name,
      height: visible ? 45.0.h : 32.0.h,
      options: <Widget>[
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onMakeCall(careProvider.phone);
          },
          title: 'Call',
          showTrailing: false,
          icon: FeatherIcons.phone,
        ),
        Divider(indent: 18.w),
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onCreateChannel(context,
                uid: widget.user.uid, otherId: careProvider.uid);
          },
          title: 'Message',
          showTrailing: false,
          icon: FeatherIcons.messageSquare,
        ),
        Divider(indent: 18.w),
        Visibility(
          visible: visible,
          child: IconListTile(
            onTap: () {
              Navigator.pop(context);
              _showRemoveCareProviderBottomSheet(careProvider, patients);
            },
            title: 'Dismiss Care Provider',
            showTrailing: false,
            icon: FeatherIcons.userMinus,
          ),
        ),
      ],
    );
  }

  void _showRemoveCareProviderBottomSheet(
      CareProvider careProvider, List<Patient> patients) {
    final List<Patient> clientPatients = patients
        .where((Patient patient) => patient.nextOfKin.contains(widget.user.uid))
        .toList();
    final List<String> patientNames =
        clientPatients.map((Patient patient) => patient.name).toList();

    showSingleSelectBottomSheet(
      context,
      items: patientNames,
      title: 'Dismiss For',
      onSelected: (String name) {
        final Patient patient = clientPatients
            .firstWhere((Patient patient) => patient.name == name);
        _showTerminateConfirmDialog(careProvider, patient);
      },
    );
  }

  void _showTerminateConfirmDialog(CareProvider careProvider, Patient patient) {
    showTwoButtonDialog(
      context,
      isDestructiveAction: true,
      title: 'Dismiss Care Provider',
      content:
          'Are you sure you want to dismiss Care Provider for ${patient.name}?',
      buttonText1: 'Cancel',
      buttonText2: 'Dismiss',
      onPressed1: () => Navigator.pop(context),
      onPressed2: () {
        Navigator.pop(context);
        _onDismissCareProvider(careProvider, patient);
      },
    );
  }
}

class _CareProviderCard extends StatelessWidget {
  final VoidCallback onTap;
  final List<Patient> patients;
  final VoidCallback onLongPress;
  final CareProvider careProvider;

  const _CareProviderCard({
    Key? key,
    required this.careProvider,
    required this.onTap,
    required this.patients,
    required this.onLongPress,
  }) : super(key: key);

  List<Widget> _getCareRecipients(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    if (patients.isEmpty) {
      return <Widget>[
        Text(
          '--',
          style: theme.titleSmall?.copyWith(fontSize: 16.5.sp),
        ),
      ];
    } else {
      return patients.map(
        (Patient patient) {
          final String shift = patient.assignedCareProviders.values
              .firstWhere((AssignedCareProvider ass) =>
                  ass.careProviderId == careProvider.uid)
              .shift;
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                patient.name,
                style: theme.titleSmall?.copyWith(fontSize: 17.2.sp),
              ),
              Text(
                '$shift Shift',
                style: theme.titleSmall?.copyWith(fontSize: 17.2.sp),
              ),
            ],
          );
        },
      ).toList();
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final String service = careProvider.occupation.join(', ');
    return Card(
      elevation: 0.1,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.5)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListTile(
            onTap: onTap,
            onLongPress: onLongPress,
            contentPadding: EdgeInsets.symmetric(horizontal: 2.0.w),
            leading: CircleAvatar(
              radius: 3.8.h,
              backgroundImage: CachedNetworkImageProvider(careProvider.avatar),
            ),
            title: Text(careProvider.name, style: theme.titleMedium),
            subtitle: Text(service, style: theme.titleSmall),
            trailing: Icon(Icons.chevron_right, size: 3.8.h),
          ),
          Divider(indent: 2.0.w, endIndent: 2.0.w),
          SizedBox(height: 1.0.h),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 2.5.w),
            child: Text(kCareRecipients, style: theme.titleMedium),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 2.5.w, vertical: 1.0.h),
            child: Column(
              children: _getCareRecipients(context),
            ),
          ),
          SizedBox(height: 1.0.h),
        ],
      ),
    );
  }
}

class _PopupButton extends StatelessWidget {
  final VoidCallback onFindCareTap;
  final VoidCallback onAssignCareProviderTap;
  const _PopupButton({
    Key? key,
    required this.onFindCareTap,
    required this.onAssignCareProviderTap,
  }) : super(key: key);

  void _onPopupActionSelected(_PopupAction action) {
    switch (action) {
      case _PopupAction.assignCareProvider:
        onAssignCareProviderTap();
        break;
      case _PopupAction.findCare:
        onFindCareTap();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<_PopupAction>(
      icon: Icon(Icons.more_vert_outlined, size: 3.8.h),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0)),
      onSelected: (_PopupAction action) => _onPopupActionSelected(action),
      itemBuilder: (BuildContext context) {
        return <PopupMenuEntry<_PopupAction>>[
          const PopupMenuItem<_PopupAction>(
            value: _PopupAction.findCare,
            child: MenuElement(title: 'Find Care Provider'),
          ),
          const PopupMenuDivider(),
          const PopupMenuItem<_PopupAction>(
            value: _PopupAction.assignCareProvider,
            child: MenuElement(title: 'Add Care Provider'),
          ),
        ];
      },
    );
  }
}

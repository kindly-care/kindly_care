import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/analytics/analytics.dart';
import '../../../common/constants/constants.dart';
import '../../references.dart';
import 'care_provider_repository.dart';

class CareProviderService implements CareProviderRepository {
  @override
  Stream<List<Review>> fetchReviews(String providerId) {
    try {
      final Stream<QuerySnapshot<Map<String, dynamic>>> query = userCollection
          .doc(providerId)
          .collection(kReviews)
          .orderBy(kCreatedAt, descending: true)
          .snapshots();

      return query.map((QuerySnapshot<Map<String, dynamic>> snapshot) =>
          snapshot.docs
              .map((QueryDocumentSnapshot<Map<String, dynamic>> doc) =>
                  Review.fromJson(doc.data()))
              .toList());
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Future<void> submitReview(Review review) async {
    try {
      await userCollection
          .doc(review.careProviderId)
          .collection(kReviews)
          .doc(review.authorId)
          .set(review.toJson())
          .timeout(kTimeOut);

      analytics.logEvent(name: kSubmitReviewEvt);
    } on Exception {
      throw UpdateDataException('Failed to submit review');
    }
  }

  @override
  Stream<CareProvider?> fetchCareProviderById(String id) {
    try {
      final Stream<DocumentSnapshot<Map<String, dynamic>>> docStream =
          userCollection.doc(id).snapshots();

      return docStream.map((DocumentSnapshot<Map<String, dynamic>> doc) =>
          doc.exists ? CareProvider.fromJson(doc.data()!) : null);
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Future<CareProvider?> fetchCareProviderByEmail(String email) async {
    try {
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .where(kEmail, isEqualTo: email)
          .where(kUserType, isEqualTo: kCareProvider)
          .get()
          .timeout(kTimeOut);

      if (query.docs.isNotEmpty) {
        return CareProvider.fromJson(query.docs.first.data());
      } else {
        return null;
      }
    } catch (e) {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Stream<List<CareProvider>> fetchCareProviders(List<String> ids) {
    try {
      if (ids.isEmpty) {
        ids.add('placeholderValue');
      }
      final Stream<QuerySnapshot<Map<String, dynamic>>> query = userCollection
          .where(kUID, whereIn: ids)
          .where(kUserType, isEqualTo: kCareProvider)
          .snapshots();

      return query.map((QuerySnapshot<Map<String, dynamic>> snapshot) =>
          snapshot.docs
              .map((QueryDocumentSnapshot<Map<String, dynamic>> doc) =>
                  CareProvider.fromJson(doc.data()))
              .toList());
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Future<List<CareProvider>> fetchAllCareProviders() async {
    try {
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .where(kUserType, isEqualTo: kCareProvider)
          .orderBy(kRating, descending: true)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((QueryDocumentSnapshot<Map<String, dynamic>> doc) =>
              CareProvider.fromJson(doc.data()))
          .toList();
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Future<void> assignCareProvider({
    required AppUser client,
    required AssignedPatient assignedPatient,
    required AssignedCareProvider assignedCareProvider,
  }) async {
    try {
      await userCollection
          .doc(assignedPatient.patientId)
          .update(<String, dynamic>{
        'assignedCareProviders.${assignedCareProvider.careProviderId}':
            assignedCareProvider.toJson(),
      }).timeout(kTimeOut);

      await userCollection
          .doc(assignedCareProvider.careProviderId)
          .update(<String, dynamic>{
        'contacts': FieldValue.arrayUnion(<String>[client.uid]),
        'assignedPatients.${assignedPatient.patientId}':
            assignedPatient.toJson(),
      }).timeout(kTimeOut);

      await userCollection.doc(client.uid).update(<String, dynamic>{
        'contacts': FieldValue.arrayUnion(
            <String>[assignedCareProvider.careProviderId]),
      }).timeout(kTimeOut);
      analytics.logEvent(name: kProviderAssignedEvt);
    } on Exception {
      throw UpdateDataException(kOperationFailed);
    }
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/constants/constants.dart';
import '../../references.dart';
import 'notification_repository.dart';

class NotificationService implements NotificationRepository {
  @override
  Stream<List<PushNotification>> fetchNotifications(String uid) {
    try {
      final Stream<QuerySnapshot<Map<String, dynamic>>> query = userCollection
          .doc(uid)
          .collection(kNotifications)
          .orderBy(kCreatedAt, descending: true)
          .snapshots();

      return query.map((QuerySnapshot<Map<String, dynamic>> snapshot) =>
          snapshot.docs
              .map((QueryDocumentSnapshot<Map<String, dynamic>> doc) =>
                  PushNotification.fromMap(doc.data()))
              .toList());
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Future<void> sendPushNotification(PushNotification notification) async {
    final WriteBatch batch = FirebaseFirestore.instance.batch();

    try {
      for (final String recipient in notification.recipients) {
        final DocumentReference<Map<String, dynamic>> doc = userCollection
            .doc(recipient)
            .collection(kNotifications)
            .doc(notification.id);

        batch.set(doc, notification.toMap());
      }

      await batch.commit().timeout(kTimeOut);
    } on Exception {
      throw UpdateDataException(kOperationFailed);
    }
  }

  @override
  Future<void> deleteNotification(
      {required String uid, required String notificationId}) async {
    try {
      await userCollection
          .doc(uid)
          .collection(kNotifications)
          .doc(notificationId)
          .delete()
          .timeout(kTimeOut);
    } on Exception {
      throw UpdateDataException('Failed to delete notification');
    }
  }

  @override
  Future<void> deleteAllNotifications(
      {required String uid, required List<String> notificationIds}) async {
    final WriteBatch batch = FirebaseFirestore.instance.batch();

    try {
      for (final String id in notificationIds) {
        final DocumentReference<Map<String, dynamic>> doc =
            userCollection.doc(uid).collection(kNotifications).doc(id);

        batch.delete(doc);
      }

      await batch.commit().timeout(kTimeOut);
    } on Exception {
      throw UpdateDataException(kOperationFailed);
    }
  }

  @override
  void markNotificationAsSeen(
      {required String uid, required String notificationId}) {
    userCollection
        .doc(uid)
        .collection(kNotifications)
        .doc(notificationId)
        .update(<String, dynamic>{'isSeen': true});
  }
}

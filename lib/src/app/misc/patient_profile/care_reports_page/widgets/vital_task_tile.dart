import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class VitalTaskTile extends StatelessWidget {
  final String vital;
  final String recording;
  final DateTime? takenAt;
  const VitalTaskTile({
    super.key,
    this.takenAt,
    required this.vital,
    required this.recording,
  });

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return ListTile(
      contentPadding: EdgeInsets.symmetric(horizontal: 2.0.w),
      title: Text(
        vital,
        style: theme.titleMedium
            ?.copyWith(fontWeight: FontWeight.w600, fontSize: 17.68.sp),
      ),
      subtitle: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(recording, style: theme.titleSmall),
          Text(_getTime(), style: theme.titleSmall),
        ],
      ),
    );
  }

  String _getTime() {
    if (takenAt != null) {
      return DateFormat.jm().format(takenAt!);
    } else {
      return '';
    }
  }
}

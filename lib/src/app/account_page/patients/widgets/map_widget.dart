import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../blocs/location/location_bloc.dart';

class MapWidget extends StatelessWidget {
  final LatLng? initialLocation;
  final Function(LatLng) onLocationChanged;
  const MapWidget({
    super.key,
    required this.onLocationChanged,
    this.initialLocation,
  });

  @override
  Widget build(BuildContext context) {
    final LatLng location =
        context.select((LocationBloc cubit) => cubit.state.location);
    return _MapWidgetContent(
      location: initialLocation ?? location,
      onLocationChanged: onLocationChanged,
    );
  }
}

class _MapWidgetContent extends StatefulWidget {
  final LatLng location;
  final Function(LatLng) onLocationChanged;
  const _MapWidgetContent({
    Key? key,
    required this.location,
    required this.onLocationChanged,
  }) : super(key: key);

  @override
  _MapWidgetContentState createState() => _MapWidgetContentState();
}

class _MapWidgetContentState extends State<_MapWidgetContent>
    with AutomaticKeepAliveClientMixin {
  late LatLng _location;
  final double _zoom = 15.4746;
  MapType _mapType = MapType.normal;
  late GoogleMapController _controller;

  @override
  void initState() {
    super.initState();
    _location = widget.location;
  }

  @override
  void didUpdateWidget(_MapWidgetContent oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.location != widget.location) {
      _location = widget.location;
      widget.onLocationChanged(_location);
      _controller.animateCamera(
        CameraUpdate.newLatLngZoom(_location, 15.8),
      );
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller = controller;
  }

  void _onMapTypeChanged() {
    setState(() {
      _mapType = _mapType == MapType.normal ? MapType.hybrid : MapType.normal;
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 1.0.w, vertical: 0.8.h),
      child: Stack(
        children: <Widget>[
          SizedBox(
            height: 40.0.h,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12.0),
              child: GoogleMap(
                mapType: _mapType,
                onTap: (LatLng location) {
                  _location = location;
                  widget.onLocationChanged(location);
                  setState(() {});
                },
                markers: <Marker>{
                  Marker(
                    draggable: true,
                    onDragEnd: (LatLng location) {
                      _location = location;
                      widget.onLocationChanged(location);
                    },
                    markerId: const MarkerId('marker'),
                    position: _location,
                  ),
                },
                compassEnabled: false,
                mapToolbarEnabled: false,
                zoomControlsEnabled: false,
                initialCameraPosition: CameraPosition(
                  zoom: _zoom,
                  target: _location,
                ),
                onMapCreated: _onMapCreated,
                gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>{
                  Factory<OneSequenceGestureRecognizer>(
                    () => EagerGestureRecognizer(),
                  ),
                },
              ),
            ),
          ),
          Positioned(
            top: 3.0.h,
            right: 0.5.w,
            child: Padding(
              padding: EdgeInsets.all(
                1.5.h,
              ),
              child: Align(
                alignment: Alignment.bottomRight,
                child: FloatingActionButton(
                  mini: true,
                  elevation: 1.0,
                  onPressed: _onMapTypeChanged,
                  backgroundColor: Colors.grey.shade100,
                  child: Icon(
                    Icons.map_outlined,
                    size: 3.8.h,
                    color: Colors.black.withOpacity(0.6),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

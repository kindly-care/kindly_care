import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:uuid/uuid.dart';

import '../../../../../blocs/care_task/care_task_bloc.dart';
import '../../../../../common/constants/constants.dart';
import '../../../../../data/services/care_task/care_task_service.dart';

class CreateMedicationTaskPage extends StatelessWidget {
  final String patientId;
  final MedicationTask? task;

  const CreateMedicationTaskPage(
      {super.key, this.task, required this.patientId});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CareTaskBloc>(
      create: (BuildContext context) => CareTaskBloc(
        careTaskRepository: context.read<CareTaskService>(),
      ),
      child: _CreateMedicationTaskPageView(task: task, patientId: patientId),
    );
  }
}

class _CreateMedicationTaskPageView extends StatefulWidget {
  final String patientId;
  final MedicationTask? task;

  const _CreateMedicationTaskPageView(
      {Key? key, this.task, required this.patientId})
      : super(key: key);

  @override
  _CreateMedicationTaskPageViewState createState() =>
      _CreateMedicationTaskPageViewState();
}

class _CreateMedicationTaskPageViewState
    extends State<_CreateMedicationTaskPageView> {
  DateTime? _startDate;
  DateTime? _endDate;
  late GlobalKey<FormState> _formKey;
  late TextEditingController _nameController;
  late TextEditingController _dosageController;
  late TextEditingController _metricController;
  late TextEditingController _endDateController;
  late TextEditingController _frequencyController;
  late TextEditingController _startDateController;
  late TextEditingController _descriptionController;

  @override
  void initState() {
    super.initState();
    final MedicationTask? task = widget.task;

    _endDate = task?.endDate;
    _startDate = task?.startDate;
    _formKey = GlobalKey<FormState>();
    _nameController = TextEditingController(text: task?.title);
    _metricController = TextEditingController(text: task?.metric);
    _endDateController = TextEditingController(text: _initEndDate());
    _frequencyController = TextEditingController(text: task?.frequency);
    _startDateController = TextEditingController(text: _initStartDate());
    _descriptionController = TextEditingController(text: task?.description);
    _dosageController = TextEditingController(text: task?.dosage.toString());
  }

  String? _initStartDate() {
    final DateTime? startDate = widget.task?.startDate;
    if (startDate != null) {
      return DateFormat.yMMMMd('en_GB').format(startDate);
    } else {
      return null;
    }
  }

  String? _initEndDate() {
    final DateTime? endDate = widget.task?.endDate;
    if (endDate != null) {
      return DateFormat.yMMMMd('en_GB').format(endDate);
    } else {
      return null;
    }
  }

  Future<void> _onSaveButtonTap(AppUser user) async {
    final bool isValid = _formKey.currentState?.validate() ?? false;
    if (isValid) {
      final num dosage = num.tryParse(_dosageController.text) ?? 0;
      final MedicationTask task = MedicationTask(
        dosage: dosage,
        endDate: _endDate,
        startDate: _startDate,
        lastUpdatedBy: user.name,
        repeatDays: kLongWeekDays,
        lastUpdated: DateTime.now(),
        patientId: widget.patientId,
        type: CareTaskType.medication,
        metric: _metricController.text,
        frequency: _frequencyController.text,
        time: widget.task?.time ?? DateTime.now(),
        taskId: widget.task?.taskId ?? const Uuid().v4(),
        title: widget.task?.title ?? _nameController.text,
        description: widget.task?.description ?? _descriptionController.text,
      );

      context.read<CareTaskBloc>().add(CreateCareTask(task: task));
    }
  }

  void _onErrorState(AppUser user, String message) {
    showTwoButtonDialog(
      context,
      title: 'Error',
      content: message,
      buttonText1: 'Exit',
      buttonText2: 'Retry',
      onPressed1: () {
        Navigator.pop(context);
        Navigator.pop(context);
      },
      onPressed2: () {
        Navigator.pop(context);
        _onSaveButtonTap(user);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final bool isLoading = context.select((CareTaskBloc bloc) => bloc.state)
        is CreateCareTaskInProgress;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.task == null ? 'Add Medication' : 'Edit Medication'),
      ),
      body: LoadingOverlay(
        isLoading: isLoading,
        child: BlocListener<CareTaskBloc, CareTaskState>(
          listener: (BuildContext context, CareTaskState state) {
            if (state is CreateCareTaskError) {
              _onErrorState(user, state.error);
            } else if (state is CreateCareTaskSuccess) {
              Navigator.pop(context);
              successSnackbar(context, state.message);
            }
          },
          child: Form(
            key: _formKey,
            child: ListView(
              physics: const BouncingScrollPhysics(),
              padding: EdgeInsets.all(3.0.w),
              children: <Widget>[
                SizedBox(height: 1.0.h),
                MaterialTextField(
                  controller: _nameController,
                  labelText: 'Medication name',
                  hintText: 'Enter medication name',
                  onChanged: (_) => setState(() {}),
                  validator: (String? val) => val.simpleValidate,
                ),
                SizedBox(height: 2.5.h),
                Padding(
                  padding: EdgeInsets.only(left: 1.0.w),
                  child: Text(
                    'Dosage',
                    style:
                        theme.bodyMedium?.copyWith(fontWeight: FontWeight.w500),
                  ),
                ),
                SizedBox(height: 0.6.h),
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: MaterialTextField(
                        textInputType: const TextInputType.numberWithOptions(
                            decimal: true),
                        controller: _dosageController,
                        hintText: 'Enter dosage count',
                        onChanged: (_) => setState(() {}),
                        validator: (String? val) => val.simpleValidateNumber,
                      ),
                    ),
                    SizedBox(width: 2.0.w),
                    Expanded(
                      child: DropdownTextField(
                        controller: _metricController,
                        onChanged: (_) => setState(() {}),
                        items: const <String>['mg', 'ml'],
                        validator: (String? val) => val.simpleValidate,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 2.5.h),
                DropdownTextField(
                  labelText: 'Frequency',
                  controller: _frequencyController,
                  onChanged: (_) => setState(() {}),
                  validator: (String? val) => val.simpleValidate,
                  items: const <String>[
                    'Once Daily',
                    'Twice Daily',
                    'Thrice Daily',
                  ],
                ),
                SizedBox(height: 2.5.h),
                MaterialTextField(
                  readOnly: true,
                  isOptional: true,
                  enableInteractiveSelection: false,
                  focusNode: AlwaysDisabledFocusNode(),
                  onTap: () {
                    showDateTimeSelector(
                      context,
                      title: 'Start Date',
                      minDate: DateTime.now(),
                      initialDate: _startDate,
                      controller: _startDateController,
                      mode: CupertinoDatePickerMode.date,
                      format: DateFormat.yMMMMd('en_GB'),
                      onDateChanged: (DateTime date) {
                        _startDate = date;
                        _startDateController.text =
                            DateFormat.yMMMMd('en_GB').format(date);
                      },
                    );
                  },
                  labelText: 'Start Date',
                  hintText: 'Select date',
                  controller: _startDateController,
                  onChanged: (_) => setState(() {}),
                  prefixIcon: const Icon(Icons.event_outlined),
                ),
                SizedBox(height: 2.5.h),
                MaterialTextField(
                  readOnly: true,
                  isOptional: true,
                  enableInteractiveSelection: false,
                  focusNode: AlwaysDisabledFocusNode(),
                  onTap: () {
                    showDateTimeSelector(
                      context,
                      title: 'End Date',
                      minDate: DateTime.now(),
                      initialDate: _endDate,
                      controller: _endDateController,
                      mode: CupertinoDatePickerMode.date,
                      format: DateFormat.yMMMMd('en_GB'),
                      onDateChanged: (DateTime date) {
                        _endDate = date;
                        _endDateController.text =
                            DateFormat.yMMMMd('en_GB').format(date);
                      },
                    );
                  },
                  labelText: 'End Date',
                  hintText: 'Select date',
                  controller: _endDateController,
                  onChanged: (_) => setState(() {}),
                  prefixIcon: const Icon(Icons.event_outlined),
                ),
                SizedBox(height: 2.5.h),
                MaterialTextField(
                  minLines: 3,
                  maxLines: null,
                  isOptional: true,
                  labelText: 'Note',
                  hintText: 'Enter note',
                  onChanged: (_) => setState(() {}),
                  controller: _descriptionController,
                  textInputAction: TextInputAction.newline,
                ),
                SizedBox(height: 9.0.h),
                ActionButton(
                  title: 'Save Task',
                  onPressed: () => _onSaveButtonTap(user),
                  enabled: _isButtonEnabled(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool _isButtonEnabled() {
    if (_nameController.text.isNotEmpty &&
        _dosageController.text.isNotEmpty &&
        _metricController.text.isNotEmpty &&
        _frequencyController.text.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  @override
  void dispose() {
    _nameController.dispose();
    _dosageController.dispose();
    _metricController.dispose();
    _endDateController.dispose();
    _frequencyController.dispose();
    _startDateController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }
}

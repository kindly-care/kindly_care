import 'package:bloc_test/bloc_test.dart';
import 'package:casa_home/src/blocs/care_provider/care_provider_bloc.dart';
import 'package:casa_home/src/data/services/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:mocktail/mocktail.dart';

import '../test_data.dart';

class MockPatientRepository extends Mock implements PatientRepository {}

class MockNotificationRepository extends Mock
    implements NotificationRepository {}

class MockCareProviderRepository extends Mock
    implements CareProviderRepository {}

void main() {
  late PatientRepository patientRepository;
  late NotificationRepository notificationRepository;
  late CareProviderRepository careProviderRepository;

  setUp(() {
    patientRepository = MockPatientRepository();
    notificationRepository = MockNotificationRepository();
    careProviderRepository = MockCareProviderRepository();
  });

  group('CareProviderBloc', () {
    test('initial state is CareProviderInitial', () {
      final CareProviderBloc careProviderBloc = CareProviderBloc(
        patientRepository: patientRepository,
        notificationRepository: notificationRepository,
        careProviderRepository: careProviderRepository,
      );
      expect(careProviderBloc.state, CareProviderInitial());
      careProviderBloc.close();
    });

    group('AssignCareProvider', () {
      blocTest<CareProviderBloc, CareProviderState>(
        'emits [AssignCareProviderError] when [AssignCareProvider] is added and '
        'fetchCareProviderByEmail throws',
        setUp: () {
          when(() => careProviderRepository.fetchCareProviderByEmail(email))
              .thenThrow(FetchDataException(errorMessage));
        },
        build: () => CareProviderBloc(
          patientRepository: patientRepository,
          notificationRepository: notificationRepository,
          careProviderRepository: careProviderRepository,
        ),
        act: (CareProviderBloc bloc) => bloc.add(AssignCareProvider(
          shift: shift,
          email: email,
          client: user,
          patient: patient,
        )),
        expect: () => <Matcher>[
          isA<AssignCareProviderInProgress>(),
          isA<AssignCareProviderError>(),
        ],
      );

      blocTest<CareProviderBloc, CareProviderState>(
        'emits [AssignCareProviderError] when [AssignCareProvider] is added with an '
        'email that does not belong to any [CareProvider]',
        setUp: () {
          when(() => careProviderRepository.fetchCareProviderByEmail(email))
              .thenAnswer(
            (_) => Future<CareProvider?>.value(null),
          );
        },
        build: () => CareProviderBloc(
          patientRepository: patientRepository,
          notificationRepository: notificationRepository,
          careProviderRepository: careProviderRepository,
        ),
        act: (CareProviderBloc bloc) => bloc.add(AssignCareProvider(
          shift: shift,
          email: email,
          client: user,
          patient: patient,
        )),
        expect: () => <Matcher>[
          isA<AssignCareProviderInProgress>(),
          isA<AssignCareProviderError>(),
        ],
      );
    });

    group('DismissCareProvider', () {
      blocTest<CareProviderBloc, CareProviderState>(
        'emits [DismissCareProviderError] when [DismissCareProvider] is added and '
        'removeCareProvider throws',
        setUp: () {
          when(() => patientRepository.removeCareProvider(
                patient: patient,
                careProvider: careProvider,
              )).thenThrow(UpdateDataException(errorMessage));
        },
        build: () => CareProviderBloc(
          patientRepository: patientRepository,
          notificationRepository: notificationRepository,
          careProviderRepository: careProviderRepository,
        ),
        act: (CareProviderBloc bloc) => bloc.add(DismissCareProvider(
          patient: patient,
          careProvider: careProvider,
        )),
        expect: () => <Matcher>[
          isA<DismissCareProviderInProgress>(),
          isA<DismissCareProviderError>(),
        ],
      );

      blocTest<CareProviderBloc, CareProviderState>(
        'emits [DismissCareProviderSuccess] when [DismissCareProvider] is added and '
        'removeCareProvider is successfully called',
        setUp: () {
          when(() => patientRepository.removeCareProvider(
                patient: patient,
                careProvider: careProvider,
              )).thenAnswer((_) async {});
        },
        build: () => CareProviderBloc(
          patientRepository: patientRepository,
          notificationRepository: notificationRepository,
          careProviderRepository: careProviderRepository,
        ),
        act: (CareProviderBloc bloc) => bloc.add(DismissCareProvider(
          patient: patient,
          careProvider: careProvider,
        )),
        expect: () => <Matcher>[
          isA<DismissCareProviderInProgress>(),
          isA<DismissCareProviderSuccess>(),
        ],
      );
    });

    group('FetchAllCareProviders', () {
      blocTest<CareProviderBloc, CareProviderState>(
        'emits [CareProvidersLoadError] when [FetchAllCareProviders] is added and '
        'fetchAllCareProviders throws',
        setUp: () {
          when(() => careProviderRepository.fetchAllCareProviders())
              .thenThrow(FetchDataException(errorMessage));
        },
        build: () => CareProviderBloc(
          patientRepository: patientRepository,
          notificationRepository: notificationRepository,
          careProviderRepository: careProviderRepository,
        ),
        act: (CareProviderBloc bloc) => bloc.add(const FetchAllCareProviders()),
        expect: () => <Matcher>[
          isA<CareProvidersLoading>(),
          isA<CareProvidersLoadError>(),
        ],
      );

      blocTest<CareProviderBloc, CareProviderState>(
        'emits [CareProvidersLoadSuccess] when [FetchAllCareProviders] is added and '
        'fetchAllCareProviders is successfully called',
        setUp: () {
          when(() => careProviderRepository.fetchAllCareProviders())
              .thenAnswer((_) async => <CareProvider>[careProvider]);
        },
        build: () => CareProviderBloc(
          patientRepository: patientRepository,
          notificationRepository: notificationRepository,
          careProviderRepository: careProviderRepository,
        ),
        act: (CareProviderBloc bloc) => bloc.add(const FetchAllCareProviders()),
        expect: () => <Matcher>[
          isA<CareProvidersLoading>(),
          isA<CareProvidersLoadSuccess>(),
        ],
      );
    });

    group('FetchAssignedCareProviders', () {
      blocTest<CareProviderBloc, CareProviderState>(
        'emits [CareProvidersLoadError] when [FetchAssignedCareProviders] is added and '
        'fetchAssignedCareProviders throws',
        setUp: () {
          when(() => patientRepository.fetchAssignedCareProviders(<String>[]))
              .thenThrow(FetchDataException(errorMessage));
        },
        build: () => CareProviderBloc(
          patientRepository: patientRepository,
          notificationRepository: notificationRepository,
          careProviderRepository: careProviderRepository,
        ),
        act: (CareProviderBloc bloc) =>
            bloc.add(const FetchAssignedCareProviders(providerIds: <String>[])),
        expect: () => <Matcher>[
          isA<CareProvidersLoading>(),
          isA<CareProvidersLoadError>(),
        ],
      );

      blocTest<CareProviderBloc, CareProviderState>(
        'emits [CareProvidersLoadSuccess] when [FetchAssignedCareProviders] is added and '
        'fetchAssignedCareProviders is successfully called',
        setUp: () {
          when(() => patientRepository.fetchAssignedCareProviders(<String>[]))
              .thenAnswer((_) async => <CareProvider>[careProvider]);
        },
        build: () => CareProviderBloc(
          patientRepository: patientRepository,
          notificationRepository: notificationRepository,
          careProviderRepository: careProviderRepository,
        ),
        act: (CareProviderBloc bloc) =>
            bloc.add(const FetchAssignedCareProviders(providerIds: <String>[])),
        expect: () => <Matcher>[
          isA<CareProvidersLoading>(),
          isA<CareProvidersLoadSuccess>(),
        ],
      );
    });
  });
}

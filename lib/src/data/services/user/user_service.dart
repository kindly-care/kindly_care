import 'package:kindly_components/kindly_components.dart';

import '../../../common/constants/constants.dart';
import '../../references.dart';
import '../firebase_helper.dart';
import 'user_repository.dart';

class UserService implements UserRepository {
  @override
  Future<void> updateUser(AppUser user) async {
    String avatar = user.avatar;

    try {
      if (user.localAvatar != null) {
        avatar = await getDownloadURL(file: user.localAvatar!, pathId: user.uid)
            .timeout(kTimeOut);
      }

      await userCollection
          .doc(user.uid)
          .update(user.copyWith(avatar: avatar).toJson())
          .timeout(kTimeOut);
    } on Exception {
      throw UpdateDataException(kOperationFailed);
    }
  }
}

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../blocs/care_task/care_task_bloc.dart';
import '../../../../data/services/care_task/care_task_service.dart';

class CareTaskTab extends StatelessWidget {
  final Patient patient;
  const CareTaskTab({super.key, required this.patient});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CareTaskBloc>(
      create: (BuildContext context) => CareTaskBloc(
        careTaskRepository: context.read<CareTaskService>(),
      ),
      child: _CareTaskTabView(patient),
    );
  }
}

class _CareTaskTabView extends StatefulWidget {
  final Patient patient;
  const _CareTaskTabView(this.patient, {Key? key}) : super(key: key);

  @override
  CareTasksTabViewState createState() => CareTasksTabViewState();
}

class CareTasksTabViewState extends State<_CareTaskTabView>
    with AutomaticKeepAliveClientMixin {
  @override
  void initState() {
    super.initState();
    _loadCareTasks();
  }

  void _loadCareTasks() {
    context
        .read<CareTaskBloc>()
        .add(FetchCareTasks(patientId: widget.patient.uid));
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<CareTaskBloc, CareTaskState>(
      builder: (BuildContext context, CareTaskState state) {
        if (state is CareTasksLoadError) {
          return LoadErrorWidget(
            message: state.error,
            onRetry: _loadCareTasks,
          );
        } else if (state is CareTasksLoadSuccess) {
          return _PageContent(tasks: state.tasks, patient: widget.patient);
        } else {
          return const LoadingIndicator();
        }
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class _PageContent extends StatelessWidget {
  final Patient patient;
  final List<CareTask> tasks;
  const _PageContent({Key? key, required this.patient, required this.tasks})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (tasks.isEmpty) {
      return const NoDataWidget('No Care Tasks found.');
    } else {
      tasks.sortBy((CareTask task) => task.time);
      tasks
          .sort((CareTask a, CareTask b) => a.time.hour.compareTo(b.time.hour));
      return ListView.builder(
        shrinkWrap: true,
        itemCount: tasks.length,
        physics: const BouncingScrollPhysics(),
        padding: EdgeInsets.symmetric(horizontal: 2.0.w, vertical: 1.5.h),
        itemBuilder: (BuildContext context, int index) {
          final CareTask task = tasks[index];
          return Padding(
            padding: EdgeInsets.only(bottom: 1.5.h),
            child: CareTaskCard(task),
          );
        },
      );
    }
  }
}

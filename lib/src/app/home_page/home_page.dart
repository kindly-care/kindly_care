import 'package:animations/animations.dart';
import 'package:fcm_config/fcm_config.dart';
import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:stream_chat_flutter_core/stream_chat_flutter_core.dart';

import '../../common/common.dart';
import '../../common/constants/constants.dart';
import '../../cubits/cubits.dart';
import '../account_page/account_page.dart';
import '../messages/messages_page.dart';
import '../patients_overview/patients_overview_page.dart';
import '../settings_page/settings_page.dart';
import 'cubit/home_cubit.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    return BlocProvider<HomeCubit>(
      create: (BuildContext context) => HomeCubit(),
      child: _HomeView(user: user),
    );
  }
}

class _HomeView extends StatefulWidget {
  final AppUser user;
  const _HomeView({Key? key, required this.user}) : super(key: key);

  @override
  HomeViewState createState() => HomeViewState();
}

class HomeViewState extends State<_HomeView>
    with FCMNotificationClickMixin<_HomeView> {
  @override
  void initState() {
    super.initState();
    FCMConfig.instance.getInitialMessage().then(_onMessage);
  }

  void _onMessage(RemoteMessage? message) {
    final String? type = message?.data['type'] as String?;
    if (type == kNotification) {
      Navigator.pushNamed(context, kNotificationsPageRoute,
          arguments: widget.user.uid);
    } else if (type == kChat) {
      context.read<HomeCubit>().setTab(1);
    }
  }

  bool _showNotificationBadge() {
    final StreamChatClient client = ChatClient.client;
    final int unreadMessages = client.state.totalUnreadCount;
    if (unreadMessages != 0) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    final Color activeColor = Theme.of(context).primaryColor;
    final Color disabledColor = Theme.of(context).disabledColor;
    final int selectedTab =
        context.select((HomeCubit cubit) => cubit.state.tab);
    final List<Patient> patients =
        context.select((AccountCubit cubit) => cubit.state.patients);

    return Scaffold(
      body: PageTransitionSwitcher(
        transitionBuilder: (Widget child, Animation<double> animation,
            Animation<double> secondaryAnimation) {
          return FadeThroughTransition(
            animation: animation,
            secondaryAnimation: secondaryAnimation,
            child: child,
          );
        },
        duration: const Duration(milliseconds: 700),
        child: IndexedStack(
          index: selectedTab,
          children: <Widget>[
            PatientsOverviewPage(uid: widget.user.uid),
            const MessagesPage(),
            const AccountPage(),
            const SettingsPage(),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        onPressed: () => _showQuickActionsBottomSheet(patients),
        child: Icon(
          Icons.menu_outlined,
          size: 3.8.h,
          color: Colors.white,
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            IconButton(
              icon: Icon(
                selectedTab == 0 ? Icons.groups : Icons.groups_outlined,
              ),
              iconSize: 4.0.h,
              onPressed: () => context.read<HomeCubit>().setTab(0),
              color: selectedTab == 0 ? activeColor : disabledColor,
            ),
            Padding(
              padding: EdgeInsets.only(right: 15.0.w),
              child: IconButton(
                icon: Stack(
                  children: <Widget>[
                    Icon(
                      selectedTab == 1 ? Boxicons.bxs_chat : Boxicons.bx_chat,
                      size: 4.0.h,
                    ),
                    Visibility(
                      visible: _showNotificationBadge(),
                      child: Positioned(
                        top: 0.1.h,
                        right: 0.8.w,
                        child: Icon(Icons.brightness_1,
                            size: 1.5.h, color: Colors.redAccent),
                      ),
                    ),
                  ],
                ),
                onPressed: () => context.read<HomeCubit>().setTab(1),
                color: selectedTab == 1 ? activeColor : disabledColor,
              ),
            ),
            IconButton(
              icon:
                  Icon(selectedTab == 2 ? Boxicons.bxs_user : Boxicons.bx_user),
              iconSize: 4.0.h,
              onPressed: () => context.read<HomeCubit>().setTab(2),
              color: selectedTab == 2 ? activeColor : disabledColor,
            ),
            IconButton(
              icon: Icon(selectedTab == 3 ? Boxicons.bxs_cog : Boxicons.bx_cog),
              iconSize: 4.0.h,
              onPressed: () => context.read<HomeCubit>().setTab(3),
              color: selectedTab == 3 ? activeColor : disabledColor,
            ),
          ],
        ),
      ),
    );
  }

  void _showQuickActionsBottomSheet(List<Patient> patients) {
    optionsBottomSheet(
      height: 52.0.h,
      context: context,
      title: 'Care quick actions',
      options: <Widget>[
        IconListTile(
          onTap: () {
            Navigator.of(context).popAndPushNamed(kFindCarePageRoute);
          },
          title: 'Find Care',
          showTrailing: false,
          icon: Icons.person_search_outlined,
          subtitle: 'Find Care Providers',
        ),
        Divider(indent: 18.0.w),
        IconListTile(
          onTap: () {
            if (patients.length == 1) {
              Navigator.of(context).popAndPushNamed(kPatientRecordsPageRoute,
                  arguments: patients.first);
            } else {
              Navigator.popAndPushNamed(context, kPatientSelectPageRoute,
                  arguments: DestinationPage.records);
            }
          },
          showTrailing: false,
          title: 'Health Vitals',
          subtitle: 'View Health Vitals',
          icon: FeatherIcons.activity,
        ),
        Divider(indent: 18.w),
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            _onAddPatientTap();
          },
          title: 'Care Recipient',
          showTrailing: false,
          subtitle: 'Add Care Recipient',
          icon: Icons.person_add_alt,
        ),
      ],
    );
  }

  void _onAddPatientTap() {
    if (widget.user.address == null) {
      showOneButtonDialog(
        context,
        title: 'Account',
        content:
            'You need to finish setting up your account first before you can add Care Recipients.',
        buttonText: 'Finish Account Setup',
        onPressed: () {
          Navigator.popAndPushNamed(context, kEditUserProfilePageRoute);
        },
      );
    } else {
      Navigator.pushNamed(context, kAddPatientPageRoute);
    }
  }

  @override
  void onClick(RemoteMessage notification) {
    final String? type = notification.data['type'] as String?;
    if (type == kNotification) {
      Navigator.pushNamed(context, kNotificationsPageRoute,
          arguments: widget.user.uid);
    } else if (type == kChat) {
      context.read<HomeCubit>().setTab(1);
    }
  }
}

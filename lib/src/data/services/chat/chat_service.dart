import 'package:cloud_functions/cloud_functions.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/analytics/analytics.dart';
import '../../../common/constants/constants.dart';
import 'chat_repository.dart';

class ChatService implements ChatRepository {
  final FirebaseFunctions _functions = FirebaseFunctions.instance;

  @override
  Future<void> sendChatNotification({
    required String message,
    required String sender,
    required String recipientId,
  }) async {
    final HttpsCallable callable = _functions.httpsCallable(kChatNotification);
    await callable.call<void>(<String, dynamic>{
      'message': message,
      'sender': sender,
      'recipientId': recipientId,
    });
    analytics.logEvent(name: kChatMessageSentEvt);
  }

  @override
  Future<String> fetchStreamUserToken() async {
    try {
      final HttpsCallable callable = _functions.httpsCallable(kChatUserToken);
      final HttpsCallableResult<String> result = await callable.call<String>();
      return result.data;
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }
}

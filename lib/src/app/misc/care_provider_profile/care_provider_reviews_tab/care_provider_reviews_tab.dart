import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../common/common.dart';
import '../../../../data/services/services.dart';

import '../widgets/widgets.dart';
import 'bloc/review_bloc.dart';

class CareProviderReviewsTab extends StatelessWidget {
  final String careProviderId;
  const CareProviderReviewsTab({super.key, required this.careProviderId});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ReviewBloc>(
      create: (BuildContext context) => ReviewBloc(
        careProviderRepository: context.read<CareProviderService>(),
      )..add(FetchReviews(careProviderId: careProviderId)),
      child: _CareProviderReviewsTabView(careProviderId: careProviderId),
    );
  }
}

class _CareProviderReviewsTabView extends StatelessWidget {
  final String careProviderId;
  const _CareProviderReviewsTabView({Key? key, required this.careProviderId})
      : super(key: key);

  void _onLoadReviews(BuildContext context) {
    context
        .read<ReviewBloc>()
        .add(FetchReviews(careProviderId: careProviderId));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReviewBloc, ReviewState>(
      buildWhen: (ReviewState prev, ReviewState current) {
        return current is ReviewsLoading ||
            current is ReviewsLoadSuccess ||
            current is ReviewsLoadError;
      },
      builder: (BuildContext context, ReviewState state) {
        if (state is ReviewsLoadError) {
          return LoadErrorWidget(
            message: state.error,
            onRetry: () => _onLoadReviews(context),
          );
        } else if (state is ReviewsLoadSuccess) {
          return _PageContent(
            reviews: state.reviews,
            careProviderId: careProviderId,
          );
        } else {
          return const LoadingIndicator();
        }
      },
    );
  }
}

class _PageContent extends StatefulWidget {
  final String careProviderId;
  final List<Review> reviews;
  const _PageContent(
      {Key? key, required this.careProviderId, required this.reviews})
      : super(key: key);

  @override
  PageContentState createState() => PageContentState();
}

class PageContentState extends State<_PageContent>
    with AutomaticKeepAliveClientMixin {
  String _getMessage(AppUser user) {
    if (widget.careProviderId == user.uid) {
      return 'You do not have any reviews yet. Your reviews will be displayed here.';
    } else {
      return 'No reviews here yet.';
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);

    if (widget.reviews.isEmpty) {
      return NoDataWidget(_getMessage(user));
    } else {
      return ListView.builder(
        physics: const BouncingScrollPhysics(),
        padding: EdgeInsets.zero,
        itemCount: widget.reviews.length,
        itemBuilder: (BuildContext context, int index) {
          final Review review = widget.reviews[index];
          return ReviewCard(
            review: review,
            onTap: () => _onReviewTap(review, user),
          );
        },
      );
    }
  }

  void _onReviewTap(Review review, AppUser user) {
    optionsBottomSheet(
      title: review.name,
      height: 45.0.h,
      context: context,
      options: <Widget>[
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onMakeCall(review.phone);
          },
          title: 'Call',
          showTrailing: false,
          icon: FeatherIcons.phone,
        ),
        Divider(indent: 18.w),
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onCreateChannel(context, uid: user.uid, otherId: review.authorId);
          },
          title: 'Message',
          showTrailing: false,
          icon: FeatherIcons.messageSquare,
        ),
        Divider(indent: 18.w),
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onEmail(review.email ?? '');
          },
          title: 'Send Email',
          showTrailing: false,
          icon: FeatherIcons.mail,
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}

import 'package:google_maps_flutter/google_maps_flutter.dart';

// Meals
const String kLunch = 'lunch';
const String kSupper = 'supper';
const String kBreakfast = 'breakfast';

// Tasks
const String kMeal = 'meal';
const String kGeneral = 'general';
const String kMedication = 'medication';
const String kCareTasks = 'care_tasks';

// Vitals
const String kWeight = 'weight';
const String kTemperature = 'temperature';
const String kBloodGlucose = 'blood_glucose';
const String kBloodPressure = 'blood_pressure';

// Users
const String kUser = 'user';
const String kPatient = 'patient';
const String kCareProvider = 'careProvider';

// Care Shifts
const String kDay = 'Day';
const String kNight = 'Night';
const String k24Hrs = '24Hrs';

// Stream Chat
const String kChatUserToken = 'getStreamUserToken';
const String kChatNotification = 'notifications-sendChatNotification';

// Lists
final List<String> kGenderOptions = <String>['Female', 'Male'];

final List<String> kCareShifts = <String>[
  'Day',
  'Night',
  '24Hrs',
];

const List<String> kProductList = <String>[
  kProdOneMonth,
  kProdThreeMonths,
  kProdSixMonths,
];

final List<String> kMobilityOptions = <String>[
  'Cane',
  'Walker',
  'Wheelchair',
  'Independent',
];

final List<String> kMaritalStatus = <String>[
  'Married',
  'Divorced',
  'Widowed',
  'Separated',
];

final List<String> kConditions = <String>[
  'Arthritis',
  'Cancer',
  'Dementia',
  'Diabetes',
  'Hypertension',
];

final List<String> kShortWeekDays = <String>[
  'Sun',
  'Mon',
  'Tue',
  'Wed',
  'Thu',
  'Fri',
  'Sat'
];

final List<String> kLongWeekDays = <String>[
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday'
];

final List<String> kRelations = <String>[
  'Son',
  'Daughter',
  'Brother',
  'Sister',
  'Mother',
  'Father',
  'Son-in-law',
  'Daughter-in-law',
  'Grandson',
  'Granddaughter',
  'Family Doctor',
  'Family Friend',
  'Neighbor',
  'Cousin',
  'Other'
];

// Analytics Events
const String kSubmitReviewEvt = 'submit_review_evt';
const String kContactAddedEvt = 'contact_added_evt';
const String kCallInitiatedEvt = 'call_initiated_evt';
const String kEmailInitiatedEvt = 'email_initiated_evt';
const String kPatientCreatedEvt = 'patient_created_evt';
const String kPatientRemovedEvt = 'patient_removed_evt';
const String kContactRemovedEvt = 'contact_removed_evt';
const String kChatMessageSentEvt = 'chat_message_sent_evt';
const String kCareTaskCreatedEvt = 'care_task_created_evt';
const String kCircleMemberAddEvt = 'circle_member_added_evt';
const String kProviderAssignedEvt = 'care_provider_assigned_evt';
const String kSubscriptionUpdatedEvt = 'subscription_updated_evt';
const String kCircleMemberRemovedEvt = 'circle_member_removed_evt';
const String kCareProviderDismissedEvt = 'care_provider_dismissed_evt';

// Hive Keys
const String kAuthKey = 'auth_key';

// Subscriptions
const String kProdOneMonth = 'casa_1199_1month';
const String kProdThreeMonths = 'casa_3299_3months';
const String kProdSixMonths = 'casa_6599_6months';

// Routes
const String kHomePageRoute = 'home';
const String kAppName = 'Kindly Care';
const String kLoginPageRoute = 'login';
const String kAccountRoute = 'account';
const String kMessagesRoute = 'messages';
const String kWeightPageRoute = 'weight';
const String kSplashPageRoute = 'splash';
const String kChatPageRoute = 'chat-page';
const String kContactsPageRoute = 'contacts';
const String kSettingsPageRoute = 'settings';
const String kPatientsPageRoute = 'patients';
const String kTimeLogsPageRoute = 'timelogs';
const String kFindCarePageRoute = 'find-care';
const String kCareTasksPageRoute = 'care-tasks';
const String kOnboardingPageRoute = 'onboarding';
const String kAddPatientPageRoute = 'add-patient';
const String kPatientMapPageRoute = 'patient-map';
const String kTemperaturePageRoute = 'temperature';
const String kUserProfilePageRoute = 'user-profile';
const String kEditUserProfilePageRoute = 'edit-user';
const String kBloodGlucosePageRoute = 'blood-glucose';
const String kSubmitReviewPageRoute = 'submit-review';
const String kNotificationsPageRoute = 'notifications';
const String kReportDetailPageRoute = 'report-detail';
const String kSubscriptionsPageRoute = 'subscriptions';
const String kBloodPressurePageRoute = 'blood-pressure';
const String kCareProvidersPageRoute = 'care-providers';
const String kPatientSelectPageRoute = 'patient-select';
const String kCreateAccountPageRoute = 'create-account';
const String kResetPasswordPageRoute = 'reset-password';
const String kCreateMealTaskPageRoute = 'meal-care-task';
const String kPatientRecordsPageRoute = 'patient-records';
const String kPatientProfilePageRoute = 'patient-profile';
const String kPatientReportsPageRoute = 'patient-reports';
const String kAddCircleMemberPageRoute = 'add-circle-member';
const String kRecipientsOverviewRoute = 'recipients-overview';
const String kRequestInterviewPageRoute = 'request-interview';
const String kCreateGeneralTaskPageRoute = 'general-care-task';
const String kAssignCareProviderPageRoute = 'assign-care-provider';
const String kCreateMedicationTaskPageRoute = 'medication-care-task';
const String kCareProviderProfilePageRoute = 'care-provider-profile';

// Misc
const String kUID = 'uid';
const String kType = 'type';
const String kChat = 'chat';
const String kEmail = 'email';
const String kRating = 'rating';
const String kVitals = 'vitals';
const String kCircle = 'circle';

/// date format (year-month-date)
const String kYMD = 'yyyy-MM-dd';
const String kReviews = 'reviews';
const String kReports = 'reports';
const int kAnimationDuration = 400;
const String kContacts = 'contacts';
const String kNextKin = 'nextOfKin';
const String kUserType = 'userType';
const String kTimeLogs = 'time_logs';
const String kPatientId = 'patientId';
const String kCreatedAt = 'createdAt';
const String kLastUpdated = 'lastUpdated';
const String kDateCreated = 'dateCreated';
const String kAuthDetails = 'auth_details';
const String kNotification = 'notification';
const String kNotifications = 'notifications';
const String kCircleMembers = 'circleMembers';
const String kIsAppFirstRun = 'isAppFirstRun';
const String kCareProviders = 'Care Providers';
const String kCareProviderId = 'careProviderId';
const Duration kTimeOut = Duration(seconds: 12);
const String kCareRecipients = 'Care Recipients';
const String kSubscriptionEnds = 'subscriptionEnds';
const LatLng kDefaultLocation = LatLng(-17.824858, 31.053028);
const String kOperationFailed = 'Operation failed';
const String kFailedToLoadData = 'Failed to load data';
const String kSomethingWentWrong = 'Something went wrong';

const String kStreamChatKey = 'stream_chat_api';
const String kWiredashSecret = 'wiredash_secret';
const String kWiredashProjectId = 'wiredash_projectId';

const String kAbout =
    'Made with extreme love and care by the good people over at PrimeLabs.\n\nAll rights reserved.';

// Contact
const String kContactPhone = '+263773018727';
const String kContactEmail = 'primelabs@tutamail.com';

// Links
const String kAppURL =
    'https://play.google.com/store/apps/details?id=com.primelabs.casa_home';

const String kDefaultAvatar =
    'https://firebasestorage.googleapis.com/v0/b/casa-333c5.appspot.com/o/app%2Fdefault-avatar.png?alt=media&token=893bccf7-6fe6-430f-b132-e352baae1748';

import 'package:kindly_components/kindly_components.dart';

abstract class CircleRepository {
  /// add circle member to a [Patient]. Throws [UpdateDataException].
  Future<void> addCircleMember(CircleMemberData data);

  /// removes circle member from a [Patient]. Throws [UpdateDataException].
  Future<void> removeCircleMember(
      {required String patientId, required String memberId});

  /// fetches [Patient]s circle. Throws [FetchDataException].
  Stream<List<AppUser>> fetchPatientCircle(List<String> ids);
}

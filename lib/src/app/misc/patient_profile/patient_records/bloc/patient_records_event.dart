part of 'patient_records_bloc.dart';

abstract class PatientRecordsEvent extends Equatable {
  const PatientRecordsEvent();

  @override
  List<Object> get props => <Object>[];
}

class FetchWeightRecords extends PatientRecordsEvent {
  final int month;
  final String patientId;
  const FetchWeightRecords({required this.month, required this.patientId});

  @override
  List<Object> get props => <Object>[month, patientId];
}

class FetchTemperatureRecords extends PatientRecordsEvent {
  final int month;
  final String patientId;
  const FetchTemperatureRecords({required this.month, required this.patientId});

  @override
  List<Object> get props => <Object>[month, patientId];
}

class FetchBloodPressureRecords extends PatientRecordsEvent {
  final int month;
  final String patientId;
  const FetchBloodPressureRecords(
      {required this.month, required this.patientId});

  @override
  List<Object> get props => <Object>[month, patientId];
}

class FetchBloodGlucoseRecords extends PatientRecordsEvent {
  final int month;
  final String patientId;
  const FetchBloodGlucoseRecords({required this.month, required this.patientId});

  @override
  List<Object> get props => <Object>[month, patientId];
}

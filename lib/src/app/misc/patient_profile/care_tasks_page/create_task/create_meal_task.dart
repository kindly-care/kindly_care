import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:uuid/uuid.dart';

import '../../../../../blocs/care_task/care_task_bloc.dart';
import '../../../../../common/constants/constants.dart';
import '../../../../../data/services/care_task/care_task_service.dart';

class CreateMealTaskPage extends StatelessWidget {
  final MealTask? task;
  final String patientId;

  const CreateMealTaskPage({super.key, this.task, required this.patientId});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CareTaskBloc>(
      create: (BuildContext context) => CareTaskBloc(
        careTaskRepository: context.read<CareTaskService>(),
      ),
      child: _CreateMealTaskPageView(task: task, patientId: patientId),
    );
  }
}

class _CreateMealTaskPageView extends StatefulWidget {
  final MealTask? task;
  final String patientId;

  const _CreateMealTaskPageView({
    Key? key,
    this.task,
    required this.patientId,
  }) : super(key: key);

  @override
  _CreateMealTaskPageViewState createState() => _CreateMealTaskPageViewState();
}

class _CreateMealTaskPageViewState extends State<_CreateMealTaskPageView> {
  late DateTime _time;
  late GlobalKey<FormState> _formKey;
  late TextEditingController _timeController;
  late TextEditingController _titleController;
  late TextEditingController _descriptionController;

  @override
  void initState() {
    super.initState();
    _formKey = GlobalKey<FormState>();
    final MealTask? task = widget.task;
    _time = task?.time ?? DateTime.now();
    _timeController = TextEditingController(
      text: task != null ? DateFormat.jm().format(task.time) : null,
    );
    _titleController = TextEditingController(text: task?.title);
    _descriptionController = TextEditingController(text: task?.description);
  }

  Future<void> _onSaveButtonTap(AppUser user) async {
    final bool isValid = _formKey.currentState?.validate() ?? false;

    if (isValid) {
      final MealTask task = MealTask(
        time: _time,
        lastUpdatedBy: user.name,
        repeatDays: kLongWeekDays,
        lastUpdated: DateTime.now(),
        patientId: widget.patientId,
        mealType: _titleController.text,
        taskId: widget.task?.taskId ?? const Uuid().v4(),
        title: widget.task?.title ?? _titleController.text,
        description: widget.task?.description ?? _descriptionController.text,
      );

      context.read<CareTaskBloc>().add(CreateCareTask(task: task));
    }
  }

  void _onErrorState(AppUser user, String message) {
    showTwoButtonDialog(
      context,
      title: 'Error',
      content: message,
      buttonText1: 'Exit',
      buttonText2: 'Retry',
      onPressed1: () {
        Navigator.pop(context);
        Navigator.pop(context);
      },
      onPressed2: () {
        Navigator.pop(context);
        _onSaveButtonTap(user);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final bool isLoading = context.select((CareTaskBloc bloc) => bloc.state)
        is CreateCareTaskInProgress;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.task == null ? 'Add Meal Task' : 'Edit Meal Task'),
      ),
      body: LoadingOverlay(
        isLoading: isLoading,
        child: BlocListener<CareTaskBloc, CareTaskState>(
          listener: (BuildContext context, CareTaskState state) {
            if (state is CreateCareTaskError) {
              _onErrorState(user, state.error);
            } else if (state is CreateCareTaskSuccess) {
              Navigator.pop(context);
              successSnackbar(context, state.message);
            }
          },
          child: Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(3.0.w),
              physics: const BouncingScrollPhysics(),
              children: <Widget>[
                SizedBox(height: 1.0.h),
                DropdownTextField(
                  labelText: 'Meal',
                  hintText: 'Select meal type',
                  controller: _titleController,
                  onChanged: (_) => setState(() {}),
                  validator: (String? val) => val.simpleValidate,
                  items: const <String>[
                    'Breakfast',
                    'Lunch',
                    'Supper',
                  ],
                ),
                SizedBox(height: 2.5.h),
                MaterialTextField(
                  readOnly: true,
                  enableInteractiveSelection: false,
                  focusNode: AlwaysDisabledFocusNode(),
                  onTap: () {
                    showDateTimeSelector(
                      context,
                      title: 'Meal Time',
                      initialDate: _time,
                      controller: _timeController,
                      format: DateFormat.jm('en_GB'),
                      mode: CupertinoDatePickerMode.time,
                      onDateChanged: (DateTime val) {
                        setState(() {});
                        _time = val;
                        _timeController.text =
                            DateFormat.jm('en_GB').format(val);
                      },
                    );
                  },
                  labelText: 'Time',
                  hintText: 'Select meal time',
                  controller: _timeController,
                  onChanged: (_) => setState(() {}),
                  validator: (String? val) => val.simpleValidate,
                  prefixIcon: const Icon(Icons.access_time_outlined),
                ),
                SizedBox(height: 3.0.h),
                MaterialTextField(
                  minLines: 3,
                  maxLines: null,
                  isOptional: true,
                  labelText: 'Description',
                  hintText: 'Enter description',
                  onChanged: (_) => setState(() {}),
                  controller: _descriptionController,
                  textInputAction: TextInputAction.newline,
                ),
                SizedBox(height: 21.0.h),
                ActionButton(
                  title: 'Save Task',
                  onPressed: () => _onSaveButtonTap(user),
                  enabled: _isButtonEnabled(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool _isButtonEnabled() {
    if (_timeController.text.isNotEmpty && _titleController.text.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  @override
  void dispose() {
    _timeController.dispose();
    _titleController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }
}

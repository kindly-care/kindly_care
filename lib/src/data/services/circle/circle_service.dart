import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/common.dart';
import '../../../common/constants/constants.dart';
import '../../references.dart';
import 'circle_repository.dart';

class CircleService implements CircleRepository {
  @override
  Future<void> addCircleMember(CircleMemberData data) async {
    try {
      await userCollection.doc(data.patientId).update(<String, dynamic>{
        '$kCircle.${data.memberId}': data.relation,
        kCircleMembers: FieldValue.arrayUnion(<String>[data.memberId!]),
      }).timeout(kTimeOut);

      if (data.memberId != null) {
        await userCollection.doc(data.uid).update(<String, dynamic>{
          kContacts: FieldValue.arrayUnion(<String>[data.memberId!]),
        }).timeout(kTimeOut);
      }

      if (data.isNextOfKin) {
        await userCollection.doc(data.patientId).update(<String, dynamic>{
          kNextKin: FieldValue.arrayUnion(<String>[data.memberId!]),
        }).timeout(kTimeOut);
      }
      analytics.logEvent(name: kCircleMemberAddEvt);
    } on Exception {
      throw UpdateDataException(kOperationFailed);
    }
  }

  @override
  Future<void> removeCircleMember(
      {required String patientId, required String memberId}) async {
    try {
      await userCollection.doc(patientId).update(<String, dynamic>{
        '$kCircle.$memberId': FieldValue.delete(),
        kCircleMembers: FieldValue.arrayRemove(<String>[memberId]),
      }).timeout(kTimeOut);

      analytics.logEvent(name: kCircleMemberRemovedEvt);
    } on Exception {
      throw UpdateDataException(kOperationFailed);
    }
  }

  @override
  Stream<List<AppUser>> fetchPatientCircle(List<String> ids) {
    try {
      if (ids.isEmpty) {
        ids.add('placeholder');
      }

      final Stream<QuerySnapshot<Map<String, dynamic>>> query =
          userCollection.where('uid', whereIn: ids).snapshots();

      return query.map(
        (QuerySnapshot<Map<String, dynamic>> snapshot) => snapshot.docs
            .map((QueryDocumentSnapshot<Map<String, dynamic>> doc) =>
                AppUser.fromJson(doc.data()))
            .toList(),
      );
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }
}

part of 'subscription_bloc.dart';

enum PurchasesErrorType { network, serviceUnavailable }

abstract class SubscriptionState extends Equatable {
  const SubscriptionState();

  @override
  List<Object> get props => <Object>[];
}

class SubscriptionInitial extends SubscriptionState {
  const SubscriptionInitial();
}

class PurchasesLoading extends SubscriptionState {
  const PurchasesLoading();
}

class PurchasesLoadSuccess extends SubscriptionState {
  final Patient patient;
  final List<Product> products;
  const PurchasesLoadSuccess({required this.patient, required this.products});

  @override
  List<Object> get props => <Object>[patient, products];
}

class PurchasesLoadError extends SubscriptionState {
  final String message;
  final PurchasesErrorType error;

  const PurchasesLoadError({required this.message, required this.error});

  @override
  List<Object> get props => <Object>[message, error];
}

import 'package:kindly_components/kindly_components.dart';

abstract class UserRepository {
  /// updates [AppUser]. Throws [UpdateDataException].
  Future<void> updateUser(AppUser user);
}

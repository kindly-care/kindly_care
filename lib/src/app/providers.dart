import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';

import '../blocs/blocs.dart';
import '../cubits/chats_cubit/chat_cubit.dart';
import '../cubits/cubits.dart';
import '../data/local/local.dart';
import '../data/services/services.dart';

class Providers extends StatelessWidget {
  final Widget child;
  const Providers({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: <RepositoryProvider<dynamic>>[
        RepositoryProvider<AuthService>(
          create: (BuildContext context) => AuthService(),
        ),
        RepositoryProvider<PatientService>(
          create: (BuildContext context) => PatientService(),
        ),
        RepositoryProvider<LocationService>(
          create: (BuildContext context) => LocationService(),
        ),
        RepositoryProvider<NotificationService>(
          create: (BuildContext context) => NotificationService(),
        ),
        RepositoryProvider<ContactsService>(
          create: (BuildContext context) => ContactsService(),
        ),
        RepositoryProvider<CareProviderService>(
          create: (BuildContext context) => CareProviderService(),
        ),
        RepositoryProvider<UserService>(
          create: (BuildContext context) => UserService(),
        ),
        RepositoryProvider<ChatService>(
          create: (BuildContext context) => ChatService(),
        ),
        RepositoryProvider<SubscriptionService>(
          create: (BuildContext context) => SubscriptionService(),
        ),
        RepositoryProvider<ReportService>(
          create: (BuildContext context) => ReportService(),
        ),
        RepositoryProvider<CircleService>(
          create: (BuildContext context) => CircleService(),
        ),
        RepositoryProvider<CareTaskService>(
          create: (BuildContext context) => CareTaskService(),
        ),
        RepositoryProvider<VitalsService>(
          create: (BuildContext context) => VitalsService(),
        ),
      ],
      child: MultiBlocProvider(
        providers: <BlocProvider<dynamic>>[
          BlocProvider<AuthBloc>(
            create: (BuildContext context) => AuthBloc(
              authRepository: context.read<AuthService>(),
            )..add(StartApp()),
          ),
          BlocProvider<NotificationBloc>(
            create: (BuildContext context) => NotificationBloc(
              notificationRepository: context.read<NotificationService>(),
            ),
          ),
          BlocProvider<ChatCubit>(
            create: (BuildContext context) => ChatCubit(
              chatRepository: context.read<ChatService>(),
            ),
          ),
          BlocProvider<AccountCubit>(
            create: (BuildContext context) => AccountCubit(
              authRepository: context.read<AuthService>(),
              patientRepository: context.read<PatientService>(),
              agentRepository: context.read<CareProviderService>(),
            )..fetchAccountData(),
          ),
        ],
        child: child,
      ),
    );
  }
}

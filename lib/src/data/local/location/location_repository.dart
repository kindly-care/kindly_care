import 'package:google_maps_flutter/google_maps_flutter.dart';

abstract class LocationRepository {
  /// get user's current position [LatLng].
  Future<LatLng> getCurrentPosition();

  /// get user's position [LatLng] from area [String].
  Future<LatLng> getPositionFromArea(String area);

  /// get distant in km from current position.
  Future<double> getDistanceFromCurrentPosition(LatLng destination);
}

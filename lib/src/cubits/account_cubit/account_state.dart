part of 'account_cubit.dart';

enum AccountStatus { loading, success, error }

class AccountState extends Equatable {
  final String message;
  final AccountStatus status;
  final List<Patient> patients;
  final List<CareProvider> careProviders;

  const AccountState({
    this.message = '',
    this.patients = const <Patient>[],
    this.status = AccountStatus.loading,
    this.careProviders = const <CareProvider>[],
  });

  @override
  List<Object> get props => <Object>[
        status,
        message,
        patients,
        careProviders,
      ];
}

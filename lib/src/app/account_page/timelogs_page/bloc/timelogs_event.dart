part of 'timelogs_bloc.dart';

abstract class TimelogsEvent extends Equatable {
  const TimelogsEvent();

  @override
  List<Object> get props => <Object>[];
}

class FetchTimeLogs extends TimelogsEvent {
  final int month;
  final String patientId;
  final String careProviderId;

  const FetchTimeLogs({
    required this.month,
    required this.patientId,
    required this.careProviderId,
  });

  @override
  List<Object> get props => <Object>[month, careProviderId, patientId];
}

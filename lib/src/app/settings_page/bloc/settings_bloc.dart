import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../data/services/services.dart';

part 'settings_event.dart';
part 'settings_state.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  final AuthRepository _authRepository;
  final PatientRepository _patientRepository;
  SettingsBloc({
    required AuthRepository authRepository,
    required PatientRepository patientRepository,
  })  : _authRepository = authRepository,
        _patientRepository = patientRepository,
        super(SettingsInitial()) {
    on<DeleteAccountData>(_onDeleteAccountData);
  }

  Future<void> _onDeleteAccountData(
      DeleteAccountData event, Emitter<SettingsState> emit) async {
    emit(DeleteAccountInProgress());

    try {
      await _patientRepository.deletePatients(event.patientIds);
      await _authRepository.deleteAccount();
      emit(const DeleteAccountSuccess(message: 'Account deleted.'));
    } on UpdateDataException catch (e) {
      emit(DeleteAccountError(error: e.toString()));
    }
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:collection/collection.dart';
import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../blocs/circle_member/circle_member_bloc.dart';
import '../../../../common/common.dart';
import '../../../../data/services/services.dart';

class PatientCircleTab extends StatelessWidget {
  final Patient patient;
  final List<String> members;

  const PatientCircleTab(
      {super.key, required this.patient, required this.members});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CircleMemberBloc>(
      create: (BuildContext context) {
        return CircleMemberBloc(
          authRepository: context.read<AuthService>(),
          circleRepository: context.read<CircleService>(),
        )..add(FetchCircleMembers(memberIds: members));
      },
      child: _PatientCircleTabView(patient, members),
    );
  }
}

class _PatientCircleTabView extends StatefulWidget {
  final Patient patient;
  final List<String> members;
  const _PatientCircleTabView(this.patient, this.members, {Key? key})
      : super(key: key);

  @override
  _PatientCircleTabState createState() => _PatientCircleTabState();
}

class _PatientCircleTabState extends State<_PatientCircleTabView>
    with AutomaticKeepAliveClientMixin {
  @override
  void didUpdateWidget(_PatientCircleTabView oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (widget.members.length != oldWidget.members.length) {
      _onLoadCircleMembers();
    }
  }

  void _onLoadCircleMembers() {
    final List<String> members = widget.patient.circle.keys.toList();
    context
        .read<CircleMemberBloc>()
        .add(FetchCircleMembers(memberIds: members));
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocConsumer<CircleMemberBloc, CircleMemberState>(
      listener: (BuildContext context, CircleMemberState state) {
        if (state is CircleMemberRemoveSuccess) {
          successSnackbar(context, state.message);
        } else if (state is CircleMemberRemoveError) {
          errorSnackbar(context, state.error);
        }
      },
      buildWhen: (CircleMemberState prev, CircleMemberState current) {
        return current is CircleMembersLoading ||
            current is CircleMembersLoadSuccess ||
            current is CircleMemberRemoveSuccess;
      },
      builder: (BuildContext context, CircleMemberState state) {
        if (state is CircleMembersLoadError) {
          return LoadErrorWidget(
              message: state.error, onRetry: _onLoadCircleMembers);
        } else if (state is CircleMembersLoadSuccess) {
          return _PageContent(
              patient: widget.patient, members: state.circleMembers);
        } else {
          return const LoadingIndicator();
        }
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class _PageContent extends StatelessWidget {
  final Patient patient;
  final List<AppUser> members;
  const _PageContent({Key? key, required this.patient, required this.members})
      : super(key: key);

  String _getRelation(AppUser member) {
    final String relation = patient.circle.entries
            .firstWhereOrNull(
                (MapEntry<String, String> entry) => entry.key == member.uid)
            ?.value ??
        'Unknown Relation';

    return relation;
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);

    if (members.isEmpty) {
      return Padding(
        padding: EdgeInsets.only(top: 18.0.h),
        child: const NoDataWidget('No circle members found.'),
      );
    } else {
      return ListView.separated(
        shrinkWrap: true,
        itemCount: members.length,
        physics: const BouncingScrollPhysics(),
        padding: EdgeInsets.symmetric(horizontal: 1.0.w, vertical: 1.8.h),
        separatorBuilder: (_, __) => Divider(indent: 2.0.w, endIndent: 2.0.w),
        itemBuilder: (BuildContext context, int index) {
          final AppUser member = members[index];
          return ListTile(
            onTap: () {
              if (member.uid != user.uid) {
                _onMemberTap(context, member, user);
              }
            },
            leading: CircleAvatar(
              radius: 3.6.h,
              backgroundImage: CachedNetworkImageProvider(member.avatar),
            ),
            title: Text(
              member.name,
              overflow: TextOverflow.ellipsis,
              style: theme.titleMedium,
            ),
            subtitle: Text(
              _getRelation(member),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: theme.titleSmall?.copyWith(
                fontWeight: FontWeight.w500,
                color: Colors.black.withOpacity(0.5),
              ),
            ),
            trailing: Visibility(
              visible: member.uid != user.uid,
              child: IconButton(
                onPressed: () => onMakeCall(member.phone),
                icon: const Icon(Icons.call_outlined),
              ),
            ),
          );
        },
      );
    }
  }

  void _onMemberTap(BuildContext context, AppUser member, AppUser user) {
    final bool visible = patient.nextOfKin.contains(user.uid);
    optionsBottomSheet(
      context: context,
      title: member.name,
      height: visible ? 45.0.h : 32.0.h,
      options: <Widget>[
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onMakeCall(member.phone);
          },
          title: 'Call',
          showTrailing: false,
          icon: FeatherIcons.phone,
        ),
        Divider(indent: 18.w),
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onCreateChannel(context, uid: user.uid, otherId: member.uid);
          },
          title: 'Message',
          showTrailing: false,
          icon: FeatherIcons.messageSquare,
        ),
        Divider(indent: 18.w),
        Visibility(
          visible: visible,
          child: IconListTile(
            onTap: () {
              Navigator.pop(context);
              _showRemoveMemberConfirmationDialog(context, member);
            },
            title: 'Remove Circle Member',
            showTrailing: false,
            icon: FeatherIcons.userMinus,
          ),
        ),
      ],
    );
  }

  void _showRemoveMemberConfirmationDialog(
      BuildContext context, AppUser member) {
    showTwoButtonDialog(
      context,
      title: 'Confirm Action',
      isDestructiveAction: true,
      content:
          'Are you sure you want to remove ${member.name.firstName()} as a Circle Member?',
      buttonText1: 'Cancel',
      buttonText2: 'Remove',
      onPressed1: () => Navigator.pop(context),
      onPressed2: () {
        Navigator.pop(context);
        context.read<CircleMemberBloc>().add(
            RemoveCircleMember(memberId: member.uid, patientId: patient.uid));
      },
    );
  }
}

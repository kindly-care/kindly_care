part of 'care_task_bloc.dart';

abstract class CareTaskState extends Equatable {
  const CareTaskState();

  @override
  List<Object> get props => <Object>[];
}

class CareTaskInitial extends CareTaskState {}

class CareTasksLoading extends CareTaskState {
  const CareTasksLoading();
}

class CareTasksLoadSuccess extends CareTaskState {
  final List<CareTask> tasks;
  const CareTasksLoadSuccess({required this.tasks});

  @override
  List<Object> get props => <Object>[tasks];
}

class CareTasksLoadError extends CareTaskState {
  final String error;
  const CareTasksLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class CreateCareTaskInProgress extends CareTaskState {}

class CreateCareTaskSuccess extends CareTaskState {
  final String message;
  const CreateCareTaskSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class CreateCareTaskError extends CareTaskState {
  final String error;
  const CreateCareTaskError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class RemoveCareTaskInProgress extends CareTaskState {}

class RemoveCareTaskSuccess extends CareTaskState {
  final String message;
  const RemoveCareTaskSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class RemoveCareTaskError extends CareTaskState {
  final String error;
  const RemoveCareTaskError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

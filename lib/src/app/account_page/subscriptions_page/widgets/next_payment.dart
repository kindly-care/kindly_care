import 'package:dart_date/dart_date.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class NextPayment extends StatelessWidget {
  final DateTime subscriptionEnds;

  const NextPayment({super.key, required this.subscriptionEnds});

  String _getSubscriptionStatus() {
    if (subscriptionEnds.isPast) {
      return 'Expired';
    } else {
      final String expires =
          DateFormat.yMMMMd('en_GB').format(subscriptionEnds);
      return 'Expires: $expires';
    }
  }

  Color _getContainerColor() {
    final bool fiveDaysLeft = subscriptionEnds.isWithinRange(
        DateTime.now(), DateTime.now().addDays(5));
    if (fiveDaysLeft) {
      return Colors.deepOrange.shade100;
    } else if (subscriptionEnds.isPast) {
      return Colors.red.shade100;
    } else {
      return Colors.teal.shade100;
    }
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Container(
      padding: EdgeInsets.all(2.5.h),
      margin: EdgeInsets.symmetric(horizontal: 1.8.w, vertical: 2.5.h),
      decoration: BoxDecoration(
        color: _getContainerColor(),
        borderRadius: const BorderRadius.all(
          Radius.circular(16.0),
        ),
      ),
      child: Column(
        children: <Widget>[
          Text(
            'Subscription Status',
            style: theme.textTheme.titleLarge,
            textAlign: TextAlign.center,
          ),
          Container(
            padding: EdgeInsets.all(1.2.h),
            child: Text(
              _getSubscriptionStatus(),
              style: theme.textTheme.bodyMedium?.copyWith(fontSize: 17.5.sp),
            ),
          ),
        ],
      ),
    );
  }
}

// ignore_for_file: depend_on_referenced_packages

import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart' as path;

Future<String> getDownloadURL(
    {required File file, required String pathId}) async {
  final String fileName = path.basename(file.path);
  try {
    final FirebaseStorage firebaseStorage = FirebaseStorage.instance;

    final Reference ref = firebaseStorage.ref('$pathId/images/$fileName');

    await ref.putFile(file);

    return await ref.getDownloadURL();
  } on Exception {
    rethrow;
  }
}

Future<List<String>> getDownloadURLs(
    {required List<File> files, required String pathId}) async {
  final List<String> urls = <String>[];
  try {
    for (final File file in files) {
      final String fileName = path.basename(file.path);
      final Reference ref =
          FirebaseStorage.instance.ref().child('$pathId/images/$fileName');
      await ref.putFile(file).whenComplete(() async {
        await ref.getDownloadURL().then((String url) => urls.add(url));
      });
    }

    return urls;
  } catch (e) {
    rethrow;
  }
}

Future<void> deletePropertyImages(List<String> urls) async {
  try {
    final FirebaseStorage firebaseStorage = FirebaseStorage.instance;

    for (final String url in urls) {
      final Reference ref = firebaseStorage.refFromURL(url);
      await ref.delete();
    }
  } on Exception {
    rethrow;
  }
}

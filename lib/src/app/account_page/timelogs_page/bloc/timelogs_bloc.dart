import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../../data/services/patient/patient_repository.dart';

part 'timelogs_event.dart';
part 'timelogs_state.dart';

class TimelogsBloc extends Bloc<TimelogsEvent, TimelogsState> {
  final PatientRepository _patientRepository;

  TimelogsBloc({required PatientRepository patientRepository})
      : _patientRepository = patientRepository,
        super(TimelogsInitial()) {
    on<FetchTimeLogs>(_onFetchTimeLogs);
  }

  Future<void> _onFetchTimeLogs(
      FetchTimeLogs event, Emitter<TimelogsState> emit) async {
    emit(TimelogsLoading());

    try {
      final List<TimeLog> timelogs =
          await _patientRepository.fetchTimeLogsByMonth(
        month: event.month,
        patientId: event.patientId,
        careProviderId: event.careProviderId,
      );

      emit(TimelogsLoadSuccess(timelogs: timelogs));
    } on FetchDataException catch (e) {
      emit(TimelogsLoadError(error: e.toString()));
    }
  }
}

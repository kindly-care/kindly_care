import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class AvailabilityItem extends StatelessWidget {
  final String title;
  final String value;
  const AvailabilityItem({super.key, required this.title, required this.value});

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final TextStyle? dayStyle =
        theme.bodyLarge?.copyWith(fontWeight: FontWeight.w500);

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 2.5.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(title, style: dayStyle),
          Text(value, style: dayStyle),
        ],
      ),
    );
  }
}

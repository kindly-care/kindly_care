// ignore_for_file: depend_on_referenced_packages

import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:collection/collection.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:rxdart/rxdart.dart';

import '../../data/services/services.dart';

part 'account_state.dart';

class AccountCubit extends Cubit<AccountState> {
  final AuthRepository _authRepository;
  final PatientRepository _patientRepository;
  final CareProviderRepository _careProviderRepository;

  AccountCubit({
    required AuthRepository authRepository,
    required PatientRepository patientRepository,
    required CareProviderRepository agentRepository,
  })  : _authRepository = authRepository,
        _careProviderRepository = agentRepository,
        _patientRepository = patientRepository,
        super(const AccountState());

  StreamSubscription<AppUser?>? _userSubscription;
  StreamSubscription<Object>? _multiStreamSubscription;
  StreamSubscription<List<Patient>>? _patientSubscription;

  void fetchAccountData() {
    emit(const AccountState());

    try {
      _userSubscription?.cancel();
      _userSubscription =
          _authRepository.fetchCurrentUser().listen((AppUser? user) {
        if (user != null) {
          _patientSubscription?.cancel();
          _patientSubscription = _patientRepository
              .fetchCirclePatients(user.uid)
              .listen((List<Patient> patients) {
            final List<String> agentIds = patients
                .map((Patient patient) =>
                    patient.assignedCareProviders.keys.toList())
                .flattened
                .toList();

            final Stream<List<CareProvider>> providerStream =
                _careProviderRepository.fetchCareProviders(agentIds);

            _multiStreamSubscription?.cancel();
            _multiStreamSubscription = CombineLatestStream.list<dynamic>(
                <Stream<Object>>[providerStream]).listen((List<dynamic> value) {
              final List<CareProvider> careProviders =
                  value[0] as List<CareProvider>;

              emit(AccountState(
                patients: patients,
                careProviders: careProviders,
                status: AccountStatus.success,
              ));
            });
          });
        }
      });
    } on FetchDataException catch (e) {
      emit(AccountState(status: AccountStatus.error, message: e.toString()));
    }
  }

  @override
  Future<void> close() {
    _userSubscription?.cancel();
    _patientSubscription?.cancel();
    _multiStreamSubscription?.cancel();
    return super.close();
  }
}

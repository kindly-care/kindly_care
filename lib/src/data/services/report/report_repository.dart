import 'package:kindly_components/kindly_components.dart';

abstract class ReportRepository {
  /// fetches [Patient]s [Report]. Throws [FetchDataException].
  Future<List<Report>> fetchReportsByMonth(
      {required String patientId, required int month});

  /// fetches a single [Patient]'s [Report] by id. Throws [FetchDataException].
  Future<Report?> fetchReport(
      {required String patientId, required String reportId});
}

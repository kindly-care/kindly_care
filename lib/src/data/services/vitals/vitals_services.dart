import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dart_date/dart_date.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/constants/constants.dart';
import '../../references.dart';
import 'vitals_repository.dart';

class VitalsService implements VitalsRepository {
  @override
  Future<List<WeightRecord>> fetchWeightByMonth(
      {required int month, required String patientId}) async {
    final DateTime date = DateTime.now().setMonth(month);

    final String start = DateFormat(kYMD).format(date.startOfMonth);
    final String end = DateFormat(kYMD).format(date.endOfMonth);

    try {
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .doc(patientId)
          .collection(kWeight)
          .where(kDateCreated, isGreaterThanOrEqualTo: start)
          .where(kDateCreated, isLessThanOrEqualTo: end)
          .orderBy(kDateCreated, descending: true)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((DocumentSnapshot<Map<String, dynamic>> doc) =>
              WeightRecord.fromJson(doc.data()!))
          .toList();
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Future<List<TemperatureRecord>> fetchTemperatureByMonth(
      {required int month, required String patientId}) async {
    final DateTime date = DateTime.now().setMonth(month);

    final String start = DateFormat(kYMD).format(date.startOfMonth);
    final String end = DateFormat(kYMD).format(date.endOfMonth);

    try {
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .doc(patientId)
          .collection(kTemperature)
          .where(kDateCreated, isGreaterThanOrEqualTo: start)
          .where(kDateCreated, isLessThanOrEqualTo: end)
          .orderBy(kDateCreated, descending: true)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((DocumentSnapshot<Map<String, dynamic>> doc) =>
              TemperatureRecord.fromJson(doc.data()!))
          .toList();
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Future<List<BloodPressureRecord>> fetchBloodPressureByMonth(
      {required int month, required String patientId}) async {
    final DateTime date = DateTime.now().setMonth(month);

    final String start = DateFormat(kYMD).format(date.startOfMonth);
    final String end = DateFormat(kYMD).format(date.endOfMonth);

    try {
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .doc(patientId)
          .collection(kBloodPressure)
          .where(kDateCreated, isGreaterThanOrEqualTo: start)
          .where(kDateCreated, isLessThanOrEqualTo: end)
          .orderBy(kDateCreated, descending: true)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((DocumentSnapshot<Map<String, dynamic>> doc) =>
              BloodPressureRecord.fromJson(doc.data()!))
          .toList();
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Future<List<BloodGlucoseRecord>> fetchBloodGlucoseByMonth(
      {required int month, required String patientId}) async {
    final DateTime date = DateTime.now().setMonth(month);

    final String start = DateFormat(kYMD).format(date.startOfMonth);
    final String end = DateFormat(kYMD).format(date.endOfMonth);

    try {
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .doc(patientId)
          .collection(kBloodGlucose)
          .where(kDateCreated, isGreaterThanOrEqualTo: start)
          .where(kDateCreated, isLessThanOrEqualTo: end)
          .orderBy(kDateCreated, descending: true)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((DocumentSnapshot<Map<String, dynamic>> doc) =>
              BloodGlucoseRecord.fromJson(doc.data()!))
          .toList();
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Future<List<TemperatureRecord>> fetchTemperatureByDate({
    required DateTime date,
    required String patientId,
    required String careProviderId,
  }) async {
    final String dateCreated = DateFormat(kYMD).format(date);

    try {
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .doc(patientId)
          .collection(kTemperature)
          .where(kDateCreated, isEqualTo: dateCreated)
          .where(kCareProviderId, isEqualTo: careProviderId)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((QueryDocumentSnapshot<Map<String, dynamic>> doc) =>
              TemperatureRecord.fromJson(doc.data()))
          .toList();
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Future<List<WeightRecord>> fetchWeightByDate({
    required DateTime date,
    required String patientId,
    required String careProviderId,
  }) async {
    final String dateCreated = DateFormat(kYMD).format(date);

    try {
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .doc(patientId)
          .collection(kWeight)
          .where(kDateCreated, isEqualTo: dateCreated)
          .where(kCareProviderId, isEqualTo: careProviderId)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((QueryDocumentSnapshot<Map<String, dynamic>> doc) =>
              WeightRecord.fromJson(doc.data()))
          .toList();
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Future<List<BloodPressureRecord>> fetchBloodPressureByDate({
    required DateTime date,
    required String patientId,
    required String careProviderId,
  }) async {
    final String dateCreated = DateFormat(kYMD).format(date);

    try {
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .doc(patientId)
          .collection(kBloodPressure)
          .where(kDateCreated, isEqualTo: dateCreated)
          .where(kCareProviderId, isEqualTo: careProviderId)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((QueryDocumentSnapshot<Map<String, dynamic>> doc) =>
              BloodPressureRecord.fromJson(doc.data()))
          .toList();
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Future<List<BloodGlucoseRecord>> fetchBloodSugarByDate({
    required DateTime date,
    required String patientId,
    required String careProviderId,
  }) async {
    final String dateCreated = DateFormat(kYMD).format(date);

    try {
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .doc(patientId)
          .collection(kBloodGlucose)
          .where(kDateCreated, isEqualTo: dateCreated)
          .where(kCareProviderId, isEqualTo: careProviderId)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((QueryDocumentSnapshot<Map<String, dynamic>> doc) =>
              BloodGlucoseRecord.fromJson(doc.data()))
          .toList();
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }
}

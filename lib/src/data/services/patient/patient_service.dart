import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dart_date/dart_date.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:uuid/uuid.dart';

import '../../../common/common.dart';
import '../../../common/constants/constants.dart';
import '../../references.dart';
import '../firebase_helper.dart';
import 'patient_repository.dart';

class PatientService implements PatientRepository {
  final FirebaseFirestore _db = FirebaseFirestore.instance;

  @override
  Future<void> createPatient(Patient patient,
      {bool isNewPatient = false}) async {
    try {
      String avatar = patient.avatar;
      if (patient.localAvatar != null) {
        avatar = await getDownloadURL(
                file: patient.localAvatar!, pathId: patient.uid)
            .timeout(kTimeOut);
      }

      await userCollection
          .doc(patient.uid)
          .set(patient.copyWith(avatar: avatar).toJson(),
              SetOptions(merge: true))
          .timeout(kTimeOut);
      analytics.logEvent(name: kPatientCreatedEvt);
    } on Exception {
      throw UpdateDataException(kOperationFailed);
    } finally {
      if (isNewPatient) {
        await _autogenerateTasks(patient.uid);
      }
    }
  }

  @override
  Future<List<CareProvider>> fetchAssignedCareProviders(
      List<String> ids) async {
    try {
      if (ids.isEmpty) {
        return const <CareProvider>[];
      }
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .where(kUID, whereIn: ids)
          .where(kUserType, isEqualTo: kCareProvider)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((DocumentSnapshot<Map<String, dynamic>> doc) =>
              CareProvider.fromJson(doc.data()!))
          .toList();
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Stream<List<Patient>> fetchCirclePatients(String uid) {
    try {
      final Stream<QuerySnapshot<Map<String, dynamic>>> query = userCollection
          .where(kCircleMembers, arrayContains: uid)
          .where(kUserType, isEqualTo: kPatient)
          .snapshots();

      return query.map((QuerySnapshot<Map<String, dynamic>> snapshot) =>
          snapshot.docs
              .map((QueryDocumentSnapshot<Map<String, dynamic>> doc) =>
                  Patient.fromJson(doc.data()))
              .toList());
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Stream<Patient?> fetchPatient(String patientId) {
    try {
      final Stream<DocumentSnapshot<Map<String, dynamic>>> query =
          userCollection.doc(patientId).snapshots();

      return query.map((DocumentSnapshot<Map<String, dynamic>> doc) {
        if (doc.exists) {
          return Patient.fromJson(doc.data()!);
        } else {
          return null;
        }
      });
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Future<Patient?> fetchPatientById(String patientId) async {
    try {
      final DocumentSnapshot<Map<String, dynamic>> doc =
          await userCollection.doc(patientId).get().timeout(kTimeOut);

      if (doc.exists) {
        return Patient.fromJson(doc.data()!);
      } else {
        return null;
      }
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  @override
  Future<void> deleteCareRecipient(String patientId) async {
    try {
      await userCollection.doc(patientId).delete().timeout(kTimeOut);
      analytics.logEvent(name: kPatientRemovedEvt);
    } on Exception {
      throw UpdateDataException(kOperationFailed);
    }
  }

  @override
  Future<void> deletePatients(List<String> patientIds) async {
    final WriteBatch batch = FirebaseFirestore.instance.batch();
    try {
      for (final String patientId in patientIds) {
        final DocumentReference<Map<String, dynamic>> patientDoc =
            userCollection.doc(patientId);

        batch.delete(patientDoc);
      }
      await batch.commit().timeout(kTimeOut);
    } on Exception {
      throw UpdateDataException(kOperationFailed);
    }
  }

  @override
  Future<void> removeCareProvider(
      {required Patient patient, required CareProvider careProvider}) async {
    final WriteBatch batch = FirebaseFirestore.instance.batch();
    try {
      final DocumentReference<Map<String, dynamic>> patientDoc =
          userCollection.doc(patient.uid);

      final DocumentReference<Map<String, dynamic>> careProviderDoc =
          userCollection.doc(careProvider.uid);

      batch.update(patientDoc, <String, dynamic>{
        'assignedCareProviders.${careProvider.uid}': FieldValue.delete(),
      });

      batch.update(careProviderDoc, <String, dynamic>{
        'assignedPatients.${patient.uid}': FieldValue.delete(),
      });

      await batch.commit().timeout(kTimeOut);
      analytics.logEvent(name: kCareProviderDismissedEvt);
    } on Exception {
      throw UpdateDataException(kOperationFailed);
    }
  }

  @override
  Future<List<TimeLog>> fetchTimeLogsByMonth({
    required int month,
    required String patientId,
    required String careProviderId,
  }) async {
    final DateTime day = DateTime.now().setMonth(month);

    final String start = DateFormat(kYMD).format(day.startOfMonth);
    final String end = DateFormat(kYMD).format(day.endOfMonth);

    try {
      final QuerySnapshot<Map<String, dynamic>> query = await _db
          .collectionGroup(kTimeLogs)
          .where(kCareProviderId, isEqualTo: careProviderId)
          .where(kPatientId, isEqualTo: patientId)
          .where(kDateCreated, isGreaterThanOrEqualTo: start)
          .where(kDateCreated, isLessThanOrEqualTo: end)
          .orderBy(kDateCreated, descending: true)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((DocumentSnapshot<Map<String, dynamic>> doc) =>
              TimeLog.fromJson(doc.data()!))
          .toList();
    } on Exception {
      throw FetchDataException(kFailedToLoadData);
    }
  }

  Future<void> _autogenerateTasks(String patientId) async {
    final WriteBatch batch = _db.batch();

    final List<MealTask> tasks = _buildCareTasks(patientId);
    for (final MealTask task in tasks) {
      final DocumentReference<Map<String, dynamic>> doc =
          userCollection.doc(patientId).collection(kCareTasks).doc(task.taskId);

      batch.set(doc, task.toJson());
    }

    await batch.commit();
  }

  List<MealTask> _buildCareTasks(String patientId) {
    return <MealTask>[
      MealTask(
        title: 'Breakfast',
        patientId: patientId,
        mealType: 'Breakfast',
        type: CareTaskType.meal,
        repeatDays: kLongWeekDays,
        taskId: const Uuid().v4(),
        lastUpdated: DateTime.now(),
        lastUpdatedBy: 'Auto generated',
        time: DateTime.now().setHour(9, 15),
        description: 'Serve patient Breakfast.',
      ),
    ];
  }
}

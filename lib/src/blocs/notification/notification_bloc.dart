import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../data/services/services.dart';

part 'notification_event.dart';
part 'notification_state.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  final NotificationRepository _notificationRepository;
  List<PushNotification> notifications = <PushNotification>[];
  NotificationBloc({required NotificationRepository notificationRepository})
      : _notificationRepository = notificationRepository,
        super(NotificationInitial()) {
    on<FetchNotifications>(_onFetchNotifications);
    on<DeleteNotification>(_onDeleteNotification);
    on<DeleteAllNotifications>(_onDeleteAllNotifications);
    on<SendPushNotification>(_onSendPushNotification);
    on<MarkNotificationAsSeen>(_onMarkNotificationAsSeen);
  }

  Future<void> _onFetchNotifications(
      FetchNotifications event, Emitter<NotificationState> emit) async {
    emit(NotificationsLoading());

    await emit.forEach<List<PushNotification>>(
      _notificationRepository.fetchNotifications(event.uid),
      onData: (List<PushNotification> data) {
        notifications = data;
        return NotificationsLoadSuccess(notifications: data);
      },
      onError: (_, __) =>
          const NotificationsLoadError(message: 'Failed to load data'),
    );
  }

  Future<void> _onDeleteNotification(
      DeleteNotification event, Emitter<NotificationState> emit) async {
    emit(DeleteNotificationInProgress());
    try {
      await _notificationRepository.deleteNotification(
          uid: event.uid, notificationId: event.notificationId);

      emit(const DeleteNotificationSuccess(message: 'Notification deleted.'));
    } on UpdateDataException catch (e) {
      emit(DeleteNotificationError(message: e.toString()));
    }
  }

  Future<void> _onDeleteAllNotifications(
      DeleteAllNotifications event, Emitter<NotificationState> emit) async {
    emit(DeleteNotificationInProgress());

    final List<String> notificationIds = notifications
        .map((PushNotification notification) => notification.id)
        .toList();
    try {
      await _notificationRepository.deleteAllNotifications(
        uid: event.uid,
        notificationIds: notificationIds,
      );
      emit(const DeleteNotificationSuccess(message: 'Notifications deleted.'));
    } on UpdateDataException catch (e) {
      emit(DeleteNotificationError(message: e.toString()));
    }
  }

  Future<void> _onSendPushNotification(
      SendPushNotification event, Emitter<NotificationState> emit) async {
    emit(SendNotificationInProgress());

    try {
      await _notificationRepository.sendPushNotification(event.notification);
      emit(const SendNotificationSuccess(message: 'Operation success'));
    } on UpdateDataException catch (e) {
      emit(SendNotificationError(message: e.toString()));
    }
  }

  Future<void> _onMarkNotificationAsSeen(
      MarkNotificationAsSeen event, Emitter<NotificationState> emit) async {
    _notificationRepository.markNotificationAsSeen(
        uid: event.uid, notificationId: event.notificationId);
  }
}

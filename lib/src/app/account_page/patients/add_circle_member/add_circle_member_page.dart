import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../blocs/circle_member/circle_member_bloc.dart';
import '../../../../common/constants/constants.dart';
import '../../../../data/services/services.dart';

class AddCircleMemberPage extends StatelessWidget {
  final String patientId;
  const AddCircleMemberPage({super.key, required this.patientId});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CircleMemberBloc>(
      create: (BuildContext context) => CircleMemberBloc(
        authRepository: context.read<AuthService>(),
        circleRepository: context.read<CircleService>(),
      ),
      child: _AddCircleMemberPageView(patientId),
    );
  }
}

class _AddCircleMemberPageView extends StatefulWidget {
  final String patientId;
  const _AddCircleMemberPageView(this.patientId, {Key? key}) : super(key: key);

  @override
  State<_AddCircleMemberPageView> createState() =>
      _AddCircleMemberPageViewState();
}

class _AddCircleMemberPageViewState extends State<_AddCircleMemberPageView> {
  bool _isNextOfKin = false;
  late TextEditingController _emailController;
  late TextEditingController _relationController;

  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController();
    _relationController = TextEditingController()
      ..addListener(_onRelationChanged);
  }

  void _onRelationChanged() {
    setState(() {});
  }

  @override
  void dispose() {
    _emailController.dispose();
    _relationController.dispose();
    super.dispose();
  }

  void _onSaveButtonTap(AppUser user) {
    if (_emailController.text.simpleValidateEmail != null) {
      showToast(message: 'Please enter a valid email address');
      return;
    } else if (_relationController.text.simpleValidate != null) {
      showToast(message: 'Enter relation to the Care Recipient');
      return;
    } else {
      final CircleMemberData data = CircleMemberData(
        uid: user.uid,
        isNextOfKin: _isNextOfKin,
        patientId: widget.patientId,
        email: _emailController.text,
        relation: _relationController.text,
      );
      context.read<CircleMemberBloc>().add(AddCircleMember(data: data));
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final bool isLoading = context.select((CircleMemberBloc bloc) => bloc.state)
        is AddingCircleMemberInProgress;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Circle Member'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.help_outline, size: 3.8.h),
            onPressed: () {
              showOneButtonDialog(
                context,
                title: 'Circle Member',
                content:
                    'Circle Members are friends or relatives of the Care Recipient. Once added, Circle Members will start receiving updates on meals, health records, care tasks and reports from Care Providers. Before adding Circle Members, please ensure that they have the Kindly Care App installed and set up on their device.',
                buttonText: 'Ok',
                onPressed: () => Navigator.pop(context),
              );
            },
          ),
        ],
      ),
      body: LoadingOverlay(
        isLoading: isLoading,
        child: BlocListener<CircleMemberBloc, CircleMemberState>(
          listener: (BuildContext context, CircleMemberState state) {
            if (state is CircleMemberAddError) {
              retrySnackbar(
                context,
                state.error,
                onRetry: () => _onSaveButtonTap(user),
              );
            } else if (state is CircleMemberAddSuccess) {
              Navigator.pop(context);
              successSnackbar(context, state.message);
            }
          },
          child: ListView(
            padding: EdgeInsets.all(3.0.w),
            physics: const BouncingScrollPhysics(),
            children: <Widget>[
              SizedBox(height: 1.5.h),
              Text('Add Circle Member', style: theme.titleLarge),
              SizedBox(height: 3.2.h),
              UnderlineTextField(
                controller: _emailController,
                onTap: () => setState(() {}),
                textInputAction: TextInputAction.next,
                textInputType: TextInputType.emailAddress,
                labelText: "Circle Member's email address",
              ),
              SizedBox(height: 5.0.h),
              UnderlineTextField(
                readOnly: true,
                controller: _relationController,
                onTap: () {
                  _relationController.addListener(() {});
                  showSingleSelectPicker(
                    context,
                    title: 'Relation',
                    items: kRelations,
                    labelText: 'Relation to Care Recipient',
                    controller: _relationController,
                  );
                },
                textInputAction: TextInputAction.next,
                textInputType: TextInputType.emailAddress,
                labelText: 'Relation to Care Recipient',
              ),
              SizedBox(height: 5.0.h),
              CheckboxListTile(
                value: _isNextOfKin,
                onChanged: (bool? val) {
                  setState(() => _isNextOfKin = val!);
                },
                title: const Text('Is Member Next Of Kin?'),
                activeColor: Theme.of(context).primaryColor,
                controlAffinity: ListTileControlAffinity.trailing,
                contentPadding: EdgeInsets.symmetric(horizontal: 2.0.w),
              ),
              SizedBox(height: 25.5.h),
              ActionButton(
                enabled: _emailController.text.isNotEmpty &&
                    _relationController.text.isNotEmpty,
                title: 'Save',
                onPressed: () => _onSaveButtonTap(user),
              )
            ],
          ),
        ),
      ),
    );
  }
}
